..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

========
Glossary
========

.. glossary::

   Control System
     A system that manages, commands, regulates the behaviour of a set
     of devices.

   Digital Signal Processing
     The use of digital processing to perform a wide variety of signal
     processing operations.

   Embedded System
     An autonomus system made of software, hardware (and gateware),
     implementing dedicated functions

   End-Point
     A gateware core connected to a Mock Turtle RMQ that provides
     connection to an external network.

   Firmware
     An embedded software system running on a Mock Turtle soft-CPU.

   Gateware
     A bitstream which configures an FPGA, or the HDL sources
     from which it was generated.

   Gateware Core
     An HDL component part of a more complex gateware design.

   Hardware
     A physical component.

   Host
     It is the system that hosts the hardware in use.

   Host Application
     A user space program running on the host system.

   HMQ
   Host Message Queue
     A message queue that connects Mock Turtle to the host system.

   MQ
   Message Queue
     A communication system based on queues with FIFO policy. Messages are
     put on the queue and they are sent to the programmed destination. Each
     message queue has two directions: input and output.
     Mock Turtle supports two message queues: host and remote.

   RMQ
   Remote Message Queue
     A message queue that connects the Mock Turtle to a network.

   SHM
   Shared Memory
     A memory shared among soft-CPUs and the host system.

   Soft CPU
     An HDL implementation of a CPU running on an FPGA.

   MQ Entry
     A single element in the MQ.

   User Space
     A software running on the host, but not in kernel mode.
     This includes libraries and programs.
