..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

==========================
The *Alarm Clock* Demo
==========================

The *Alarm Clock* demo is a firmware program that makes use of the
*Host Message Queue* to receive configuration options and to send
messages to the host system.

This program makes use of the :ref:`sw:fw:frm` and shows the use
of *variables*. The application exports to the host a set of local
variables which are used to configure the application.

::

   mockturtle-variable -D $DEVID

The application counts the number of internal iterations; this is represented
by the AC_TIME variable. According to the local variable AC_PERIOD_UPDATE it
periodically sends messages to the host system to notify the current time.
Using the variables AC_ALARM_EN and AC_ALARM_ITER, It is possible to enable and
configure an alarm which will produce a message.

The communication with the host happens through the message queues as described
in the following table.

.. list-table::
   :widths: 1 1 2
   :header-rows: 1

   * - Direction
     - Index
     - Description

   * - Input
     - 0
     - Receive configuration from the host

   * - Output
     - 0
     - Send configuration to the host

   * - Output
     - 1
     - Send notifications

.. highlight:: c
.. literalinclude:: ../../demos/alarm_clock/firmware/fw-01/fw-ac.c
