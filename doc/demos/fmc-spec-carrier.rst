..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`demo:spec`:

===================
The *FMC SPEC* Demo
===================

The *FMC SPEC* demo is a complete demo that uses hardware features from
the `FMC SPEC carrier`_. This demo offers an example of all layers, so it is
a good starting point to understand how to create a complete Mock Turtle
application. Apart from the board itself, no other hardware is necessary to
run the demo.

The main aim of this demo is to handle the SPEC LEDs and buttons. The LEDs
can be turned *on* and *off* and the status of the buttons can be read.

HDL Code
==========

The top-level VHDL entity of the demo can be found under *hdl/top/spec_mt_demo/spec_mt_demo.vhd*,
while an `Hdlmake`_ project file (able to produce an FPGA bitstream of the demo) is available under
*hdl/syn/spec_mt_demo/Manifest.py*.

The SPEC demo defines the following :ref:`hdl:cfg`::

  constant c_MT_CONFIG : t_mt_config := (
    app_id     => x"d331d331",
    cpu_count  => 2,
    cpu_config => (others =>
		   (memsize => 8192,
		    hmq_config => (2, (0      => (7, 3, 2, x"0000_0000"),
				       1      => (5, 4, 3, x"0000_0000"),
				       others => (c_DUMMY_MT_MQUEUE_SLOT))),
		    rmq_config => (1, (0      => (7, 2, 2, x"0000_0000"),
				       others => (c_DUMMY_MT_MQUEUE_SLOT))))),
    shared_mem_size => 2048);

The above configuration instantiates two soft CPUs, each with two host message queues (of different
sizes) and one remote message queue.

the SPEC demo :ref:`hdl:inst` is done using the following VHDL code::

  U_Mock_Turtle : mock_turtle_core
    generic map (
      g_CONFIG            => c_MT_CONFIG,
      g_WITH_WHITE_RABBIT => FALSE)
    port map (
      clk_i           => clk_sys,
      rst_n_i         => rst_n_sys,
      dp_master_o     => dp_wb_out,
      dp_master_i     => dp_wb_in,
      host_slave_i    => cnx_master_out(c_SLAVE_MT),
      host_slave_o    => cnx_master_in(c_SLAVE_MT),
      rmq_src_o       => rmq_ds_o,
      rmq_src_i       => rmq_ds_i,
      rmq_snk_o       => rmq_us_o,
      rmq_snk_i       => rmq_us_i,
      hmq_in_irq_o    => mt_hmq_in_irq,
      hmq_out_irq_o   => mt_hmq_out_irq,
      notify_irq_o    => mt_notify_irq,
      console_irq_o   => mt_console_irq);

All unconnected inputs will get their default values.

The Wishbone host interface is attached to a Wishbone crossbar, and from there to the PCIe host
interface. All interrupt lines are driven into a Vectored Interrupt Controller (VIC). The remote
message queue interfaces are simply forming a loopback (for testing).

For each one of the two configured soft CPUs, their respective DP interface is attached to an 8-bit
Wishbone GPIO peripheral. The outputs from the two GPIO peripherals are logically OR'ed, while their
inputs are copies of the same signals. The mapping of these 8 signals is the following:

+ GPIO0 to GPIO1: Two push buttons on the SPEC board (0 button pressed, 1 button released)
+ GPIO2 to GPIO5: Four LEDs on the SPEC board (1 LED on, 0 LED off)
+ GPIO6 to GPIO7: Two LEDs on the front panel of the SPEC (1 LED on, 0 LED off)

All mentioned peripherals (WB crossbar, VIC, WB GPIO) are available as part of `OHWR
general-cores`_. The SPEC demo also uses the Gennum `GN4124 core`_ to provide the host interface.

Software
=========

This demo uses two firmware programs. One is named *blinker* (fw-01), the other
*controller* (fw-02).

The *blinker* firmware runs autonomously without any communication with the host
system or a remote node and for this reason it is the simplest one.
It does not use :ref:`sw:fw:frm` but only :ref:`sw:fw:lib`. This firmware does
the following things:

- it turns *on* and *off* all the LEDs according to the given mode;
- it pauses the blinking sequence when a button is configured to trigger this;
- it resets the blinking sequence when a button is configured to trigger this;

.. highlight:: c
.. literalinclude:: ../../demos/fmc-spec-carrier/software/firmware/fw-01/fw-spec.c

The *controller* firmware gives you the possibility to control the *blinker*.
This firmware does the following things:

- it exports as a shared memory :ref:`variable <sw:fw:frm:var>` the blinking mode;
- it exports as a shared memory :ref:`variable <sw:fw:frm:var>` the buttons' actions;

.. highlight:: c
.. literalinclude:: ../../demos/fmc-spec-carrier/software/firmware/fw-02/fw-spec.c

.. _`FMC SPEC carrier`: https://www.ohwr.org/project/spec/
.. _GN4124 core: https://www.ohwr.org/project/gn4124-core/wiki
.. _Hdlmake: https://www.ohwr.org/project/hdl-make/wiki
.. _OHWR general-cores: https://www.ohwr.org/project/general-cores/wiki
