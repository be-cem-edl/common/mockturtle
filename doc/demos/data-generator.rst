..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

==============================
The *Data Generator* Demo
==============================

The *Data Generator* demo is a firmware program that pretends to be a
little acquisition system. It makes use of the *Host Message Queue* to
receive configuration options and to send messages to the host system.

This program makes use of the :ref:`sw:fw:frm` and shows the use
of *variables* and *buffers*. The application exports to the host a
set of local variables which are used to configure the application; it
also exports buffers for data array or structures.

::

   mockturtle-variable -D $DEVID
   mockturtle-buffer -D $DEVID

The application periodically generates data. The generation period can be
adjusted using the variable DG_PERIOD_UPDATE. The application generates
sequential values which can be adjusted using gain and offset; these 2
parameters are part of a data structure exported with the buffer DG_CONF.
Finally, it is possible to read the data from the buffer DG_DATA.

The communication with the host happens through the message queues as described
in the following table.

.. list-table::
   :widths: 1 1 2
   :header-rows: 1

   * - Direction
     - Index
     - Description

   * - Input
     - 0
     - Receive configuration from the host

   * - Output
     - 0
     - Send configuration to the host

   * - Output
     - 1
     - Send data to the host

.. highlight:: c
.. literalinclude:: ../../demos/data_generator/firmware/fw-01/fw-dg.c
