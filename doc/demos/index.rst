..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`demo`:

============
The Demos
============

This is a collection of demo applications whose main purpose is to
introduce the users to the Mock Turtle development. In the following
demos you will find some example code to run and test the applications.
Unless it is explicitly specified, these demos can run on **any** Mock Turtle
instance, in other words they do not depends on a specific HDL or hardware
design.

You will notice the usage of environment variables; these variables, of
course, depend on your environment. Here is a list of used variables:

TRTL
  This is the path to the root directory of the Mock Turtle project.

CROSS_COMPILE_TARGET
  This is the path to the cross-compiler for the soft-CPU used by Mock Turtle.

DEVID
  This is the device-id that uniquely identify a Mock Turtle instance. This
  is an integer number in hexadecimal representation (e.g. 0x0201)

CPU_INDEX
  This is used to select a Mock Turtle soft-CPU starting from 0.

TTYTRTL
  This is the path to the TTY device in /dev (e.g. /dev/ttyTRTL0)

DEMO
  This is the path to the demo application directory that you can find in the
  ``software/demos`` main directory.

In principle you can compile all the demos by running *make* in the main
directory. Then you can load the firmware using the
:ref:`tools:mockturtle-firmware-loader` tool and restart the CPU with the
:ref:`tools:mockturtle-cpu-restart` tool::

   # Compile
   make -C $DEMO
   # Program
   mockturtle-loader -D $DEVID -i $CPU_INDEX -f $DEMO/firmware/fw01/hello_world.bin
   # Restart and start execution
   mockturtle-cpu-restart -D $DEVID -i $CPU_INDEX

.. toctree::
   :maxdepth: 2
   :caption: Contents

   hello-world
   data-generator
   alarm-clock
   fmc-svec-carrier
   fmc-spec-carrier
