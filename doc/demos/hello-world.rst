..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

==========================
The *Hello World* Demo
==========================

The *Hello World* demo is a firmware program that prints
over the serial interface the string ``"Hello World"`` and exits.

This program makes use of the :ref:`sw:fw:lib`.

::

   minicom -D $TTYTRTL
   # On a different shell instance
   mockturtle-cpu-restart -D $DEVID -i $CPU_INDEX

.. literalinclude:: ../../demos/hello_world/firmware/fw-01/fw-hello.c

There is also the *Hello World* demo based on the :ref:`sw:fw:frm`.
This demo will print on the serial interface general information regarding
the firmware application.

::

   minicom -D $TTYTRTL
   # On a different shell instance
   mockturtle-cpu-restart -D $DEVID -i $CPU_INDEX

.. highlight:: c
.. literalinclude:: ../../demos/hello_world_framework/firmware/fw-01/fw-hellofrm.c
