..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. Mock Turtle documentation master file, created by
   sphinx-quickstart on Wed Mar  7 15:12:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mock Turtle's documentation!
=======================================

.. toctree::
   :maxdepth: 1
   :caption: Contents

   introduction
   architecture
   hdl/index
   software/index
   tools/index
   demos/index
   registers/index
   glossary

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`wbgen_csr`
* :ref:`wbgen_lr`

