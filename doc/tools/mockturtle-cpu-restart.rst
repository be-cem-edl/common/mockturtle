..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`tools:mockturtle-cpu-restart`:

=========================
Mock Turtle CPU Restart
=========================

The Mock Turtle CPU Restart application (*mockturtle-cpu-restart*) is
used to restart a soft-CPU. This will stop the firmware execution and
start it again from the ``main()`` function.

You can get the list of available Mock Turtle devices that you can access
with the command ``lsmockturtle``.
