..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`tools:mockturtle-buffer`:

Mock Turtle Buffer
------------------

The Mock Turtle buffer application (*mockturtle-buffer*) allows the
user to read/write the buffers that a firmware exports. The tool only
works with firmware developed using :ref:`sw:fw:frm`.

You can get the list of available Mock Turtle devices that you can access
with the command ``lsmockturtle``.
