..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _tools:

=======================
The Mock Turtle Tools
=======================

This section describes the Mock Turtle tools. The description is limited
to the main purpose of the tool, for more details use the tool’s help
message. All tools are available in the directory *software/tools*.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   lsmockturtle
   mockturtle-project-creator
   mockturtle-firmware-loader
   mockturtle-messages
   mockturtle-smem
   mockturtle-cpu-restart
   mockturtle-ping
   mockturtle-variable
   mockturtle-buffer
   mockturtle-debug
   mockturtle-gdbserver
