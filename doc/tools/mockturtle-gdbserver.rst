..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _tools:mockturtle-gdbserver:

======================
Mock Turtle GDB Server
======================

The Mock Turtle GDB Server application (*mockturtle-gdbserver*) provides
access to the Mock Turtle CPU debug interface. The user can start the server
against a Mock Turtle instance and start debugging remotly using ``gdb(1)``.
This debug interface is accessible only if the Linux debugfs file-system is
mounted at ``/sys/kernel/debug``; be sure to run the following command before
starting the *mockturtle-gdbserver*:::

  mount -t debugfs none /sys/kernel/debug

Please refer to the application help message for more information.

You can get the list of available Mock Turtle devices that you can access
with the command ``lsmockturtle``.
