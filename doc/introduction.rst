..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _introduction:description:

============
Introduction
============

Mock Turtle is a framework to develop embedded systems on FPGA.

The need for this framework comes from the fact that in some contexts
the development of a gateware core is more complex than writing a
software application. Typically, software takes more computation time
than a custom-designed gateware core; but on the other hand, the
development and support efforts are significantly reduced. By using Mock
Turtle, the gateware core complexity is moved to the software domain
within the Mock Turtle boundaries, without sacrificing determinism.

The Mock Turtle framework provides an infrastructure on which you can
build an FPGA-based embedded system. The basic ingredient of this
framework is a soft-CPU multi-core environment that can be used to write
firmware to control/monitor gateware cores. In other words, you can
connect Mock Turtle to your gateware cores and control them with the
firmware running on the soft-CPU. In addition, the Mock Turtle framework
provides a communication channel between the firmware and the host
applications which can be used to configure or control the firmware.

The Mock Turtle framework focuses mainly on the determinism of the
firmware running in it. Indeed, Mock Turtle does not support any kind of
interrupt or scheduling which might compromise the determinism.

The Mock Turtle framework includes the following components:

-  Gateware

   -  The Mock Turtle core

      -  Shared Memory among soft-CPUs and host system

      -  Up to 8 soft-CPUs

      -  communication with the host system (input, output)

      -  communication with remote systems (input, output)

-  Software

   -  the Mock Turtle firmware library to access gateware cores from the
      firmware

   -  the Mock Turtle firmware framework to develop firmware

If Mock Turtle is used within a Linux host system, the user can take
advantage of a number of software components which run on the host and
support Mock Turtle:

-  the Mock Turtle Linux device driver

-  the Mock Turtle library that provides uniform access to the driver

-  the Mock Turtle Python module to access the library using Python

.. _introduction:use-cases:

Use Cases
==========

The focus on the high determinism of its soft-CPUs makes Mock Turtle
a very good candidate to implement (typically real-time) control systems.

To demonstrate the system architecture and benefits of Mock Turtle,
let’s take the classical control system example from control theory: the
heating system. We have a thermometer sensor, and an actuator to adjust
the temperature.

The following figures show the control system architecture with and
without Mock Turtle.

.. list-table::
   :header-rows: 0
   :widths: 1 1

   * - .. figure:: img/mock-turtle-use-case-1.svg
          :align: center

          Control system **without** Mock-Turtle

     - .. figure:: img/mock-turtle-use-case-2.svg
          :align: center

          Control system **with** Mock-Turtle



In the scenario without Mock-Turtle, the user is responsible for the
entire development. On the other hand, with Mock Turtle, the user will
be responsible only for the development of the application-specific
control logic, which is moved to the software domain.  The user does not
have to care about the communication with the host system or the
external world because it’s already part of the Mock Turtle framework.

.. _introduction:when-do-not-consider-mock-turtle:

When Not To Consider Mock Turtle
--------------------------------

Since the Mock Turtle soft-CPUs have limited computation power, this
precludes using it for applications like Digital Signal Processing
(DSP).

If you really want to use Mock Turtle for dsp analysis, please consider
the development of a dedicated gateware core to perform the DSP analysis
and to use Mock Turtle as a control system for the DSP gateware core.

Where To Get Mock Turtle
========================

Mock Turtle is officially hosted on the `Open Hardware Repository`_:
`Mock Turtle`_. This project is distributed as a git repository which
can be cloned using the following command::

    git clone https://ohwr.org/project/mock-turtle.git

In it you can find all sources: HDL, software, demos, tests and this
documentation.

.. _`Open Hardware Repository`: https://www.ohwr.org/
.. _`Mock Turtle`: https://www.ohwr.org/project/mock-turtle
