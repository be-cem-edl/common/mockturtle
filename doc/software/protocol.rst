..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN


.. _`sw:common`:

===========================
Common Data Structures
===========================

.. _`sw:cfg`:

Configuration ROM
===================

The *configuration ROM* is, indeed, a ROM where at synthesis time we put
information about the synthesis configuration. This configuration is the one
used to tailor Mock Turtle to fit users needs. The configuration can be read,
with different APIs, by both host system and firmware.

.. doxygenstruct:: trtl_config_rom
   :members:

.. doxygenstruct:: trtl_config_rom_mq
   :members:

.. doxygendefine:: TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES

.. doxygendefine:: TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD

.. doxygendefine:: TRTL_CONFIG_ROM_MQ_SIZE_HEADER

.. _`sw:proto:hmq`:

Host Message Queue Protocol
==============================

A protocol is necessary in order to exchange messages between two entities.
Any Mock Turtle message queue has a header part and a payload part. It is
within the header part that users put the information to handle the chosen
protocol.

In order to standardize the message exchange between host and firmware
a message header has been defined. This header is expected to be in the
message queue header buffer.

Different Mock Turtle layers make different use of this message header.
The HDL code does not process the header, while the driver uses the
:c:member:`trtl_hmq_header.len` to optimize the amount of data copied.

This protocol is mostly used by libraries and firmware, which are the
two end-points of Host Message Queue communication channel.


.. doxygenstruct:: trtl_hmq_header
   :members:

.. doxygendefine:: TRTL_HMQ_HEADER_FLAG_SYNC

.. doxygendefine:: TRTL_HMQ_HEADER_FLAG_ACK

.. doxygendefine:: TRTL_HMQ_HEADER_FLAG_RPC


.. todo::
   Should the framework/library handle sync_id and seq?



.. _`sw:proto:rmq`:

Remote Message Queue Protocol
-------------------------------

Mock Turtle only defines a protocol for the communication of the cores with
the host system. How to handle the communication with remote nodes is left
to the user who can choose among existing protocols (for example UDP).

A remote queue message is also made of a header and a payload; whatever is
the chosen protocol, its header will lay in the header part and the payload
the payload part. End-points can use the header part to configure themselves
with user parameters.
