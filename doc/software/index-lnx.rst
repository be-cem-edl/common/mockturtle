..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`sw:lnx`:

====================
Linux Development
====================

This section provides details regarding host development (libraries or
applications) and the Mock Turtle API on a Linux host. References to a
"host" in this section will assume a Linux host because it is the only
supported platform for the time being.

Mock Turtle offers 3 interfaces: a Python module, a C library and a
Linux kernel interface. Users are expected to use either the Python
module or the C library.

.. graphviz::
   :align: center
   :caption: Mock Turtle Linux Interfaces.

   digraph layers {
      concentrate=true;
      node [shape=rectangle, style=filled, penwidth=2];
      edge [dir=both]
      mt [shape=record, label="<d> driver | <l> library | <p> python", fillcolor=lightblue, color=blue];
      usr [label="programs", fillcolor=lightgrey, color=gray];

      usr -> mt:d [style=dotted, color=dimgrey];
      usr -> mt:l;
      usr->mt:p;
   }

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   linux-driver
   linux-library
   linux-python
