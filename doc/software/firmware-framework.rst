..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. highlight:: c

.. _`sw:fw:frm`:

==================================
The Mock Turtle Firmware Framework
==================================

The Mock Turtle firmware framework guides users' development by
keeping them focused on the core logic without the need to deal
with Mock Turtle architectural details.

This API is available by including ``mockturtle-framework.h`` in your source file::

      #include <mockturtle-framework.h>

We recommend this framework to develop Mock Turtle firmware. You should
consider alternatives if you see that it is consuming too much memory or
that the performance is not enough for your application.

.. note::

   This framework internally uses the :ref:`sw:fw:lib`


Application
===========

Firmware developed with this framework needs to be described by
:c:type:`trtl_fw_application`. Firmware applications developed with this
framework do not have a ``main()``. The ``main()`` is implemented within the
framework itself. What you should do instead is to declare a new
:c:type:`trtl_fw_application` named **app** and implement the operations
*init*, *main* and *exit*. These operations are all optional, it means that if
you do not implement them, nothing will be executed.

.. graphviz::
   :align: center
   :caption: Mock Turtle Firmware Framework Phases.

   digraph phases {
      init -> main;
      main -> exit;
   }



Here is a minimal example of a firmware using the framework::

    #include <mockturtle-framework.h>

    static init myinit()
    {
        return 0;
    }

    static int mymain()
    {
        while (1) {
            /* main code here */
        }
        return 0;
    }

    static init myexit()
    {
        return 0;
    }

    struct trtl_application app = {
        .name = "myfirmware",
        .fpga_id_compat = NULL,
        .fpga_id_compat_n = 0,
        .version = {
                .id = 0x12345678,
                .version = RT_VERSION(1, 0),
                .git = GIT_VERSION,
        },

        .init = myinit,
        .main = mymain,
        .exit = myexit,
    };


Since the Mock Turtle is FPGA-based, the firmware is
typically loaded by the host. The procedure is error prone, so it may
happen to load the wrong firmware with unpredictable consequences. To
limit the damage, the ``fpga_id_compat`` can be used to declare a
list of compatible gateware identifiers. The firmware framework will
refuse to execute the firmware when it is not compatible with the
gateware according to the given list. To disable this validation step, do
not provide a compatibility list (like in the example above).

.. doxygenstruct:: trtl_fw_application
   :members:

.. doxygenstruct:: trtl_fw_version
   :members:


Actions
========

The *action* is a function that gets executed when a special message
arrives through a message queue. In order words, an action is similar to a
remote procedure call (RPC).
The firmware framework relies on the :ref:`sw:proto:hmq` to make this work.

The mechanism is quite simple. The Mock Turtle message header has a
dedicated flag to mark a message as an RPC call
:c:macro:`TRTL_HMQ_HEADER_FLAG_RPC`. When this flag is set, the firmware
framework interprets the header’s message-id as action-id. Thus, the
corresponding function gets executed.

This framework supports up to :c:macro:`__TRTL_MSG_ID_MAX` actions.
Part of it (:c:macro:`__TRTL_MSG_ID_MAX_TRTL`) is reserved for internal use,
the rest (:c:macro:`__TRTL_MSG_ID_MAX_USER`) can be used to implement new
actions. The reserved IDs are at the end of the number space and they are
defined in :c:type:`trtl_msg_id`.

The declaration of a new user action consists of 4 steps:

#. enumerate the action IDs, share it with host;

#. implement the function to execute;

#. add the function to the action list, and map it to an action-id.
   The framework uses the message id in :c:type:`trtl_hmq_header`
   as action-id to execute the functions mapped here;

#. export the action list in the :c:type:`trtl_fw_application` data structure;

The framework does not execute actions automatically. Once the actions
are declared, the user must ask the framework to dispatch incoming actions.
This is performed by :c:func:`trtl_fw_mq_action_dispatch`, which listens for
RPC messages on a given mq.

Here is an example of declaring and using an action::

    #include <mockturtle-framework.h>

    /* [POINT 1] */
    enum id_actions {
        MY_ACTION_ID = 0,
        /* ... */
    };

    /* [POINT 2] */
    static int my_action(struct trtl_msg *msg_i, struct trtl_msg *msg_o)
    {
        /* ... */
    }

    /* [POINT 3] */
    static trtl_fw_action_t *actions[] = {
        [MY_ACTION_ID] = my_action,
        /* ... */
    };

    static int mymain()
    {
        int err;

        while (1) {
            /* ... */
            err = trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);
            /* ... */
        }

        return 0;
    }


    /* POINT [4]*/
    struct trtl_application app = {
        /* ... */

        /* Export Actions */
        .actions = actions,
        .n_actions = ARRAY_SIZE(actions),

        .main = mymain,
    };


.. doxygentypedef:: trtl_fw_action_t

.. doxygenfunction:: trtl_fw_mq_action_dispatch

.. _`sw:fw:frm:var`:

Variables
==========

The firmware framework offers the possibility to export local variables
to the host system. Variables must be declared using the
:c:type:`trtl_fw_variable` and then exported by your
:c:type:`trtl_fw_application`.

The meaning of a variable in this context is extended to any memory location:
local variable, Mock Turtle registers, device peripheral registers
and so on.

The framework handles the variable exchange as a special action.
Internally, the framework defines actions to write and to read
variables. This implies that :c:func:`trtl_fw_mq_action_dispatch` must
be used to dispatch incoming actions::

        #include <mockturtle-framework.h>

        #define REGISTER_TAI_SEC (CPU_LR_BASE + 0xC)
        static int var1;
        static int var2;

        struct trtl_fw_variable variables[] = {
            [0] = {
                .addr = (void *)&var1,
                .mask = 0xFFFFFFFF,
                .offset = 0,
                .flags = 0,
            },
            [1] = {
                .addr = (void *)&var2,
                .mask = 0xFFFFFFFF,
                .offset = 0,
                .flags = 0,
            },
            [2] = {
                .addr = (void *)REGISTER_TAI_SEC,
                .mask = 0xFFFFFFFF,
                .offset = 0,
                .flags = 0,
            },
        };

        static int mymain()
        {
            /* ... */

            while (1) {
                trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);
                /* ... */
            }

            return 0;
        }

        struct trtl_fw_application app = {
            /* ... */

            .variables = variables,
            .n_variable = ARRAY_SIZE(variables),

            .main = mymain,
        };

The user can define any number of variables, the firmware framework does
not impose any constraint.

From the host you can read/write the variable by using the
:ref:`tools:mockturtle-variable` tool.

.. highlight:: none

::

        $ mockturtle-variable -D 0xdead -i 0 read 0 1 2
        [0] 0x00000100
        [1] 0x00123fcb
        [2] 0x003f42ca

.. highlight:: c

In chapter :ref:`sw:lnx:lib:hmq` you can find the corresponding host API,
which consists of two functions: :c:func:`trtl_fw_variable_set`
and :c:func:`trtl_fw_variable_get`.

All of the above is handled automatically by the framework.
The user can also send, asynchronously, variables of choice using the function
:c:func:`trtl_fw_mq_send_buf`.

.. doxygenfunction:: trtl_fw_mq_send_uint32

.. _`sw:fw:frm:buf`:

Buffers
========

The firmware framework offers the possibility to export local buffers to
the host system. The buffers must be declared using the
:c:type:`trtl_fw_buffer` and then exported by your
:c:type:`trtl_fw_application`.

The meaning of a buffer in this context is extended to any contiguous
memory location.

The framework handles the buffer exchange as a special action.
Internally, the framework defines actions to write and to read buffers.
This implies that :c:func:`trtl_fw_mq_action_dispatch` must
be used to dispatch incoming actions::

        #include <mockturtle-framework.h>

        static int buf1[32];
        static int buf2[16];

        struct trtl_fw_buffer buffers[] = {
            [0] = {
                .buf = buf1,
                .size = sizeof(buf1),
            },
            [1] = {
                .buf = buf2,
                .size = sizeof(buf2),
            },

        };

        static int mymain()
        {
            /* ... */

            while (1) {
                trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);
                /* ... */
            }

            return 0;
        }

        struct trtl_fw_application app = {
            /* ... */

            .buffers = buffers,
            .n_buffer = ARRAY_SIZE(buffers),

            .main = mymain,
        };

The user can define any number of buffers, the firmware framework does
not impose any constraint.

From the host you can read/write the buffer by using the
:ref:`tools:mockturtle-buffer` tool.

.. highlight:: none

::

        $ mockturtle-buffer -D 0xdead -i 0 read 0 32 1 16
        Buffer 0 (32 bytes)

        0000 : 0x00000001 0x34597332 0x12393903 0xf423a4c4
        0004 : 0x00000005 0x32432ffe 0x432bbff3 0x2232342b

        Buffer 1 (16 bytes)

        0000 : 0x03fffffe 0x07fffffe 0x0ffffffe 0x1ffffffe

.. highlight:: c

In chapter :ref:`sw:lnx:lib:hmq` you can find the corresponding host
API, which consists of two functions: :c:func:`trtl_fw_buffer_set` and
:c:func:`trtl_fw_buffer_get`.

All of the above is handled automatically by the framework.  The user
can also send, asynchronously, buffers of choice using the function
:c:func:`trtl_fw_mq_send_buf`.

.. doxygenfunction:: trtl_fw_mq_send_buf

Miscellaneous
=============

The following is a list of miscellaneous, helper functions.

.. doxygenfunction:: trtl_fw_time

.. doxygenfunction:: trtl_fw_message_error
