..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

:orphan:

.. _wbgen_csr:

Control/Status Registers (CSR)
==============================

This is a list of all the control and status registers, which are accessible
from the Host. There is one CSR for the whole MT.

.. raw:: html
  :file: wbgen/mt_cpu_csr.html

.. raw:: latex
  :file: wbgen/mt_cpu_csr.tex
