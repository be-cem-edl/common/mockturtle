..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. highlight:: none

===============
Register Tables
===============

.. toctree::
   :maxdepth: 1
   :caption: Contents

   wbgen_csr
   wbgen_lr

Mock Turtle Top Level Layout
----------------------------

Memory map from the host point of view:

.. list-table::
   :header-rows: 1
   :widths: 50 50

   * - Name
     - Offset

   * - :ref:`Control Status Register <wbgen_csr>`
     - 0x00000C00

   * - Configuration Rom
     - 0x00000E00

   * - Shared Memory
     - 0x00010000


Memory map from the soft-CPU point of view:

.. list-table::
   :header-rows: 1
   :widths: 50 50

   * - Name
     - Offset

   * - :ref:`Local Register <wbgen_lr>`
     - 0x00100000

   * - Shared Memory
     - 0x40200000
