"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

import os
import pytest
import PyMockTurtle
import time

@pytest.fixture
def firmware_file_config(fw_locator):
    return fw_locator.get_path("firmware/config_rom/fw-config-rom.bin")


class TestConfig(object):
    def test_exist(self, devid):
        assert os.path.exists("/sys/class/mockturtle/trtl-{:04x}/config-rom".format(devid)) == True

    def test_valid_host(self, trtl_device, cfg):
        assert trtl_device.rom.signature == cfg.signature
        assert ((trtl_device.rom.app_id == cfg.app_id | ord('C')) or
                (trtl_device.rom.app_id == cfg.app_id | ord('V'))) # SVEC or SPEC DEMO
        assert trtl_device.rom.n_cpu == cfg.n_cpu

    def test_valid_softcpu(self, trtl_cpu, cfg, firmware_file_config):
        trtl_cpu.load_application_file(firmware_file_config)
        trtl_cpu.enable()

        # Give time to the soft-CPU to build and send the message
        time.sleep(0.1)

        msg = trtl_cpu.hmq[0].recv_msg()

        assert msg is not None
        assert msg.payload[0] == cfg.signature
        assert ((msg.payload[5] == cfg.app_id | ord('C')) or
                (msg.payload[5] == cfg.app_id | ord('V'))) # SVEC or SPEC DEMO
        assert msg.payload[6] == cfg.n_cpu
