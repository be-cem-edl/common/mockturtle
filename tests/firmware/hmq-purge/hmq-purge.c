/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>

int main()
{
	int cpu, hmq;
	const struct trtl_config_rom *cfgrom = trtl_config_rom_get();
	uint32_t val;

	pr_debug("PURGE MESSAGES\r\n");
	pr_debug("Fill the HMQ while CPU is not running\r\n");

	mdelay(100);
	cpu = trtl_get_core_id();
	for (hmq = 0; hmq < cfgrom->n_hmq[cpu]; ++hmq) {
		val = mq_poll_in(TRTL_HMQ, 1 << hmq);
		if (!val) {
			pr_error("CPU-%d HMQ-%d after poll (1): 0x%"PRIx32"\r\n",
				 cpu, hmq, val);
			return -1;
		}

		mq_purge(TRTL_HMQ, hmq);

		val = mq_poll_in(TRTL_HMQ, 1 << hmq);
		if (val) {
			pr_error("CPU-%d HMQ-%d after poll (2): 0x%"PRIx32"\r\n",
				 cpu, hmq, val);
			return -1;
		}
	}

	val = mq_poll_in(TRTL_HMQ, ~0);
	if (val != 0) {
		pr_error("CPU-%d END poll: 0x%"PRIx32"\r\n",
			 cpu, val);
		return -1;
	}

	pp_printf("OK\r\n");
	return 0;
}
