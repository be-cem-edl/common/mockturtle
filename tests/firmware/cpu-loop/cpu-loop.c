/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
int main()
{
	int sum = 0;

	while (1)
		sum ++;

	return 0;
}
