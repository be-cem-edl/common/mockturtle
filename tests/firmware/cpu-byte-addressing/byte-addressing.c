/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>


int byte_address_test_8(void *mem, size_t size)
{
	uint8_t *data = mem;
	int i, dw = 1;

	for (i = 0; i < size / dw; ++i)
		data[i] = (i & 0xFF); /* ignored by the HW on
					 input buffer */
	for (i = 0; i < size / dw; ++i) {
		if (data[i] != (i & 0xFF)) {
			pr_error("Failure at %p (%d) dw: %d (0x%x != 0x%x)\r\n",
				 &data[i], i, dw,
				 data[i], (i & 0xFF));
			while (i++ < 4)
				pp_printf("0x%x 0x%x\r\n", data[i], i);

			return -1;
		}
	}

	return 0;
}

int byte_address_test_16(void *mem, size_t size)
{
	uint16_t *data = mem;
	int i, dw = 2;

	for (i = 0; i < size / dw; ++i)
		data[i] = (i & 0xFFFF); /* ignored by the HW on
					   input buffer */
	for (i = 0; i < size / dw; ++i) {
		if (data[i] != (i & 0xFFFF)) {
			pr_error("Failure at %p (%d) dw: %d (0x%x != 0x%x)\r\n",
				 &data[i], i, dw,
				 data[i], (i & 0xFFFF));
			while (i++ < 4)
				pp_printf("0x%x 0x%x\r\n", data[i], i);

			return -1;
		}
	}

	return 0;
}

int byte_address_test_32(void *mem, size_t size)
{
	uint32_t *data = mem;
	int i, dw = 4;

	for (i = 0; i < size / dw; ++i)
		data[i] = (i & 0xFFFFFFFF); /* ignored by the HW on
					       input buffer */
	for (i = 0; i < size / dw; ++i) {
		if (data[i] != (i & 0xFFFFFFFF)) {
			pr_error("Failure at %p (%d) dw: %d (0x%lx != 0x%x)\r\n",
				 &data[i], i, dw,
				 data[i], (i & 0xFFFFFFFF));
			while (i++ < 4)
				pp_printf("0x%"PRIx32" 0x%x\r\n", data[i], i);

			return -1;
		}
	}

	return 0;
}

static int (*func[])(void *mem, size_t size) = {
	byte_address_test_32,
	byte_address_test_16,
	byte_address_test_8,
};


static int byte_addressing_test(void *mem, size_t size)
{
	int err, i;

	for (i = 0; i < ARRAY_SIZE(func); ++i) {
		err = func[i](mem, size);
		if (err)
			return err;
	}

	return 0;
}

static int byte_addressing_test_mq(enum trtl_mq_type type,
				   unsigned int cpu, unsigned int hmq)
{
	const struct trtl_config_rom *cfg = trtl_config_rom_get();
	uint32_t size, status;
	int err, g_err = 0, i;
	char c = (type == TRTL_HMQ ? 'H' : 'R');


	mq_claim(type, hmq);
	size = TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(cfg->rmq[cpu][hmq].sizes);
	err = byte_addressing_test(mq_map_out_buffer(type, hmq),
				   size);
	g_err |= err;
	if (err)
		pr_error("Failed, %cMQ[%d] out payload\r\n", c, hmq);

	size = TRTL_CONFIG_ROM_MQ_SIZE_HEADER(cfg->hmq[cpu][hmq].sizes);
	err = byte_addressing_test(mq_map_out_header(type, hmq),
				   size);
	g_err |= err;
	if (err)
		pr_error("Failed, %cMQ[%d] out header\r\n", c, hmq);

	if (type == TRTL_RMQ)
		goto out; /* can't check input for RMQ */

	for (i = 0; i < ARRAY_SIZE(func); ++i) {
		status = mq_poll_in_wait(type, 1 << hmq, 1000);
		if (!status) {
			pr_error("\tNO MESSAGE PENDING %cMQ%d\r\n",
				 c, hmq);
			return -1;
		}
		break; /* FIXME do not test input - somehting wrong with the test */
		err = func[i](mq_map_in_header(type, hmq), size);
		g_err |= err;
		if (err)
			pr_error("Failed, %cMQ[%d] in header\r\n", c, hmq);
		err = func[i](mq_map_in_buffer(type, hmq), size);
		g_err |= err;
		if (err)
			pr_error("Failed, %cMQ[%d] in payload\r\n", c, hmq);

		mq_discard(TRTL_HMQ, hmq);
	}

out:
	mq_purge(type, hmq);

	return g_err;
}

static int byte_addressing_test_shm(void)
{
	const struct trtl_config_rom *cfg = trtl_config_rom_get();
	void *smem_buf = (void *)TRTL_ADDR_SHM_BASE;
	int err;

	err = byte_addressing_test(smem_buf, cfg->smem_size);
	if (err)
		pr_error("Failed, shared memory\r\n");
	return err;
}

int main()
{
	int cpu, i, g_err = 0;
	const struct trtl_config_rom *cfg = trtl_config_rom_get();

	pr_debug("BYTE ADDRESSING\r\n");

	cpu = trtl_get_core_id();
	g_err |= byte_addressing_test_shm();
	for (i = 0; i < cfg->n_rmq[cpu]; ++i)
		g_err |= byte_addressing_test_mq(TRTL_RMQ, cpu, i);
	for (i = 0; i < cfg->n_hmq[cpu]; ++i)
		g_err |= byte_addressing_test_mq(TRTL_HMQ, cpu, i);

	if (!g_err)
		pp_printf("OK\r\n");
	return g_err;
}
