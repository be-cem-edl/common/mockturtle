/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>

int main()
{
	pp_printf("this is a message\r\n");
	pr_debug("that goes through the serial interface\n\r");
	pr_error("and we use it for testing\r\n");
	return 0;
}
