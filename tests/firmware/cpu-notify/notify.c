/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>

int main()
{
	int i, k;

	pr_debug("NOTIFICATION\r\n");

	for (k = 0; k < 10; ++k) {
		for (i = 0; i < __TRTL_CPU_NOTIFY_APPLICATION_MAX; ++i) {
			mdelay(1);
			trtl_notify_user(i);
		}
		for (i = TRTL_CPU_NOTIFY_APPLICATION; i < __TRTL_CPU_NOTIFY_MAX; ++i) {
			mdelay(1);
			trtl_notify(i);
		}
	}

	pp_printf("OK\r\n");
	return 0;
}
