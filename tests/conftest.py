"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

import os
import PyMockTurtle
import pytest
import time

class FirmwareLocator:
    @staticmethod
    def get_path(fw):
        _dir_name, _file_name = os.path.split(fw)
        _module_path = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
        if pytest.fw_path:
            return os.path.join(pytest.fw_path, _file_name)
        return os.path.join(_module_path, _dir_name, _file_name)

@pytest.fixture(scope="module")
def fw_locator():
    return FirmwareLocator

@pytest.fixture(scope="module")
def devid():
    return pytest.dev_id

def build_default_cfg():
    config = PyMockTurtle.TrtlConfig()
    config.signature = PyMockTurtle.TrtlConfig.TRTL_CONFIG_ROM_SIGNATURE
    config.version = 0
    config.pad = 0
    config.clock_freq = 0
    config.flags = 0
    config.app_id = 0x4d544400 # Common part of app_id for SPEC and SVEC demo
    config.n_cpu = 2
    config.smem_size = 0x00002000
    config.n_hmq[0] = 2
    config.n_hmq[1] = 2
    config.n_rmq[0] = 1
    config.n_rmq[1] = 1

    return config


@pytest.fixture(scope="module")
def cfg():
    return build_default_cfg()

@pytest.fixture(scope="module", params=range(build_default_cfg().n_cpu))
def cpu_idx(request):
    return request.param


@pytest.fixture(scope="module")
def trtl_device(devid):
    dev = PyMockTurtle.TrtlDevice(devid)
    return dev

@pytest.fixture(scope="function", params=range(build_default_cfg().n_cpu))
def trtl_cpu(request, trtl_device):
    trtl_cpu_l = trtl_device.cpu[request.param]

    trtl_cpu_l.disable()
    for hmq in trtl_cpu_l.hmq:
        hmq.flush()

    yield trtl_device.cpu[request.param]

    trtl_cpu_l.disable()
    for hmq in trtl_cpu_l.hmq:
        hmq.flush()

@pytest.fixture(scope="module")
def trtl_shm(devid):
    dev = PyMockTurtle.TrtlDevice(devid)
    return dev.shm

@pytest.fixture(scope="module", params=[2**x for x in range(5)])
def trtl_msg(request):
    """This provides a list of test messages. Each test message contains
    two values (so len = 2): a sequence number, the maximum number in the
    sequence. These two values in the last message are equal
    """
    msg_list = []
    for val in range(request.param):
        msg = PyMockTurtle.TrtlMessage()
        msg.header.len = 2
        msg.payload[0] = val + 1
        msg.payload[1] = request.param
        msg_list.append(msg)
    return msg_list

@pytest.fixture(scope="module")
def trtl_version_exp():
    v = PyMockTurtle.TrtlFirmwareVersion()
    v.fw_id = 0xFEDE
    v.fw_version = 0x1
    v.git_version = 0x12345678

    return v

def pytest_addoption(parser):
    parser.addoption("--id", type=lambda x: int(x, 0),
                     required=True, help="Mockturtle identifier")
    parser.addoption("--fw-path", default=None,
                     help="Path to test firmware. Defaults to source locations.")

def pytest_configure(config):
    pytest.dev_id = config.getoption("--id")
    pytest.fw_path = config.getoption("--fw-path")
