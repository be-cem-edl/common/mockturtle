"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

import os
import PyMockTurtle
import pytest
import serial
import time
import multiprocessing
import errno

@pytest.fixture
def trtl_binary_hmq_purge(fw_locator):
    return fw_locator.get_path("firmware/hmq-purge/fw-hmq-purge.bin")

@pytest.fixture
def trtl_binary_hmq_async_send(fw_locator):
    return fw_locator.get_path("firmware/hmq-async-send/fw-hmq-async-send.bin")

@pytest.fixture
def trtl_binary_hmq_async_recv(fw_locator):
    return fw_locator.get_path("firmware/hmq-async-recv/fw-hmq-async-recv.bin")

@pytest.fixture
def trtl_binary_hmq_sync(fw_locator):
    return fw_locator.get_path("firmware/hmq-sync/fw-hmq-sync.bin")


def do_sync_mult(q, hmq, msg):
    try:
        q.put(hmq.sync_msg(msg))
    except Exception as e:
        print(e)
        q.put(None)

def do_sync_no_multone(q, hmq):
    try:
        msg_r = hmq.sync_msg(PyMockTurtle.TrtlMessage())
        q.put(0)
    except OSError as err:
        q.put(err.errno)
    except Exception as e:
        print(e)
        q.put(None)

class TestHmq(object):
    confirm = 'OK\r\n'

    @pytest.mark.parametrize("pattern,nmsg", [(0x11111111, 1),
                                              (0x88888888, 8),
                                              (0xAAAAAAAA, 16),
                                              ])
    def test_purge(self, trtl_cpu, trtl_binary_hmq_purge, pattern, nmsg):
        msg = PyMockTurtle.TrtlMessage()
        msg.header.len = 1
        msg.payload[0] = pattern

        filename = "/dev/ttytrtl-{:04x}-{:d}".format(trtl_cpu.trtl_dev.device_id,
                                                     trtl_cpu.idx_cpu)
        with serial.Serial(filename) as ser:
            ser.reset_input_buffer()

            trtl_cpu.load_application_file(trtl_binary_hmq_purge)
            trtl_cpu.enable()

            for hmq in trtl_cpu.hmq:
                for n in range(nmsg):
                    hmq.send_msg(msg)

            time.sleep(1)

            assert ser.in_waiting >= len(self.confirm)
            assert self.confirm == ser.read(ser.in_waiting).decode()

    def test_async_send(self, trtl_cpu, trtl_msg, trtl_binary_hmq_async_send):
        """It sends the test messages on all available HMQ.

        It validates:
        - messages are correctly sent
        - statistic counter increase coherently

        The test is successful when we read "OK" from the serial console
        """
        trtl_cpu.load_application_file(trtl_binary_hmq_async_send)
        filename = "/dev/ttytrtl-{:04x}-{:d}".format(trtl_cpu.trtl_dev.device_id,
                                                     trtl_cpu.idx_cpu)
        with serial.Serial(filename) as ser:
            ser.reset_input_buffer()
            trtl_cpu.enable()

            for hmq in trtl_cpu.hmq:
                for msg in trtl_msg:
                    sb = hmq.get_stats()
                    hmq.send_msg(msg)
                    time.sleep(0.01)
                    sa = hmq.get_stats()
                    assert sa["message_sent"] - sb["message_sent"] == 1

            time.sleep(0.1)

            assert ser.in_waiting >= len(self.confirm)
            assert self.confirm == ser.read(ser.in_waiting).decode()

    def test_async_recv(self, trtl_cpu, trtl_binary_hmq_async_recv):
        """It receives messages on all available HMQ.

        It validates:
        - messages are correctly received
        - statistic counter increase coherently

        The test is succesful if the received message content is the expected
        one
        """
        sb = {}
        for hmq in trtl_cpu.hmq:
            sb[hmq] = hmq.get_stats()

        trtl_cpu.load_application_file(trtl_binary_hmq_async_recv)
        trtl_cpu.enable()
        time.sleep(0.5)

        for hmq in trtl_cpu.hmq:
            tot = trtl_cpu.trtl_dev.rom.hmq[trtl_cpu.idx_cpu][hmq.idx_hmq].entries
            sa = hmq.get_stats()
            assert sa["message_received"] - sb[hmq]["message_received"] == tot

        for hmq in trtl_cpu.hmq:
            tot = trtl_cpu.trtl_dev.rom.hmq[trtl_cpu.idx_cpu][hmq.idx_hmq].entries
            for n in range(tot):
                msg = hmq.recv_msg()
                assert msg is not None
                payload = list(msg.payload)
                for i, val in enumerate(payload):
                    if i >= msg.header.len:
                        break
                    assert i == val

    @pytest.mark.parametrize("nproc", range(1, 5))
    def test_sync_no_multone(self, trtl_cpu, nproc):
        """The driver should not accept more than 1 sync message at time
        from the same user. The test is successful when the driver refused
        a second message"""

        for hmq in trtl_cpu.hmq:
            proc = []
            for n in range(nproc):
                q = multiprocessing.Queue()
                p = multiprocessing.Process(target=do_sync_no_multone,
                                            args=(q, hmq))
                proc.append((q, p))
                p.start()

            errs = []
            for q, p in proc:
                errs.append(q.get())
                p.join()
            assert errs.count(0) == 0
            assert errs.count(errno.EBUSY) == nproc - 1
            assert errs.count(PyMockTurtle.ETRTL_MSG_SYNC_FAILED_RECV_TIMEOUT) == 1

    def test_sync_mult(self, trtl_cpu, trtl_msg, trtl_binary_hmq_sync):
        """Test Multiprocess sync messages. The firmware will copy back
        the message as answer"""
        trtl_cpu.load_application_file(trtl_binary_hmq_sync)
        trtl_cpu.enable()

        for hmq_orig in trtl_cpu.hmq:
            proc = []
            sb = hmq_orig.get_stats()
            for msg in trtl_msg:
                # Create separate handle/user for each process
                dev = PyMockTurtle.TrtlDevice(hmq_orig.trtl_dev.device_id)
                hmq = dev.cpu[hmq_orig.idx_cpu].hmq[hmq_orig.idx_hmq]
                q = multiprocessing.Queue()
                p = multiprocessing.Process(target=do_sync_mult,
                                            args=(q, hmq, msg))
                proc.append((q, p, msg))
            # Only start once all handles are open
            for q, p, msg in proc:
                p.start()
            for q, p, msg in proc:
                msg_r = q.get()
                p.join()

                assert msg_r is not None
                assert msg_r.header.rt_app_id == msg.header.rt_app_id
                assert msg_r.header.msg_id == msg.header.msg_id
                assert msg_r.header.len == msg.header.len
                assert msg_r.header.flags == PyMockTurtle.TrtlHmqHeader.TRTL_HMQ_HEADER_FLAG_ACK
                for v1, v2 in zip(msg.payload, msg_r.payload):
                    assert v1 == v2
            sa = hmq_orig.get_stats()
            assert sa["message_sent"] - sb["message_sent"] == len(trtl_msg)
            assert sa["message_received"] - sb["message_received"] == len(trtl_msg)

    def test_sync(self, trtl_cpu, trtl_msg, trtl_binary_hmq_async_send):
        """It sends the test messages on all available HMQ.
        The test is successful when what we read back is the same we sent
        """
        trtl_cpu.load_application_file(trtl_binary_hmq_async_send)
        trtl_cpu.enable()

        for hmq in trtl_cpu.hmq:
            for msg in trtl_msg:
                sb = hmq.get_stats()
                msg_r = hmq.sync_msg(msg)
                sa = hmq.get_stats()
                assert sa["message_sent"] - sb["message_sent"] == 1
                assert sa["message_received"] - sb["message_received"] == 1

                assert msg_r.header.rt_app_id == msg.header.rt_app_id
                assert msg_r.header.msg_id == msg.header.msg_id
                assert msg_r.header.len == msg.header.len
                assert msg_r.header.flags == PyMockTurtle.TrtlHmqHeader.TRTL_HMQ_HEADER_FLAG_ACK
                for v1, v2 in zip(msg.payload, msg_r.payload):
                    assert v1 == v2

                msg_r = hmq.recv_msg()
                assert msg_r is None
