"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

import os

class TestDevice(object):
    def test_exist(self, devid):
        assert os.path.isdir("/sys/class/mockturtle/trtl-{:04x}".format(devid)) == True
