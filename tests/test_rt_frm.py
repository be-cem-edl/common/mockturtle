"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

import os
import pytest
import PyMockTurtle
import random
import time

@pytest.fixture
def trtl_binary_frm(fw_locator):
    return fw_locator.get_path("firmware/rt-frm/fw-rt-frm.bin")


class TestFirmwareFramework(object):
    def test_var(self, trtl_cpu, trtl_binary_frm):
        trtl_cpu.load_application_file(trtl_binary_frm)
        trtl_cpu.enable()

        var = PyMockTurtle.TrtlFirmwareVariable(trtl_cpu.hmq[0])

        assert var[0] == 0x55AA55AA
        assert var[1] == 0xFEDE0786

        a = random.randint(0, 4096)
        b = random.randint(0, 4096)
        var[0] = a
        var[1] = b

        assert a == var[0]
        assert b == var[1]

    def test_buf(self, trtl_cpu, trtl_binary_frm):
        trtl_cpu.load_application_file(trtl_binary_frm)
        trtl_cpu.enable()

        # TODO implement the Python support

    def test_name(self, trtl_cpu, trtl_binary_frm):
        trtl_cpu.load_application_file(trtl_binary_frm)
        trtl_cpu.enable()

        assert trtl_cpu.name() == "testfrm"


    def test_ping(self, trtl_cpu, trtl_binary_frm):
        trtl_cpu.load_application_file(trtl_binary_frm)
        trtl_cpu.enable()

        assert trtl_cpu.ping() == True

    def test_ver(self, trtl_cpu, trtl_version_exp, trtl_binary_frm):
        trtl_cpu.load_application_file(trtl_binary_frm)
        trtl_cpu.enable()

        assert trtl_cpu.version() == trtl_version_exp
