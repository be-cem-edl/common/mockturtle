"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

import hashlib
import os
import pytest
import serial
import time

import PyMockTurtle

@pytest.fixture
def firmware_file_loop(fw_locator):
    return fw_locator.get_path("firmware/cpu-loop/fw-loop.bin")

@pytest.fixture
def firmware_binary_byte_addressing(fw_locator):
    return fw_locator.get_path("firmware/cpu-byte-addressing/fw-byte-addressing.bin")

@pytest.fixture
def firmware_binary_notification(fw_locator):
    return fw_locator.get_path("firmware/cpu-notify/fw-notify.bin")

class TestCPU(object):
    confirm = 'OK\r\n'

    def test_cpu(self):
        pass

    def test_exist(self, trtl_cpu):
        path = "/dev/mockturtle/trtl-{:04x}-{:02d}".format(trtl_cpu.trtl_dev.device_id,
                                                           trtl_cpu.idx_cpu)
        assert True == os.path.exists(path)

    def test_enable_disable(self, trtl_cpu):
        trtl_cpu.enable()
        assert True == trtl_cpu.is_enable()
        trtl_cpu.disable()
        assert False == trtl_cpu.is_enable()
        trtl_cpu.enable()
        assert True == trtl_cpu.is_enable()

    def test_load(self, trtl_cpu, firmware_file_loop):
        trtl_cpu.load_application_file(firmware_file_loop)
        trtl_cpu.enable()

        time.sleep(1)

        trtl_cpu.disable()
        trtl_cpu.dump_application_file("/tmp/dump.bin")

        with open(firmware_file_loop, 'rb') as f:
            data_o = f.read()
            md5_o = hashlib.md5(data_o).hexdigest()

        with open("/tmp/dump.bin", 'rb') as f:
            data_d = f.read(len(data_o))
            md5_d = hashlib.md5(data_d).hexdigest()

        assert md5_o == md5_d

    def test_byte_addressing(self, trtl_cpu, firmware_binary_byte_addressing):
        trtl_cpu.load_application_file(firmware_binary_byte_addressing)

        for hmq in trtl_cpu.hmq:
            hmq.flush()
            msg = PyMockTurtle.TrtlMessage()
            msg.header.len = trtl_cpu.trtl_dev.rom.hmq[trtl_cpu.idx_cpu][hmq.idx_hmq].payload_size
            for i in range(msg.header.len):  # 32bit DW
                msg.payload[i] = i
            hmq.send_msg(msg)

            for i in range(msg.header.len):  # 16bit DW
                val = (i * 2)
                msg.payload[i] = (((val + 1) << 16) & 0xFFFF0000) | \
                                 (((val + 0) << 0)  & 0x0000FFFF)
            hmq.send_msg(msg)

            for i in range(msg.header.len):  # 8bit DW
                val = (i * 4)
                msg.payload[i] = (((val + 3) << 24) & 0xFF000000) | \
                                 (((val + 2) << 16) & 0x00FF0000) | \
                                 (((val + 1) << 8)  & 0x0000FF00) | \
                                 (((val + 0) << 0)  & 0x000000FF)
            hmq.send_msg(msg)

        filename = "/dev/ttytrtl-{:04x}-{:d}".format(trtl_cpu.trtl_dev.device_id,
                                                     trtl_cpu.idx_cpu)
        with serial.Serial(filename) as ser:
            ser.reset_input_buffer()
            trtl_cpu.enable()
            time.sleep(5)
            assert ser.in_waiting >= len(self.confirm)
            assert self.confirm == ser.read(ser.in_waiting).decode()

    def test_notification(self, trtl_cpu, firmware_binary_notification):
        trtl_cpu.load_application_file(firmware_binary_notification)
        trtl_cpu.enable()
        time.sleep(1)
