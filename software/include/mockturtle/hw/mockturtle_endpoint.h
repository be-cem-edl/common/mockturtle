/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 */


#ifndef __TRTL_ENDPOINT_H
#define __TRTL_ENDPOINT_H

enum trtl_ep_type {
	TRTL_EP_ETH = 0,
	__TRTL_EP_MAX,
};

#define TRTL_EP_OUT_CONFIG	 0
#define TRTL_EP_OUT_DST_MAC_HI	 4
#define TRTL_EP_OUT_DST_MAC_LO	 8
#define TRTL_EP_OUT_VLAN_ID	12
#define TRTL_EP_OUT_ETHERTYPE	16
#define TRTL_EP_OUT_DST_IP	20
#define TRTL_EP_OUT_DST_PORT	24
#define TRTL_EP_OUT_SRC_IP	28
#define TRTL_EP_OUT_SRC_PORT	32

#define TRTL_EP_FILTER_RAW	 (1<<0)
#define TRTL_EP_FILTER_UDP	 (1<<1)
#define TRTL_EP_FILTER_DST_MAC   (1<<2)
#define TRTL_EP_FILTER_VLAN	 (1<<3)
#define TRTL_EP_FILTER_VLAN_DIS	 (1<<4)
#define TRTL_EP_FILTER_ETHERTYPE (1<<5)
#define TRTL_EP_FILTER_DST_IP	 (1<<6)
#define TRTL_EP_FILTER_DST_PORT  (1<<7)
#define TRTL_EP_FILTER_ENABLE	 (1<<31)

#define TRTL_EP_IN_CONFIG	 0
#define TRTL_EP_IN_DST_MAC_HI	 4
#define TRTL_EP_IN_DST_MAC_LO	 8
#define TRTL_EP_IN_VLAN_ID	12
#define TRTL_EP_IN_ETHERTYPE	16
#define TRTL_EP_IN_DST_IP	20
#define TRTL_EP_IN_DST_PORT	24

#endif
