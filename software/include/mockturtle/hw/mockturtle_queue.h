/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 */


/*.
 * Mock Turtle
 *
 * mqueue.h: MQ register definitions (Host side)
 */

#ifndef __TRTL_MQUEUE_H
#define __TRTL_MQUEUE_H

// Incoming slot base address, relative to BASE_HMQ
#define TRTL_MQ_BASE_IN  0x0000
// Outgoung slot base address, relative to BASE_HMQ
#define TRTL_MQ_BASE_OUT 0x8000

// MQ slot registers, relative to the base address of each slot: TRTL_MQ_BASE_IN(slot_no) or TRTL_MQ_BASE_OUT(slot_no)
#define TRTL_MQ_SLOT_COMMAND 0
// Status register
#define TRTL_MQ_SLOT_STATUS 4
// Start of header block
#define TRTL_MQ_SLOT_HEADER_START 0x1000
// Start of data block
#define TRTL_MQ_SLOT_DATA_START   0x2000
// Start of endpoint control registers (RMQ only)
#define TRTL_MQ_SLOT_ENDPOINT_CONFIG_START 0x1800

// Layout of TRTL_MQ_SLOT_COMMAND register:

// Claim: prepares a slot to send a message (w/o)
#define TRTL_MQ_CMD_CLAIM (1<<24)

// Purge: erases all messages from a slot (w/o)
#define TRTL_MQ_CMD_PURGE (1<<25)

// Ready: pushes the message to the queue. (w/o)
#define TRTL_MQ_CMD_READY (1<<26)

// Discard: removes last message from the queue, advancing to the next one (w/o)
#define TRTL_MQ_CMD_DISCARD (1<<27)

// Size of the message to be sent, in words (w/o). Must be written together with the
// READY command, e.g.:
// writel (TRTL_MQ_CMD_READY | 10, TRTL_MQ_SLOT_COMMAND);
#define TRTL_MQ_CMD_MSG_SIZE_MASK 0xff
#define TRTL_MQ_CMD_MSG_SIZE_SHIFT 0

// Layout of TRTL_MQ_SLOT_STATUS register:

// [0] Slot is full
#define TRTL_MQ_SLOT_STATUS_FULL (1<<0)
// [1] Slot is empty
#define TRTL_MQ_SLOT_STATUS_EMPTY (1<<1)

// [15:8] Number of occupied entries
#define TRTL_MQ_SLOT_STATUS_OCCUPIED_SHIFT 8
#define TRTL_MQ_SLOT_STATUS_OCCUPIED_MASK  0xff00

#endif
