/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#ifndef __TRTL_ADDRESSES_H
#define __TRTL_ADDRESSES_H

#ifdef __KERNEL__

/* Host addresses.  */
#define TRTL_ADDR_OFFSET_HMQ        0x00000000
#define TRTL_ADDR_OFFSET_CSR        0x0000C000
 /* FIXME update memorymap to have DBG PAGE_ALIGNED */
#define TRTL_ADDR_OFFSET_DBG        TRTL_ADDR_OFFSET_CSR
#define TRTL_ADDR_OFFSET_CONFIG_ROM 0x0000E000
#define TRTL_ADDR_OFFSET_SHM        0x00010000

#endif /* __KERNEL__ */

#ifdef __TRTL_FIRMWARE__
/* Softcpu addresses.  */

/**
 * CPU Local Registers base address
 */
#define TRTL_ADDR_LR_BASE              0x00100000

#define TRTL_ADDR_HMQ_BASE             0x00200000
#define TRTL_ADDR_RMQ_BASE             0x00300000

/**
 * Dedicated Peripheral base address.
 * All the Dedicated Paripherals will be behind this address
 */
#define TRTL_ADDR_DP_BASE              0x20000000

#define TRTL_ADDR_SI_BASE              0x40000000

#define TRTL_ADDR_SHM_BASE             0x40200000
#define TRTL_ADDR_CONFIG_ROM_BASE      0x40400000


/**
 * Give a nice pointer to the given register in the dedicated peripheral
 */
#define TRTL_ADDR_DP(_offset) ((void *)(TRTL_ADDR_DP_BASE + (_offset)))

/**
 * Give a nice pointer to the given register in the local register area
 */
#define TRTL_ADDR_LR(_offset) ((void *)(TRTL_ADDR_LR_BASE + (_offset)))

/**
 *
 */
#define TRTL_ADDR_SI(_offset) ((void *)(TRTL_ADDR_SI_BASE + (_offset)))
#endif /* __TRTL_FIRMWARE__ */


#endif
