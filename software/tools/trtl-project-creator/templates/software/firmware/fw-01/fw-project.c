/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: <year> <owner>
 *
 * FIXME choose your license and set the copyright
 */

#include <string.h>
#include <mockturtle-framework.h>
#include <{{short_name}}-common.h>
#include <fw-{{short_name}}-common.h>

enum hmq_slot_name {
	{{short_name_capital}}_CMD = 0,
};


static struct trtl_fw_variable variables[] = {
	/* put here variables that you want to share */
};


static struct trtl_fw_buffer buffers[] = {
	/* put here data structures that you want to share */
};


static trtl_fw_action_t *actions[] = {
	/* Add your actions here */
};


/**
 * Initialization code
 */
static int {{short_name}}_init()
{
	return 0;
}


/**
 * Well, the main :)
 */
static int {{short_name}}_main()
{
	while (1) {
		/*
		 * Handle all messages incoming from slot
		 * {{short_name_capital}}_CMD as actions
		 */
		trtl_fw_mq_action_dispatch(TRTL_HMQ, {{short_name_capital}}_CMD);
	}

	return 0;
}


/**
 * Exit code
 */
static int {{short_name}}_exit()
{
	return 0;
}


struct trtl_fw_application app = {
	.name = "{{short_name}}",
	.version = {
		.rt_id = CONFIG_RT_APPLICATION_ID,
		.rt_version = RT_VERSION(0, 1),
		.git_version = GIT_VERSION
	},
	.buffers = buffers,
	.n_buffers = ARRAY_SIZE(buffers),

	.variables = variables,
	.n_variables = ARRAY_SIZE(variables),

	.actions = actions,
	.n_actions = ARRAY_SIZE(actions),

	.init = {{short_name}}_init,
	.main = {{short_name}}_main,
	.exit = {{short_name}}_exit,
};
