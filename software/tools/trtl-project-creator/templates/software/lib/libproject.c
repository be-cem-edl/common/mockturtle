/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: <year> <owner>
 *
 * FIXME choose your license and set the copyright
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <mockturtle/libmockturtle.h>
#include <lib{{short_name}}-internal.h>

/**
 * List of error messages corresponding to the error codes.
 */
static const char *{{short_name}}_errors[] = {

};


/**
 * Return a string messages corresponding to a given error code. If
 * it is not a libwrtd error code, it will run trtl_strerror()
 * @param[in] err error code
 * @return a message error
 */
const char *{{short_name}}_strerror(unsigned int err)
{
	if (err < __E{{short_name_capital}}_MIN_ERROR_NUMBER ||
	    err >= __E{{short_name_capital}}_MAX_ERROR_NUMBER)
		return trtl_strerror(err);

	return {{short_name}}_errors[err - __E{{short_name_capital}}_MIN_ERROR_NUMBER];
}


/**
 * Initialize the {{name}} library. It must be called before doing
 * anything else.
 * This library is based on the libmockturtle, so internally, this function also
 * run trtl_init() in order to initialize the Mock Turtle library.
 * @return 0 on success, otherwise -1 and errno is appropriately set
 */
int {{short_name}}_init()
{
	int err;

	err = trtl_init();
	if (err)
		return err;

	return 0;
}


/**
 * Release the resources allocated by {{short_name}}_init(). It must be called when
 * you stop to use this library. Then, you cannot use functions from this
 * library.
 */
void {{short_name}}_exit()
{
	trtl_exit();
}


/**
 * Open a {{name}} node device using the Mock Turtle ID
 * @param[in] device_id Mock Turtle device identificator
 * @return It returns an anonymous {{short_name}}_node structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
struct {{short_name}}_node *{{short_name}}_open_by_id(uint32_t device_id)
{
	struct {{short_name}}_desc *{{short_name}};

	{{short_name}} = malloc(sizeof(struct {{short_name}}_desc));
	if (!{{short_name}})
		return NULL;

	{{short_name}}->trtl = trtl_open_by_id(device_id);
	if (!{{short_name}}->trtl)
		goto out;

	{{short_name}}->dev_id = device_id;
	return (struct {{short_name}}_node *){{short_name}};

out:
	free({{short_name}});
	return NULL;
}


/**
 * Close a {{name}} device opened with one of the following function:
 *  {{short_name}}_open_by_id()
 * @param[in] dev device token
 */
void {{short_name}}_close(struct {{short_name}}_node *dev)
{
	struct {{short_name}}_desc *{{short_name}} = (struct {{short_name}}_desc *)dev;

	trtl_close({{short_name}}->trtl);
	free({{short_name}});
	dev = NULL;
}


/**
 * Return the Mock Turtle token in order to allows users to run
 * functions from the Mock Turtle library
 * @param[in] dev device token
 * @return the Mock Turtle token
 */
struct trtl_dev *{{short_name}}_get_trtl_dev(struct {{short_name}}_node *dev)
{
	struct {{short_name}}_desc *{{short_name}} = (struct {{short_name}}_desc *)dev;

	return (struct trtl_dev *)({{short_name}}->trtl);
}
