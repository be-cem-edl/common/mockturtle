/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <mockturtle/libmockturtle.h>


/**
 * Print the help message
 * @param[in] name the program name
 */
static void help(char *name)
{
	fprintf(stderr, "\n");
	fprintf(stderr, "%s [options]\n\n", name);
	fprintf(stderr, "Show the Mock Turtle devices available on the system.\n\n");
	fprintf(stderr, "-v   show more information\n");
	fprintf(stderr, "-h   show this help\n");
	fprintf(stderr, "\n");
	exit(1);
}


/**
 * Show information about a given Mock Turtle device
 * @param[in] trtl Mock Turtle device instance
 */
static void trtl_list_device_info(struct trtl_dev *trtl)
{
	const struct trtl_config_rom *cfg;
	int i, k;

	cfg = trtl_config_get(trtl);

	fprintf(stdout, "signature: 0x%08"PRIx32"\n", cfg->signature);
	fprintf(stdout, "version: 0x%08"PRIx32"\n", cfg->version);
	fprintf(stdout, "clock-freq: %"PRIu32"\n", cfg->clock_freq);
	fprintf(stdout, "flags: 0x%"PRIx32"\n", cfg->flags);
	fprintf(stdout, "application-id: 0x%08"PRIx32"\n", cfg->app_id);
	fprintf(stdout, "shm-size (32bit words): %"PRIu32"\n", cfg->smem_size);
	fprintf(stdout, "cpus:\n");

	for (i = 0; i < cfg->n_cpu; ++i) {
		fprintf(stdout, "\t- index: %d\n", i);
		fprintf(stdout, "\t  mem-size (32bit words): %"PRIu32"\n", cfg->mem_size[i]);
		fprintf(stdout, "\t  hmq:\n");
		for (k = 0; k < cfg->n_hmq[i]; ++k) {
			uint32_t sizes = cfg->hmq[i][k].sizes;

			fprintf(stdout, "\t\t- index: %d\n", k);
			fprintf(stdout, "\t\t  width-header: %"PRId32"\n",
				TRTL_CONFIG_ROM_MQ_SIZE_HEADER(sizes));
			fprintf(stdout, "\t\t  width-payload: %"PRId32"\n",
				TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(sizes));
			fprintf(stdout, "\t\t  depth: %"PRId32"\n",
				TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(sizes));
			fprintf(stdout, "\t\t  endpoint-id: %"PRIu32"\n",
				cfg->hmq[i][k].endpoint_id);
		}
		fprintf(stdout, "\t  rmq:\n");
		for (k = 0; k < cfg->n_rmq[i]; ++k) {
			uint32_t sizes = cfg->rmq[i][k].sizes;

			fprintf(stdout, "\t\t- index: %d\n", k);
			fprintf(stdout, "\t\t  width-header: %"PRId32"\n",
				TRTL_CONFIG_ROM_MQ_SIZE_HEADER(sizes));
			fprintf(stdout, "\t\t  width-payload: %"PRId32"\n",
				TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(sizes));
			fprintf(stdout, "\t\t  depth: %"PRId32"\n",
				TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(sizes));
			fprintf(stdout, "\t\t  endpoint-id: %"PRIu32"\n",
				cfg->hmq[i][k].endpoint_id);
		}
	}
}


int main(int argc, char *argv[])
{
	char **list;
	struct trtl_dev *trtl;
	char c;
	int i, verbose = 0, err;

	atexit(trtl_exit);

	while ((c = getopt (argc, argv, "h:v")) != -1) {
		switch (c) {
		default:
			help(argv[0]);
			break;
		case 'v':
			verbose++;
			break;
		}
	}

	err = trtl_init();
	if (err) {
		fprintf(stderr, "Cannot init Mock Turtle library: %s\n",
			trtl_strerror(errno));
		exit(1);
	}

	list = trtl_list();
	if (!list)
		goto out;
	for (i = 0; list[i]; ++i) {
		fprintf(stdout, "%s\n" , list[i]);
		trtl = trtl_open(list[i]);
		if (!trtl) {
			fprintf(stderr, "Cannot open device: %s\n",
				trtl_strerror(errno));
			continue;
		}

		switch (verbose) {
		case 1:
			trtl_list_device_info(trtl);
			break;
		}
		trtl_close(trtl);
	}
	trtl_list_free(list);
out:
	exit(0);
}
