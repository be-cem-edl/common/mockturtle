/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <inttypes.h>
#include <mockturtle/libmockturtle.h>

static void help(char *name)
{
	fprintf(stderr, "\n");
	fprintf(stderr,
		"%s -D 0x<hex-number> -i <number> -c <number> -q <number> [read <idx> <size> <idx> <size> ...] [write <idx> <size> <value> ... <value>]\n\n",
		name);
	fprintf(stderr, "Read/write buffers on a HMQ\n\n");
	fprintf(stderr, "-D   device identificator in hexadecimal format\n");
	fprintf(stderr, "-c   CPU core index\n");
	fprintf(stderr, "-q   HMQ index\n");
	fprintf(stderr, "-h   show this help\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "write mode accept only 1 buffer\n");
	fprintf(stderr, "\n");
	exit(1);
}

enum operations {
	OP_READ,
	OP_WRITE,
};


int main(int argc, char *argv[])
{
	struct trtl_dev *trtl;
	char c;
	int err, n, v, i, k;
	uint32_t dev_id = 0xFFFFFFFF;
	enum operations mode;
	struct trtl_tlv *tlv;
	unsigned int n_tlv, idx_cpu = 0, idx_hmq = 0;;
	uint32_t *data;

	atexit(trtl_exit);

	while ((c = getopt (argc, argv, "hD:c:q:")) != -1) {
		switch (c) {
		default:
			help(argv[0]);
			break;
		case 'D':
			n = sscanf(optarg, "0x%x", &dev_id);
			if (n != 1) {
				fprintf(stderr, "Invalid Device ID\n");
				exit(1);
			}
			break;
		case 'c':
			n = sscanf(optarg, "%u", &idx_cpu);
			if (n != 1) {
				fprintf(stderr, "Invalid CPU index\n");
				exit(1);
			}
			break;
		case 'q':
			n = sscanf(optarg, "%u", &idx_hmq);
			if (n != 1) {
				fprintf(stderr, "Invalid HMQ index\n");
				exit(1);
			}
			break;
		}
	}


	if (strcmp(argv[optind], "write") == 0) {
		mode = OP_WRITE;
	} else if (strcmp(argv[optind], "read") == 0) {
		mode = OP_READ;
	} else {
		fprintf(stderr, "Unknown operation \'%s\' (read, write)\n",
			argv[optind]);
		exit(1);
	}

	optind++;
	n_tlv = argc - optind;
	if ((n_tlv < 2) || (mode == OP_READ && n_tlv % 2)) {
		fprintf(stderr, "Invalid number of arguments\n");
		exit(1);
	}

	if (mode == OP_READ)
		n_tlv /= 2;
	else
		n_tlv = 1; /* Only 1 buffer on write */

	tlv = calloc(n_tlv, sizeof(struct trtl_tlv));
	if (!tlv) {
		fprintf(stderr, "Cannot allocate TLV descriptors\n");
		exit(1);
	}
	for (i = optind, v = 0; v < n_tlv; i += 2, ++v) {
		n = sscanf(argv[i], "%"SCNu32, &tlv[v].type);
		if (n != 1) {
			fprintf(stderr,
				"Invalid type: it must be a decimal number (%s)\n",
				argv[i]);
			exit(1);
		}
		n = sscanf(argv[i + 1], "%zu", &tlv[v].size);
		if (n != 1) {
			fprintf(stderr,
				"Invalid size: it must be a decimal number (%s)\n",
				argv[i + 1]);
			exit(1);
		}

		tlv[v].buf = malloc(tlv[v].size);
		if (!tlv[v].buf) {
			fprintf(stderr, "Cannot allocate buffer\n");
			exit(1);
		}

		if (mode == OP_READ)
			continue;

		data = tlv[v].buf;
		for (i = i + 2, k = 0 ; i < argc; ++i, ++k) {
			/*
			 * Continue to read argv arguments,
			 * so use the same index
			 */
			n = sscanf(argv[i], "0x%x", &data[k]);
			if (n != 1) {
				n = sscanf(argv[i], "%u", &data[k]);
				if (n != 1) {
					fprintf(stderr,
						"Invalid index: it must be a decimal or hexadecimal number (%s)\n",
						argv[i]);
					exit(1);
				}
			}
		}

	}


	err = trtl_init();
	if (err) {
		fprintf(stderr, "Cannot init Mock Turtle lib: %s\n",
			trtl_strerror(errno));
		exit(1);
	}

	trtl = trtl_open_by_id(dev_id);
	if (!trtl) {
		fprintf(stderr, "Cannot open TRTL: %s\n", trtl_strerror(errno));
		exit(errno);
	}

	switch (mode) {
	case OP_READ:
		err = trtl_fw_buffer_get(trtl, idx_cpu, idx_hmq, tlv, n_tlv);
		if (err) {
			fprintf(stderr, "Cannot read buffer: %s\n",
				trtl_strerror(errno));
			break;
		}
		for (i = 0; i < n_tlv; ++i) {
			fprintf(stdout, "Buffer index: %"PRIu32"\n", tlv[i].type);
			fprintf(stdout, "Buffer size : %zu\n", tlv[i].size);
			fprintf(stdout, "Buffer data :");
			data = tlv[i].buf;
			for (k = 0; k < tlv[i].size / 4; ++k) {
				fprintf(stdout, " %08x", data[k]);
				if ((k & 0x3) == 0x3)
					fprintf(stdout, "\n             ");
			}
			fprintf(stdout, "\n");
		}
		/* trtl_print_payload(&hdr, tlv); */
		break;
	case OP_WRITE:
		err = trtl_fw_buffer_set(trtl, idx_cpu, idx_hmq, tlv, n_tlv);
		break;
	}

	trtl_close(trtl);

	for (i = 0; i < n_tlv; ++i) {
		free(tlv[i].buf);
	}
	free(tlv);

	if (err) {
		exit(errno);
	} else {
		exit(0);
	}
}
