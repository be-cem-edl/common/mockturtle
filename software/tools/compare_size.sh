#!/bin/bash

# SPDX-License-Identifier: LGPL-2.1-or-later
#
# SPDX-FileCopyrightText: 2019 CERN
#
# Author: Adam Wujek
#
# This script compares the size of produced binaries of a current commit
# (read from a file $size_db_file), with sizes of binaries produced for previous
# commits (read from file $size_db_file).

SIZE="$CROSS_COMPILE_TARGET""size"

if ! [ -n "$size_info_file" ]; then
    size_info_file=size_info.txt
    echo "No file with size info specified! Using default ($size_info_file)"
fi

if ! [ -n "$size_db_file" ]; then
    size_db_file=size_db.txt
    echo "No file with size DB specified! Using default ($size_db_file)"
    if ! [ -f "$size_db_file" ]; then
        touch $size_db_file
    fi
fi

if ! [ -f "$size_db_file" ]; then
    echo "No DB file with build sizes ($size_db_file)! Exit."
    exit 0
fi



# save the size of all found .elf files into "$size_info_file" as:
# GIT_HASH $file $SIZES
GIT_HASH=`git log --format=format:%H -1`

rm -f $size_info_file
find $1 | grep \.elf | while read file
do
    echo "$SIZE" "$file" $CONFIG_NAME

    SIZES=`"$SIZE" "$file" | grep "$file"`

    echo -n "$GIT_HASH " >> "$size_info_file"
#     echo -n "$file ">> "$size_info_file"
    echo "$SIZES" >> "$size_info_file"
done

if ! [ -f "$size_info_file" ]; then
    echo "No file with build sizes ($size_info_file)! Exit."
    exit 0
fi


#####################  draw a table  #####################


min_column_width=19

# print the same string multiple times
repl() { printf -- "$1"'%.s' $(seq 1 $2); }

declare -A curr_size_array;
declare -A size_db_array;
declare -A column_width;
declare -a commits_since_master;
GIT_HASH_CUR=`git rev-parse HEAD`
GIT_HASH_MASTER=`git rev-parse origin/master`

if ! [ -n "$GIT_HASH_CUR" ]; then
    echo "Unable to get hash of a current commit"
    exit 1
fi

if [ "$GIT_HASH_CUR" = "$GIT_HASH_MASTER" ]; then
    echo "In master"
    exit 0
fi

# Read size info file
while read git_hash text data bss dec hex filename
do
    if [ "$git_hash" = "$GIT_HASH_CUR" ]; then
	curr_size_array[$filename]=$dec
    fi
done < "$size_info_file"

# Read size db file
while read git_hash text data bss dec hex filename
do
    #echo "$git_hash $filename $dec"
    size_db_array["$git_hash"_"$filename"]="$dec"
done < "$size_db_file"

# find width of each column
for i in "${!curr_size_array[@]}"
do
    basename_i=$(basename $i)
    # find minimum width
    if [ $min_column_width -gt ${#basename_i} ]; then
	width=$min_column_width
    else
	width=${#basename_i}
    fi
    column_width["$i"]=$width
done


# print header
for i in "${!curr_size_array[@]}"
do
    echo -n "+--"

    width=${column_width["$i"]}

    repl - $width
done
echo "+------------------------------------------------"

for i in "${!curr_size_array[@]}"
do
    width=${column_width["$i"]}
    i=$(basename $i)
    printf "| %*s " $width $i
done
echo "|"

for i in "${!curr_size_array[@]}"
do
    echo -n "+--"
    width=${column_width["$i"]}
    repl - $width
done
echo "+------------------------------------------------"

# print data
# print current size
for i in "${!curr_size_array[@]}"
do
    # find minimum width
    size=${curr_size_array[$i]}
    width=${column_width["$i"]}
    printf "| %*s " $width "$size"
done
echo -n "| CURRENT "
# print hash and the title for current commit
git_current_commit=`git log --format=tformat:"%h %s" -1`
echo $git_current_commit

# print info about previous commits
# pick ! as the separator
# tformat to get the newline after the last entry
git log --format=tformat:"!%H!%s" origin/master~1...HEAD~1 --graph | while IFS="!" read -r git_graph git_hash git_title
do
    for i in "${!curr_size_array[@]}"
    do
	size=${size_db_array["$git_hash"_"$i"]}
	if [ -z "$size" ]; then
	    print_buff=""
	else
	    print_buff="($(($size-${curr_size_array[$i]}))) $size"
	fi

    width=${column_width["$i"]}
    printf "| %*s " $width "$print_buff"
    done
    # print graph, hash and title
    printf "|   %-*s %.8s %s\n" 5 "$git_graph" "$git_hash" "$git_title"
done

for i in "${!curr_size_array[@]}"
do
    echo -n "+--"
    width=${column_width["$i"]}
    repl - $width
done
echo "+------------------------------------------------"


#####################  draw a table end  #####################

# remove previous entries with the same GIT_HASH from the file $size_db_file
if [ -f "$size_db_file" ]; then
    cat "$size_db_file" | grep -v $GIT_HASH > "$size_db_file".tmp
    mv "$size_db_file".tmp "$size_db_file"
fi

# update $size_db_file with the entires from the current run
cat "$size_info_file" >> "$size_db_file"
