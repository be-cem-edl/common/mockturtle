/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <mockturtle/libmockturtle.h>

static void help(char *name)
{
	fprintf(stderr, "\n");
	fprintf(stderr,
		"%s -D 0x<hex-number> -c <number> -q <number> [write <idx> <value> <idx> <value> ...] [read <idx> <idx> ...]\n\n",
		name);
	fprintf(stderr, "Read/write variables on a HMQ\n\n");
	fprintf(stderr, "-D   device identificator in hexadecimal format\n");
	fprintf(stderr, "-c   CPU core index\n");
	fprintf(stderr, "-q   HMQ index\n");
	fprintf(stderr, "-h   show this help\n");
	fprintf(stderr, "\n");
	exit(1);
}

enum operations {
	OP_READ,
	OP_WRITE,
};


int main(int argc, char *argv[])
{
	struct trtl_dev *trtl;
	char c;
	unsigned int idx_cpu = 0, idx_hmq = 0;
	int err, n, i, v, n_var;
	uint32_t dev_id = -1;
	uint32_t *var;
	enum operations mode;

	atexit(trtl_exit);

	while ((c = getopt (argc, argv, "hD:c:q:")) != -1) {
		switch (c) {
		default:
			help(argv[0]);
			break;
		case 'D':
			n = sscanf(optarg, "0x%x", &dev_id);
			if (n != 1) {
				fprintf(stderr, "Invalid Device ID\n");
				exit(1);
			}
			break;
		case 'c':
			n = sscanf(optarg, "%u", &idx_cpu);
			if (n != 1) {
				fprintf(stderr, "Invalid CPU index\n");
				exit(1);
			}
			break;
		case 'q':
			n = sscanf(optarg, "%u", &idx_hmq);
			if (n != 1) {
				fprintf(stderr, "Invalid HMQ index\n");
				exit(1);
			}
			break;
		}
	}
	if (strcmp(argv[optind], "write") == 0) {
		mode = OP_WRITE;
	} else if (strcmp(argv[optind], "read") == 0) {
		mode = OP_READ;
	} else {
		fprintf(stderr, "Unknown operation \'%s\' (read, write)\n",
			argv[optind]);
		exit(1);
	}

	optind++;
	n_var = argc - optind;
	if (n_var <= 0 || (strcmp(argv[optind], "write") == 0 && n_var % 2)) {
		fprintf(stderr, "Invalid number of arguments\n");
		exit(1);
	}
	if (mode == OP_WRITE)
		n_var /= 2;

	/* Build variable message payload */
	var = calloc(n_var * 2, sizeof(uint32_t));
	if (!var)
		exit(1);

	for (i = optind, v = 0; i < argc;
	     i += (mode == OP_READ ? 1 : 2), v += 2) {
		n = sscanf(argv[i], "%u", &var[v]);
		if (n != 1) {
			fprintf(stderr,
				"Invalid index: it must be a decimal number (%s)\n",
				argv[i]);
			exit(1);
		}
		if (mode == OP_READ)
			continue;

		/* Write mode only */
		n = sscanf(argv[i + 1], "0x%x", &var[v + 1]);
		if (n == 1)
			continue;
		n = sscanf(argv[i + 1], "%u", &var[v + 1]);
		if (n != 1) {
			fprintf(stderr,
				"Invalid value: it must be a decimal or hexadecimal number (%s)\n",
				argv[i + 1]);
			exit(1);
		}
	}


	err = trtl_init();
	if (err) {
		fprintf(stderr, "Cannot init Mock Turtle lib: %s\n",
			trtl_strerror(errno));
		exit(1);
	}

	trtl = trtl_open_by_id(dev_id);
	if (!trtl) {
		fprintf(stderr, "Cannot open TRTL: %s\n", trtl_strerror(errno));
		exit(errno);
	}

	switch (mode) {
	case OP_READ:
		err = trtl_fw_variable_get(trtl, idx_cpu, idx_hmq, var, n_var);
		if (err) {
			fprintf(stderr, "Cannot read variables: %s\n",
				trtl_strerror(errno));
			break;
		}
		for (i = 0; i < n_var; ++i) {
			fprintf(stdout, "[%u] 0x%x\n",
				var[i * 2], var[i * 2 + 1]);
		}
		break;
	case OP_WRITE:
		err = trtl_fw_variable_set(trtl, idx_cpu, idx_hmq, var, n_var);
		if (err) {
			fprintf(stderr, "Cannot write variables: %s\n",
				trtl_strerror(errno));
		}
		break;
	}

	trtl_close(trtl);
	if (err) {
		exit(errno);
	} else {
		exit(0);
	}
}
