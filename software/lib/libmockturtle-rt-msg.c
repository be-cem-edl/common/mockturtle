/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <errno.h>
#include <inttypes.h>

#include "libmockturtle-internal.h"

static inline int min(const int a, const int b)
{
	return a < b ? a : b;
}

/**
 * Retrieve the current Real-Time Application name. This is a
 * synchronous message.
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[out] name application name
 * @param[in] len maximum string length
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int trtl_fw_name(struct trtl_dev *trtl,
				 unsigned int idx_cpu,
				 unsigned int idx_hmq,
				 char *name, size_t len)
{
	struct trtl_msg msg;
	int err;

	memset(&msg, 0, sizeof(struct trtl_msg));
	msg.hdr.msg_id = TRTL_MSG_ID_NAME;
	msg.hdr.flags = TRTL_HMQ_HEADER_FLAG_RPC;

	err = trtl_msg_sync(trtl, idx_cpu, idx_hmq, &msg, &msg,
			    trtl_default_timeout_ms);
	if (err < 0)
		return -1;
	if (msg.hdr.msg_id != TRTL_MSG_ID_NAME) {
		errno = ETRTL_INVALID_MESSAGE;
		return -1;
	}

	memcpy(name, msg.data, min(msg.hdr.len * 4, len));

	return 0;
}

/**
 * Retrieve the current Real-Time Application version running. This is a
 * synchronous message.
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[out] version firmware version
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int trtl_fw_version(struct trtl_dev *trtl,
		    unsigned int idx_cpu,
		    unsigned int idx_hmq,
		    struct trtl_fw_version *version)
{
	struct trtl_msg msg;
	int err;

	memset(&msg, 0, sizeof(struct trtl_msg));
	msg.hdr.msg_id = TRTL_MSG_ID_VER;
	msg.hdr.flags = TRTL_HMQ_HEADER_FLAG_RPC;

	err = trtl_msg_sync(trtl, idx_cpu, idx_hmq, &msg, &msg,
			    trtl_default_timeout_ms);
	if (err < 0)
		return -1;
	if (msg.hdr.msg_id != TRTL_MSG_ID_VER) {
		errno = ETRTL_INVALID_MESSAGE;
		return -1;
	}

	memcpy(version, msg.data, msg.hdr.len * 4);

	return 0;
}


/**
 * Check if firmware core is running and answering to messages
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in] timeout_ms timeout
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int trtl_fw_ping_timeout(struct trtl_dev *trtl,
                         unsigned int idx_cpu,
                         unsigned int idx_hmq,
                         int timeout_ms)
{
	struct trtl_msg msg;
	int err;

	memset(&msg, 0, sizeof(struct trtl_msg));
	msg.hdr.msg_id = TRTL_MSG_ID_PING;
	msg.hdr.flags = TRTL_HMQ_HEADER_FLAG_RPC;

	err = trtl_msg_sync(trtl, idx_cpu, idx_hmq, &msg, &msg,
			    timeout_ms);
	if (err < 0)
		return -1;
	if (msg.hdr.msg_id != TRTL_MSG_ID_PING) {
		errno = ETRTL_INVALID_MESSAGE;
		return -1;
	}

	return 0;
}

/**
 * Check if firmware core is running and answering to messages
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int trtl_fw_ping(struct trtl_dev *trtl,
		 unsigned int idx_cpu,
		 unsigned int idx_hmq)
{
        return trtl_fw_ping_timeout(trtl, idx_cpu, idx_hmq,
                                    trtl_default_timeout_ms);
}

static int __trtl_fw_variable(struct trtl_dev *trtl,
			      unsigned int idx_cpu,
			      unsigned int idx_hmq,
			      uint32_t *variables,
			      unsigned int n_variables,
			      uint8_t msg_id)
{
	struct trtl_msg msg;
	int err;

	memset(&msg, 0, sizeof(struct trtl_msg));
	msg.hdr.msg_id = msg_id;
	msg.hdr.flags = TRTL_HMQ_HEADER_FLAG_RPC;
	msg.hdr.len = 2 * n_variables;

	memcpy(msg.data, variables, sizeof(uint32_t) * msg.hdr.len);

	err = trtl_msg_sync(trtl, idx_cpu, idx_hmq, &msg, &msg,
			    trtl_default_timeout_ms);
	if (err < 0)
		return -1;
	if (msg.hdr.msg_id != TRTL_MSG_ID_VAR_GET) {
		/* Yes, it answers with GET even on SET */
		errno = ETRTL_INVALID_MESSAGE;
		return -1;
	}

	memcpy(variables, msg.data, sizeof(uint32_t) * msg.hdr.len);

	return 0;
}

/**
 * Send/receive a set of variables to/from the Real-Time application.
 *
 * The 'variables' field data format is the following
 *
 *     0     1     2     3     4     5    ...
 *     +-----+-----+-----+-----+-----+-----+
 *     | IDX | VAL | IDX | VAL | IDX | VAL | ...
 *     +-----+-----+-----+-----+-----+-----+
 *
 * IDX is the variable index defined by the real-time application
 * VAL is the associated value
 *
 * By setting the flag 'sync' you will send a synchronous message, otherwise
 * I asynchronous. When synchronous the 'variables' field will be
 * overwritten by the synchronous answer; the answer contains the read back
 * values for the requested variable after the set operation. You can use
 * this to verify. You can use synchronous messages to verify that you
 * variable are properly set.
 * This function will change the header content, in particular it will change
 * the following fields: msg_id, len
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in,out] variables on input variable indexes and values.
 *                On output variable indexes and values.
 * @param[in] n_variables number of variables to set. In other words,
 *            the number of indexes you have in the 'variables' fields
 * @return 0 on success, -1 on error and errno is appropriately set.
 */
int trtl_fw_variable_set(struct trtl_dev *trtl,
			 unsigned int idx_cpu,
			 unsigned int idx_hmq,
			 uint32_t *variables,
			 unsigned int n_variables)
{
	return __trtl_fw_variable(trtl, idx_cpu, idx_hmq,
				  variables, n_variables,
				  TRTL_MSG_ID_VAR_SET);
}


/**
 * It receive a set of variables from the Real-Time application.
 *
 * The 'variables' field data format is the following
 *
 *     0     1     2     3     4     5    ...
 *     +-----+-----+-----+-----+-----+-----+
 *     | IDX | VAL | IDX | VAL | IDX | VAL | ...
 *     +-----+-----+-----+-----+-----+-----+
 *
 * IDX is the variable index defined by the real-time application
 * VAL is the associated value
 *
 * This kind of message is always synchronous. The 'variables' field will be
 * overwritten by the synchronous answer; the answer contains the read back
 * values for the requested variables.
 * This function will change the header content, in particular it will change
 * the following fields: msg_id, flags, len
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in,out] variables on input variable indexes. On output variable
 *                indexes and values.
 * @param[in] n_variables number of variables to set. In other words,
 *            the number of indexes you have in the 'variables' fields
 * @return 0 on success, -1 on error and errno is appropriately set.
 */
int trtl_fw_variable_get(struct trtl_dev *trtl,
			 unsigned int idx_cpu,
			 unsigned int idx_hmq,
			 uint32_t *variables,
			 unsigned int n_variables)
{
	return __trtl_fw_variable(trtl, idx_cpu, idx_hmq,
				  variables, n_variables,
				  TRTL_MSG_ID_VAR_GET);

}



static int __trtl_fw_buffer(struct trtl_dev *trtl,
			    unsigned int idx_cpu,
			    unsigned int idx_hmq,
			    struct trtl_tlv *tlv,
			    unsigned int n_tlv,
			    uint8_t msg_id)
{
	struct trtl_msg msg;
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;
	unsigned int offset;
	unsigned int total_size = 0;
	int err, i;

	/* Validate */
	for (i = 0; i < n_tlv; ++i) {
		uint32_t sizes;

		total_size += (sizeof(uint32_t) * 2); /* 32bit for type and size */
		total_size += tlv->size;
		sizes = wdesc->cfgrom.hmq[idx_cpu][idx_hmq].sizes;
		if (total_size > TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(sizes) * 4 ||
		    tlv->size > TRTL_MAX_PAYLOAD_SIZE * 4) {
			errno = EINVAL;
			return -1;
		}
	}

	memset(&msg, 0, sizeof(struct trtl_msg));
	msg.hdr.msg_id = msg_id;
	msg.hdr.flags = TRTL_HMQ_HEADER_FLAG_RPC;
	msg.hdr.len = total_size / sizeof(uint32_t);

	/* Copy buffers */
	for (offset = 0, i = 0; i < n_tlv; ++i) {
		msg.data[offset++] = tlv[i].type;
		msg.data[offset++] = tlv[i].size;
		memcpy(&msg.data[offset], tlv[i].buf, tlv[i].size);
		offset += (tlv->size / sizeof(uint32_t));
	}

	err = trtl_msg_sync(trtl, idx_cpu, idx_hmq, &msg, &msg,
			    trtl_default_timeout_ms);
	if (err < 0)
		return -1;
	if (msg.hdr.msg_id != TRTL_MSG_ID_BUF_GET) {
		/* Yes, it answers with GET even on SET */
		errno = ETRTL_INVALID_MESSAGE;
		return -1;
	}

	/* Copy back buffers */
	for (offset = 0, i = 0; i < n_tlv; ++i) {
		tlv[i].type = msg.data[offset++];
		tlv[i].size = msg.data[offset++];
		memcpy(tlv[i].buf, &msg.data[offset],  tlv[i].size);
		offset += (tlv->size / sizeof(uint32_t));
	}

	return 0;
}

/**
 * Send/receives a set of structures within TLV records.
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in] tlv the complete buffer
 * @param[in] n_tlv number of tlv structures
 */
int trtl_fw_buffer_set(struct trtl_dev *trtl,
		       unsigned int idx_cpu,
		       unsigned int idx_hmq,
		       struct trtl_tlv *tlv,
		       unsigned int n_tlv)
{
	return __trtl_fw_buffer(trtl, idx_cpu, idx_hmq, tlv, n_tlv,
				TRTL_MSG_ID_BUF_SET);
}


/**
 * Receive a set of structures within TLV records.
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in,out] tlv on input tlv with only the type,
 *                on output the complete buffer
 * @param[in] n_tlv number of tlv structures
 */
int trtl_fw_buffer_get(struct trtl_dev *trtl,
		       unsigned int idx_cpu,
		       unsigned int idx_hmq,
		       struct trtl_tlv *tlv,
		       unsigned int n_tlv)
{
	return __trtl_fw_buffer(trtl, idx_cpu, idx_hmq, tlv, n_tlv,
				TRTL_MSG_ID_BUF_GET);
}


/**
 * Print the message header in a human readable format
 * @param[in] msg message
 */
void trtl_print_header(struct trtl_msg *msg)
{
	struct trtl_hmq_header *h = &msg->hdr;

	fprintf(stdout,
		"\tapp_id 0x%04"PRIx16" | flags   0x%02"PRIx8" | msg_id 0x%02"PRIx8"\r\n",
		 h->rt_app_id, h->flags, h->msg_id);
	fprintf(stdout,
		"\tlen 0x%04"PRIx16" | sync_id 0x%04"PRIx16"\r\n",
		 h->len, h->sync_id);
	fprintf(stdout,
		"\tseq 0x%08"PRIx32"\r\n",
		 h->seq);
}


/**
 * Print the payload in a human readable format according
 * to the message type
 * @param[in] msg message
 */
void trtl_print_payload(struct trtl_msg *msg)
{
	int i;

	switch (msg->hdr.msg_id) {
	case TRTL_MSG_ID_ERROR:
		fprintf(stdout, "Error message\n");
		fprintf(stdout, "Error number: %"PRIu32"\n", msg->data[0]);
		break;
	case TRTL_MSG_ID_PING:
		fprintf(stdout, "Ping message\n");
		break;
	case TRTL_MSG_ID_VER: {
		struct trtl_fw_version *v = (struct trtl_fw_version *)msg->data;
		fprintf(stdout, "Version message\n");
		fprintf(stdout, "Firmware ID: 0x%08"PRIx32"\n", v->rt_id);
		fprintf(stdout, "Firmware version: %"PRIu32".%"PRIu32"\n",
			RT_VERSION_MAJ(v->rt_version),
			RT_VERSION_MIN(v->rt_version));
		fprintf(stdout, "git SHA: 0x%08"PRIx32"\n", v->git_version);
		break;
	}
	case TRTL_MSG_ID_DBG:
		fprintf(stdout, "Debug message\n");
		for (i = 0; i < msg->hdr.len; ++i) {
			fprintf(stdout, "Value %d: 0x%08"PRIx32"\n",
				i, msg->data[i]);
		}
		break;
	case TRTL_MSG_ID_VAR_GET:
	case TRTL_MSG_ID_VAR_SET:
		for (i = 0; i < msg->hdr.len; i += 2) {
			fprintf(stdout, "Variable %"PRIu32": 0x%08"PRIx32"\n",
				msg->data[i], msg->data[i + 1]);
		}
		break;
	case TRTL_MSG_ID_BUF_SET:
	case TRTL_MSG_ID_BUF_GET:
		fprintf(stdout, "Buffer message\n");
		/* TODO */
		break;
	default:
		fprintf(stdout, "Unknown message\n");
		for (i = 0; i < msg->hdr.len; ++i) {
			if (i % 4 == 0)
				fprintf(stdout, "\n    %04d :", i);
			fprintf(stdout, " 0x%08"PRIx32, msg->data[i]);
		}

		break;
	}
}


/**
 * Print a message in a human readable format. This function assumes
 * that the message contains a Mock Turtle message header.
 * According to the message ID the format may change
 * @param[in] msg message
 */
void trtl_print_message(struct trtl_msg *msg)
{
	trtl_print_header(msg);
	trtl_print_payload(msg);
}
