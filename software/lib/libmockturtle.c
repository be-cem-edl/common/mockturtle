/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <libgen.h>
#include <errno.h>
#include <fcntl.h>
#include <glob.h>

#include "libmockturtle-internal.h"

/**
 * Buffer size for sysfs read
 */
#define TRTL_SYSFS_READ_LEN 32

static const char libmockturtle_version[] __attribute__((unused));
static const char libmockturtle_version[] = "libmockturtle version: "VERSION;

/**
 * The default timeout when communicating with the
 * driver: HMQ
 */
const unsigned int trtl_default_timeout_ms = 1000;


/**
 * Mock Turtle error code strings
 */
static const char * const trtl_error_str[] = {
	"Cannot parse data from sysfs attribute",
	"Invalid slot",
	"Operation not yet implemented",
	"The HMQ slot is close",
	"Invalid message",
	"Error while reading HMQ messages",
	"Failed to send synchronous message",
	"Failed to receive synchronous message",
	"Failed to receive synchronous message: poll error",
	"Failed to receive synchronous message: timeout",
	"Failed to receive synchronous message: invalid sync id",
	"Cannot extract device ID from device name",
	NULL,
};

static int trtl_sysfs_read(char *path, void *buf, size_t len);
static int trtl_sysfs_write(char *path, void *buf, size_t len);


/**
 * Return a string messages corresponding to a given error code. If
 * it is not a libtrtl error code, it will run strerror(3)
 * @param[in] err error code. Typically 'errno' variable
 * @return a message error. No need to free the string.
 */
const char *trtl_strerror(int err)
{
	if (err < ETRTL_INVAL_PARSE || err > __ETRTL_MAX)
		return strerror(err);
	return trtl_error_str[err - ETRTL_INVAL_PARSE];
}


/**
 * Initialize the TRTL library. It must be called before doing
 * anything else. If you are going to load/unload TRTL devices, then
 * you have to un-load (trtl_exit()) e reload (trtl_init()) the library.
 * @return 0 on success, otherwise -1 and errno is appropriately set
 */
int trtl_init(void)
{
	/* As you can see there is nothing to do because everything is
	   dynamical and does not require a special initialization.
	   Instead of removing this function and force the user to use it
	   I choosed to keep it even if it's empty. The reason is simple:
	   I do not know in the future if we need this library initialization,
	   if we will need it and we don't have it than we have to force ALL
	   our users to change all the software stack behind this library
	   which is not trivial and not backward compatible. I prefer to avoid
	   it and force people today to use this function and save a lot of
	   time in the future */
	return 0;
}


/**
 * Release the resources allocated by trtl_init(). It must be called when
 * you stop to use this library. Then, you cannot use functions from this
 * library anymore.
 */
void trtl_exit(void)
{
	/* READ trtl_init() */
}


/**
 * Return the number of available TRTLs. This is not calculated on demand.
 * Depend on library initialization.
 * @return the number of TRTL available
 */
uint32_t trtl_count()
{
	glob_t g;
	uint32_t count;
	int err;

	err = glob("/dev/trtl-[A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9]",
		   GLOB_NOSORT, NULL, &g);
	if (err != GLOB_NOMATCH)
		return 0;
	err = glob("/dev/mockturtle/trtl-[A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9]",
		   GLOB_NOSORT | GLOB_APPEND, NULL, &g);
	if (err)
		return 0;
	count = g.gl_pathc;
	globfree(&g);

	return count;
}


/**
 * Allocate and return the list of available TRTL devices. The user is
 * in charge to free the allocated memory wit trtl_list_free(). The list
 * contains trtl_count() + 1 elements. The last element is a NULL pointer.
 * @return a list of TRTL device's names. NULL on error
 */
char **trtl_list()
{
	char **list = NULL;
	glob_t g;
	int err, i, count;

	err = glob("/dev/trtl-[A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9]",
		   GLOB_NOSORT, NULL, &g);
	if (err && err != GLOB_NOMATCH)
		return NULL;
	err = glob("/dev/mockturtle/trtl-[A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9]",
		   GLOB_NOSORT | GLOB_APPEND, NULL, &g);
	if (err && err != GLOB_NOMATCH)
		return NULL;
	count = g.gl_pathc;

	list = malloc(sizeof(char *) * (count + 1));
	if (!list)
		return NULL;
	for (i = 0; i < count; ++i)
		list[i] = strdup(basename(g.gl_pathv[i]));
	list[i] = NULL;

	globfree(&g);

	return list;
}


/**
 * Release the list allocated memory
 * @param[in] list device list to release
 */
void trtl_list_free(char **list)
{
	int i;

	for (i = 0; list[i]; i++)
		free(list[i]);
	free(list);
}

/**
 * Open a TRTL device using a string descriptor. The descriptor correspond
 * to the main char device name of the Mock-Turtle.
 * @param[in] device name of the device to open
 * @return the TRTL token, NULL on error and errno is appropriately set
 */
struct trtl_dev *trtl_open(const char *device)
{
	struct trtl_desc *trtl;
	char path[256];
	int i, err, k, fd, ret;
	struct stat sb;

	trtl = malloc(sizeof(struct trtl_desc));
	if (!trtl)
		return NULL;
	/* device names are trtl-%04x and TRTL_NAME_LEN is 16, this is safe. */
	strncpy(trtl->name, device, TRTL_NAME_LEN - 1);

	ret = sscanf(trtl->name, "trtl-%"SCNx32, &trtl->devid);
	if (ret != 1) {
		errno = ETRTL_DEVICE_NAME_NOID;
		goto out_id;
	}

	snprintf(path, sizeof(path), "/dev/%s", trtl->name);
	err = stat(path, &sb);
	if (!err) {
		strncpy(trtl->path, "/dev", TRTL_PATH_LEN);
	} else {
		snprintf(path, sizeof(path),
			 "/dev/mockturtle/%s", trtl->name);
		err = stat(path, &sb);
		if (err)
			goto out_stat;
		strncpy(trtl->path, "/dev/mockturtle", TRTL_PATH_LEN);
	}

	snprintf(path, sizeof(path),
		 "/sys/class/mockturtle/%s/config-rom",
		 trtl->name);

	err = trtl_sysfs_read(path, &trtl->cfgrom,
			      sizeof(struct trtl_config_rom));
	if (err < 0)
		goto out_cfg;
	if (err != sizeof(struct trtl_config_rom)) {
		errno = EINVAL;
		goto out_cfg;
	}

	trtl->fd_dev = -1;
	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		trtl->fd_cpu[i] = -1;
		for (k = 0; k < trtl->cfgrom.n_hmq[i]; ++k) {
			snprintf(path, sizeof(path), "%s/%s-%02d-%02d",
				 trtl->path, trtl->name, i, k);
			fd = open(path, O_RDWR);
			if (fd < 0)
				goto out_hmq_fd;
			trtl->fd_hmq[i][k] = fd;

			fd = open(path, O_RDWR);
			if (fd < 0) {
				close(trtl->fd_hmq[i][k]);
				goto out_hmq_fd;
			}
			trtl->fd_hmq_sync[i][k] = fd;
			err = ioctl(trtl->fd_hmq_sync[i][k],
				    TRTL_IOCTL_HMQ_SYNC_SET, 1);
			if (err) {
				close(trtl->fd_hmq[i][k]);
				close(trtl->fd_hmq_sync[i][k]);
				goto out_hmq_fd;
			}
		}
	}

	return (struct trtl_dev *)trtl;

out_hmq_fd:
	while(i >= 0) {
		while(--k >= 0) {
			close(trtl->fd_hmq[i][k]);
			close(trtl->fd_hmq_sync[i][k]);
		}
		--i;
	}
out_cfg:
out_stat:
out_id:
	free(trtl);
	return NULL;
}


/**
 * Open a TRTL device using its device_id. The Mock-Turtle
 * driver is based upon the platform bus infrastructure, so all trtl devices are
 * identified with their platform id.
 * @param[in] device_id device id of the device to use
 * @return the TRTL token, NULL on error and errno is appropriately set
 */
struct trtl_dev *trtl_open_by_id(uint32_t device_id)
{
	char name[12];

	snprintf(name, sizeof(name), "trtl-%04x", device_id);
	return trtl_open(name);
}


/**
 * Open a TRTL device using its Logical Unit Number. The Logical Unit Number
 * is an instance number of a particular hardware. The LUN to use is the carrier
 * one, and not the mezzanine one (if any).
 * The driver is not aware of LUNs but only of device-id. So, if this function does
 * not work it means that your installation lacks of symbolic links that
 * convert LUNs to device-ids.
 * @param[in] lun Logical Unit Number of the device to use
 * @return the TRTL token, NULL on error and errno is appropriately set
 */
struct trtl_dev *trtl_open_by_lun(unsigned int lun)
{
	char path[16], dev_id_str[4];
	uint32_t dev_id;
	int ret;

	ret = snprintf(path, sizeof(path), "/dev/trtl.%u", lun);
	if (ret < 0 || ret >= sizeof(path)) {
		errno = EINVAL;
		return NULL;
	}
	ret = readlink(path, dev_id_str, sizeof(dev_id_str));
	if (ret < 0)
		return NULL;
	if (sscanf(dev_id_str, "%4x", &dev_id) != 1) {
		errno = ENODEV;
		return NULL;
	}

	return trtl_open_by_id(dev_id);
}


/**
 * Close a TRTL device opened with one of the following functions:
 * trtl_open(), wrcn_open_by_lun(), trtl_open_by_id()
 * @param[in] trtl device token
 */
void trtl_close(struct trtl_dev *trtl)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;
	int i, k;

	if (!trtl)
		return;

	/* Close all the file descriptors */
	if (wdesc->fd_dev >= 0)
		close(wdesc->fd_dev);
	for (i = 0; i < wdesc->cfgrom.n_cpu; ++i) {
		if (wdesc->fd_cpu[i] >= 0)
			close(wdesc->fd_cpu[i]);
		for (k = 0; k < wdesc->cfgrom.n_hmq[i]; ++k) {
			if (wdesc->fd_hmq[i][k] >= 0)
				close(wdesc->fd_hmq[i][k]);
			if (wdesc->fd_hmq_sync[i][k] >= 0)
				close(wdesc->fd_hmq_sync[i][k]);
		}
	}

	free(wdesc);
}


/**
 * Generic function that reads from a sysfs attribute
 * @param[in] path where to read
 * @param[out] buf destination buffer
 * @param[in] len number of bytes to read
 *
 * @return the number of written bytes on success, -1 otherwise and
 *         errno is set appropriately
 */
static int trtl_sysfs_read(char *path, void *buf, size_t len)
{
	int fd, i;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return -1;
	i = read(fd, buf, len);
	close(fd);

	return i;
}


/**
 * Generic function that writes to a sysfs attribute
 * @param[in] path where to write
 * @param[in] buf buffer to write
 * @param[in] len number of bytes to write
 *
 * @return the number of written bytes on success, -1 otherwise and
 *         errno is set appropriately
 */
static int trtl_sysfs_write(char *path, void *buf, size_t len)
{
	int fd, i;

	fd = open(path, O_WRONLY);
	if (fd < 0)
		return -1;
	i = write(fd, buf, len);
	close(fd);

	return i;
}

/**
 * Generic function that parse a string from a sysfs attribute
 * @param[in] path where to read
 * @param[in] fmt like scanf(3)
 *
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
static int trtl_sysfs_scanf(char *path, const char *fmt, ...)
{
	char buf[TRTL_SYSFS_READ_LEN];
	va_list args;
	int ret;

	ret = trtl_sysfs_read(path, buf, TRTL_SYSFS_READ_LEN);
	if (ret < 0)
		return ret;

	va_start(args, fmt);
	ret = vsscanf(buf, fmt, args);
	va_end(args);
	if (ret < 0)
		return ret;
	if (ret != 1) {
		errno = ETRTL_INVAL_PARSE;
		return -1;
	}

	return 0;
}


/**
 * Generic function that build a string to be written in a sysfs attribute
 * @param[in] path where to write
 * @param[in] fmt like printf(3)
 *
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
static int trtl_sysfs_printf(char *path, const char *fmt, ...)
{
	char buf[TRTL_SYSFS_READ_LEN];
	va_list args;
	int ret;

	va_start(args, fmt);
	vsnprintf(buf, TRTL_SYSFS_READ_LEN, fmt, args);
	va_end(args);

	ret = trtl_sysfs_write(path, buf, TRTL_SYSFS_READ_LEN);
	if (ret == TRTL_SYSFS_READ_LEN)
		return 0;
	return -1;
}


/**
 * Return the current status of the TRTL CPUs' reset line
 * @param[in] trtl device token
 * @param[out] mask bit mask of the reset-lines
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_cpu_reset_get(struct trtl_dev *trtl, uint32_t *mask)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;
	char path[TRTL_SYSFS_PATH_LEN];

	snprintf(path, TRTL_SYSFS_PATH_LEN,
		 "/sys/class/mockturtle/%s/reset_mask",
		 wdesc->name);

	return trtl_sysfs_scanf(path, "%x", mask);
}


/**
 * Assert or de-assert the reset line of the TRTL CPUs
 * @param[in] trtl device to use
 * @param[in] mask bit mask of the reset-lines
 *
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_cpu_reset_set(struct trtl_dev *trtl, uint32_t mask)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;
	char path[TRTL_SYSFS_PATH_LEN];

	snprintf(path, TRTL_SYSFS_PATH_LEN,
		 "/sys/class/mockturtle/%s/reset_mask",
		 wdesc->name);

	return trtl_sysfs_printf(path, "%x", mask);
}


/**
 * Load a trtl CPU firmware from a given buffer
 * @param[in] trtl device token
 * @param[in] index CPU index
 * @param[in] code buffer containing the CPU firmware binary code
 * @param[in] length code length
 * @param[in] offset memory offset where to start to write the code
 *
 * The CPU must be in reset mode in order to be programmed. This is
 * done automatically by the driver which will leave the CPU in
 * reset mode. The user must clear the reset status in order to run
 * the firmware.
 *
 * @return the number of written byte, on error -1 and errno is
 *         set appropriately
 */
int trtl_cpu_load_application_raw(struct trtl_dev *trtl,
				  unsigned int index,
				  const void *code, size_t length,
				  unsigned int offset)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;
	char path[TRTL_PATH_LEN + TRTL_NAME_LEN + 1 + 3];
	int fd;
	ssize_t ret = 0;
	size_t i = 0;

	snprintf(path, sizeof(path), "%s/%s-%02u",
		 wdesc->path, wdesc->name, index);
	fd = open(path, O_WRONLY);
	if (fd < 0)
		return -1;
	lseek(fd, offset, SEEK_SET);

	errno = ENOMEM;
	while (i < length) {
		ret = write(fd, code, length);
		if (ret <= 0)
			break;
		i += ret;
	}

	close(fd);
	return ret <= 0 ? ret : i;
}


/**
 * Dump a TRTL CPU firmware into a given buffer
 * @param[in] trtl device token
 * @param[in] index CPU index
 * @param[out] code buffer containing the CPU firmware binary code
 * @param[in] length code length
 * @param[in] offset memory offset where to start to write the code
 *
 * For a reliable dump, the CPU must be in pause. This is done by
 * the driver which then will set back the previous situation.
 *
 * @return the number of written byte, on error -1 and errno is
 *         set appropriately
 */
int trtl_cpu_dump_application_raw(struct trtl_dev *trtl,
				  unsigned int index,
				  void *code, size_t length,
				  unsigned int offset)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;
	char path[TRTL_PATH_LEN + TRTL_NAME_LEN + 1 + 3];
	int fd, i = 0, c = 100;

	snprintf(path, sizeof(path), "%s/%s-%02u",
		 wdesc->path, wdesc->name, index);
	fd = open(path, O_RDONLY);
	if (fd < 0)
		return -1;

	lseek(fd, offset, SEEK_SET);

	while (i < length && --c)
		i += read(fd, (char *)code + i, length - i);
	if (!c)
		fprintf(stderr, "Cannot read all the CPU firmware\n");

	close(fd);
	return i;
}


/**
 * Load a TRTL CPU firmware from a given file
 * @param[in] trtl device token
 * @param[in] index CPU index
 * @param[in] path path to the firmware file
 *
 * After extracting the data from the file, it internally uses
 * trtl_cpu_load_application_raw().
 *
 * @return 0 on success, on error -1 and errno is set appropriately
 */
int trtl_cpu_load_application_file(struct trtl_dev *trtl,
				   unsigned int index,
				   const char *path)
{
	int i, len, err = 0;
	void *code;
	FILE *f;

	f = fopen(path, "rb");
	if (!f)
		return -1;

	fseek(f, 0, SEEK_END);
	len = ftell(f);
	rewind(f);
	if (!len)
		goto out;

	code = malloc(len);
	if (!code) {
		err = -1;
		goto out_close;
	}

	/* Get the code from file */
	i = fread(code, 1, len, f);
	if (!i || i != len) {
		err = -1;
		goto out_free;
	}

	i = trtl_cpu_load_application_raw(trtl, index, code, len, 0);
	if (i != len)
		err = -1;
out_free:
	free(code);
out_close:
	fclose(f);
out:
	return err;
}


/**
 * Dump a TRTL CPU firmware into a given file
 * @param[in] trtl device token
 * @param[in] index CPU index
 * @param[in] path path to the firmware file
 *
 * It internally uses trtl_cpu_dump_application_raw().
 *
 * @return 0 on success, on error -1 and errno is set appropriately
 */
int trtl_cpu_dump_application_file(struct trtl_dev *trtl,
				   unsigned int index,
				   const char *path)
{
	int i = 0;
	uint8_t code[4096];
	FILE *f;

	f = fopen(path, "wb");
	if (!f)
		return -1;

        do {
		/* Read from driver and write into file */
		i += trtl_cpu_dump_application_raw(trtl, index, code, 4096, i);
		if (i != 0)
			fwrite(code, 1, i, f);
	} while(i % 4096 == 0);

	fclose(f);

	return i;
}


/**
 * Open a TRTL device
 * @param[in] wdesc device descriptor
 * @return 0 on success, -1 on error and errno is set appropriately
 */
static int trtl_dev_open(struct trtl_desc *wdesc)
{
	if (wdesc->fd_dev < 0) {
		char path[TRTL_PATH_LEN + TRTL_NAME_LEN + 1];

		snprintf(path, sizeof(path), "%s/%s", wdesc->path, wdesc->name);
		wdesc->fd_dev = open(path, O_RDWR);
		if (wdesc->fd_dev < 0)
			return -1;

	}
	return 0;
}

/**
 * It execute the ioctl command to read/write an smem address
 * @param[in] wdesc device descriptor
 * @param[in] addr memory address where start the operations
 * @param[in, out] data value in/to the shared memory
 * @param[in] count number of values in data
 * @param[in] mod the write modifier to be used on output
 * @param[in] is_input 1 for input, 0 for output
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
static int trtl_smem_io(struct trtl_desc *wdesc,
			uint32_t addr, uint32_t *data, size_t count,
			enum trtl_smem_modifier mod, int is_input)
{
	struct trtl_smem_io io;
	int err, i;

	err = trtl_dev_open(wdesc);
	if (err)
		return -1;

	io.is_input = is_input;
	io.mod = mod;
	for (i = 0; i < count; i++) {
		io.addr = addr + (i * 4);
		if (!io.is_input)
			io.value = data[i];
		err = ioctl(wdesc->fd_dev, TRTL_IOCTL_SMEM_IO, &io);
		if (err)
			return -1;
		data[i] = io.value;
	}


	return 0;
}

/**
 * Direct acces to the shared memory to read a set of cells
 * @param[in] trtl device token
 * @param[in] addr memory address where start the operations
 * @param[out] data values read from in the shared memory. The function will
 *             replace this value with the read back value
 * @param[in] count number of values in data
 * @param[in] mod shared memory operation mode
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_smem_read(struct trtl_dev *trtl, uint32_t addr, uint32_t *data,
		   size_t count, enum trtl_smem_modifier mod)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;

	return trtl_smem_io(wdesc, addr, data, count, mod, 1);
}


/**
 * Write on the shared memory of the TRTL
 * @param[in] trtl device to use
 * @param[in] addr memory address
 * @param[in, out] data values to write in the shared memory. The function will
 *                 replace this value with the read back value
 * @param[in] count number of values in data
 * @param[in] mod shared memory operation mode
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_smem_write(struct trtl_dev *trtl, uint32_t addr, uint32_t *data,
		    size_t count, enum trtl_smem_modifier mod)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;

	return trtl_smem_io(wdesc, addr, data, count, mod, 0);
}


/**
 * Enable a CPU; in other words, it clears the reset line of a CPU.
 * This function is a wrapper of trtl_cpu_reset_set() that allow you to safely
 * enable a single CPU.
 * @param[in] trtl device token
 * @param[in] index CPU index
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_cpu_enable(struct trtl_dev *trtl, unsigned int index)
{
	uint32_t tmp;

	trtl_cpu_reset_get(trtl, &tmp);
	return trtl_cpu_reset_set(trtl, tmp & ~(1 << index));
}


/**
 * Disable a CPU; in other words, it sets the reset line of a CPU.
 * This function is a wrapper of trtl_cpu_reset_set() that allows you to safely
 * disable a single CPU.
 * @param[in] trtl device token
 * @param[in] index CPU index
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_cpu_disable(struct trtl_dev *trtl, unsigned int index)
{
	uint32_t tmp;

	trtl_cpu_reset_get(trtl, &tmp);
	return trtl_cpu_reset_set(trtl, tmp | (1 << index));
}


/**
 * Check if the CPU is enabled (or not)
 * @param[in] trtl device token
 * @param[in] index CPU index
 * @param[out] enable 1 if the CPU is enable
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_cpu_is_enable(struct trtl_dev *trtl, unsigned int index,
			unsigned int *enable)
{
	uint32_t tmp, err;

	err = trtl_cpu_reset_get(trtl, &tmp);
	if (err)
		return -1;

	*enable = ((tmp & (1 << index)) ? 0 : 1);
	return 0;
}


/**
 * Return the device name
 * @param[in] trtl device token
 * @return the string representing the name of the device
 */
char *trtl_name_get(struct trtl_dev *trtl)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;

	return wdesc->name;
}


/**
 * Return the device ID
 * @param[in] trtl device token
 * @return the device ID. If the ID can't be retrived, then
 *         it will return 0xFFFFFFFF
 */
uint32_t trtl_id_get(struct trtl_dev *trtl)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;

	return trtl ? wdesc->devid : ~0;
}

/**
 * Return information about the synthesis configuration
 * @param[in] trtl device token
 * @return a pointer to a config structure
 *
 * NOTE: you do not have to free(2) the pointer.
 */
const struct trtl_config_rom *trtl_config_get(struct trtl_dev *trtl)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;

	return &wdesc->cfgrom;
}


/**
 * Return the HMQ File Descriptor
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @return the file descriptor
 */
int trtl_hmq_fd(struct trtl_dev *trtl,
		unsigned int idx_cpu,
		unsigned int idx_hmq)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;

	return wdesc->fd_hmq[idx_cpu][idx_hmq];
}

/**
 * Return the HMQ File Descriptor
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @return the file descriptor
 */
int trtl_hmq_fd_sync(struct trtl_dev *trtl,
		     unsigned int idx_cpu,
		     unsigned int idx_hmq)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;

	return wdesc->fd_hmq_sync[idx_cpu][idx_hmq];
}

static int trtl_msg_read(int fd, struct trtl_msg *msg, unsigned int n)
{
	size_t size = sizeof(*msg);
	int ret;

	ret = read(fd, msg, size * n);
	if (ret < 0)
		return ret;
	if (ret % size) {
		/* most likely a driver problem */
		errno = ETRTL_INVALID_MESSAGE;
		return -1;
	}

	return (ret / size);
}

/**
 * Read messages from a given HMQ
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[out] msg pre-allocated memory where storing the messages
 * @param[in] n maximum number of messages to read
 * @return on success the number of valid messages, otherwise -1 and errno
 *         is set appropriately
 */
int trtl_msg_async_recv(struct trtl_dev *trtl,
			unsigned int idx_cpu,
			unsigned int idx_hmq,
			struct trtl_msg *msg,
			unsigned int n)
{
	int fd = trtl_hmq_fd(trtl, idx_cpu, idx_hmq);

	/* validation */
	if (fd < 0) {
		errno = ETRTL_HMQ_CLOSE;
		return -1;
	}

	return trtl_msg_read(fd, msg, n);
}

static int trtl_msg_write(int fd, struct trtl_msg *msg, unsigned int n)
{
	size_t size = sizeof(*msg);
	int ret;

	ret = write(fd, msg, size * n);
	if (ret < 0)
		return -1;
	if (ret % size) {
		/* most likely a driver problem */
		errno = ETRTL_INVALID_MESSAGE;
		return -1;
	}

	return (ret / size);
}
/**
 * Write messages to a given HMQ
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in] msg messages to write
 * @param[in] n maximum number of messages to write
 * @return on success the number of valid messages, otherwise -1 and errno
 *         is set appropriately
 */
int trtl_msg_async_send(struct trtl_dev *trtl,
			unsigned int idx_cpu,
			unsigned int idx_hmq,
			struct trtl_msg *msg,
			unsigned int n)
{
	int fd = trtl_hmq_fd(trtl, idx_cpu, idx_hmq);

	/* validation */
	if (fd < 0) {
		errno = ETRTL_HMQ_CLOSE;
		return -1;
	}
	return trtl_msg_write(fd, msg, n);
}

/**
 * Add a new filter to the given hmq descriptor
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in] err error code
 * @return 0 on success, -1 otherwise and errno is set appropriately
 */
int trtl_msg_sync_abort(struct trtl_dev *trtl,
			unsigned int idx_cpu,
			unsigned int idx_hmq,
			unsigned int err)
{
	return ioctl(trtl_hmq_fd_sync(trtl, idx_cpu, idx_hmq),
		     TRTL_IOCTL_MSG_SYNC_ABORT, err);
}

/**
 * Send and receive a synchronous message.
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @param[in] msg_s message to send
 * @param[out] msg_r message received
 * @param[in] timeout like poll(2)
 * @return 0 on success, otherwise -1 and errno is set appropriately
 *
 * This function configures some filters, so it does some bit magic which have
 * been tested on a little-endian host.
 */
int trtl_msg_sync(struct trtl_dev *trtl,
		  unsigned int idx_cpu,
		  unsigned int idx_hmq,
		  struct trtl_msg *msg_s,
		  struct trtl_msg *msg_r,
		  int timeout)
{
	struct pollfd p;
	int ret, fd;

	/* send message */
	fd = trtl_hmq_fd_sync(trtl, idx_cpu, idx_hmq);
	ret = trtl_msg_write(fd, msg_s, 1);
	if (ret < 0)
		goto err;
	if (ret == 0) {
		errno = ETRTL_MSG_SYNC_FAILED_SEND;
		goto err;
	}

	/* wait answer */
	p.fd = fd;
	p.events = POLLIN | POLLERR;
	ret = poll(&p, 1, timeout);
	if (ret < 0)
		return -1;
	/* I (git blame) know, this if is over-complicated */
	if (ret == 0) {
		errno = ETRTL_MSG_SYNC_FAILED_RECV_TIMEOUT;
		goto err_abort;
	}
	if ((p.revents & POLLERR)) {
		errno = ETRTL_MSG_SYNC_FAILED_RECV_POLLERR;
		goto err_abort;
	}
	if (!(p.revents & POLLIN)) {
		errno = ETRTL_MSG_SYNC_FAILED_RECV;
		goto err_abort;
	}


	/* read the answer */
	ret = trtl_msg_read(fd, msg_r, 1);
	if (ret < 0)
		goto err;
	if (ret == 0) {
		errno = ETRTL_MSG_SYNC_FAILED_RECV;
		goto err;
	}

	return 0;

err_abort:
	trtl_msg_sync_abort(trtl, idx_cpu, idx_hmq, errno);
err:
	return -1;
}


/**
 * Wait for one of a set of Mock Turtle HMQ to become ready to perform I/O
 * @param[in,out] trtlp specific Mock Turtle poll descriptor
 * @param[in] npolls like in poll(2)
 * @param[in] timeout like in poll(2)
 * @return like poll(2)
 */
int trtl_msg_poll(struct polltrtl *trtlp,
		  unsigned int npolls, int timeout)
{
	struct pollfd *p;
	int i, ret;

	if (!trtlp || !npolls) {
		errno = EINVAL;
		return -1;
	}
	p = calloc(npolls, sizeof(struct pollfd));
	if (!p)
		return -1;

	for (i = 0; i < npolls; ++i) {
		struct trtl_desc *wdesc = (struct trtl_desc *)trtlp[i].trtl;

		if (!wdesc) {
			/* ignore it */
			p[i].fd = -1;
			continue;
		}

		p[i].fd = wdesc->fd_hmq[trtlp[i].idx_cpu][trtlp[i].idx_hmq];
		p[i].events = trtlp[i].events;
	}

	ret = poll(p, npolls, timeout);

	for (i = 0; i < npolls; ++i) {
		struct trtl_desc *wdesc = (struct trtl_desc *)trtlp[i].trtl;

		if (!wdesc) {
			p[i].events = 0;
			continue;
		}
		trtlp[i].revents = p[i].revents;
	}

	free(p);

	return ret;
}


/**
 * Flush the content of an HMQ for both input and output channel
 * @param[in] trtl device token
 * @param[in] idx_cpu CPU index
 * @param[in] idx_hmq HMQ index
 * @return 0 on success, otherwise -1 and errno is set appropriately
 */
int trtl_hmq_flush(struct trtl_dev *trtl,
		   unsigned int idx_cpu,
		   unsigned int idx_hmq)
{
	struct trtl_desc *wdesc = (struct trtl_desc *)trtl;
	char path[TRTL_SYSFS_PATH_LEN];

	snprintf(path, TRTL_SYSFS_PATH_LEN,
		 "/sys/class/mockturtle/%s-%02u-%02u/discard_all",
		 wdesc->name, idx_cpu, idx_hmq);

	return trtl_sysfs_printf(path, "1");
}
