/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#ifndef __LIBTRTL_INTERNAL_H__
#define __LIBTRTL_INTERNAL_H__
#include <mockturtle/libmockturtle.h>

/**
 * Maximum size for device path
 */
#define TRTL_PATH_LEN 64

/**
 * Internal descriptor for a TRTL device
 */
struct trtl_desc {
	uint32_t devid; /**< device instance identifier */
	char name[TRTL_NAME_LEN]; /**< Name of the device */
	char path[TRTL_PATH_LEN]; /**< path to device */
	int fd_dev; /**< File Descriptor of the device */
	int fd_cpu[TRTL_MAX_CPU];  /**< File Descriptor of the CPUs */
	int fd_hmq[TRTL_MAX_CPU][TRTL_MAX_MQ_CHAN]; /**< File Descriptors for the HMQ */
	int fd_hmq_sync[TRTL_MAX_CPU][TRTL_MAX_MQ_CHAN]; /**< File Descriptors for the HMQ in sync mode */
	struct trtl_config_rom cfgrom;/**< synthesis configuration */
};

#endif
