/*
 * SPDX-FileCopyrightText: 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/moduleparam.h>
#include <linux/idr.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/interrupt.h>
#include <linux/byteorder/generic.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/tty.h>
#include <linux/platform_device.h>
#include <linux/mod_devicetable.h>
#include <linux/uaccess.h>
#include <linux/version.h>
#include <linux/io.h>

#include <mockturtle/hw/mockturtle_cpu_csr.h>
#include <mockturtle/hw/mockturtle_queue.h>

#include "mockturtle-drv.h"
#include "mockturtle-compat.h"

static DEFINE_IDA(trtl_ida);

#if KERNEL_VERSION(6, 2, 0) <= LINUX_VERSION_CODE
static int trtl_dev_uevent(const struct device *dev, struct kobj_uevent_env *env)
#else
static int trtl_dev_uevent(struct device *dev, struct kobj_uevent_env *env)
#endif
{
	add_uevent_var(env, "DEVMODE=%#o", 0660);

	return 0;
}

#if KERNEL_VERSION(6, 2, 0) <= LINUX_VERSION_CODE
static char *trtl_devnode(const struct device *dev, umode_t *mode)
#else
static char *trtl_devnode(struct device *dev, umode_t *mode)
#endif
{
	return kasprintf(GFP_KERNEL, "mockturtle/%s", dev_name(dev));
}

struct class trtl_cdev_class = {
	.name		= "mockturtle",
	.owner		= THIS_MODULE,
	.dev_uevent	= trtl_dev_uevent,
	.devnode	= trtl_devnode,
};

static dev_t basedev;
static struct cdev cdev_dev;
static struct cdev cdev_cpu;
static struct cdev cdev_hmq;

struct device *minors[TRTL_MAX_MINORS];


/**
 * It return the first available char device minor for a given type.
 */
int trtl_minor_get(struct device *dev, enum trtl_dev_type type)
{
	int id, start, end;

	switch (type) {
	case TRTL_DEV:
		start = 0;
		end = start + TRTL_MAX_CARRIER;
		break;
	case TRTL_CPU:
		start = TRTL_MAX_CARRIER;
		end = start + TRTL_MAX_CPU_MINORS;
		break;
	case TRTL_HMQ:
		start = TRTL_MAX_CARRIER + TRTL_MAX_CPU_MINORS;
		end = start + TRTL_MAX_HMQ_MINORS;
		break;
	default:
		return -1;
	}

	id = ida_simple_get(&trtl_ida, start, end, GFP_KERNEL);
	if (id < 0) {
		dev_err(dev, " can't assign minor number\n");
		return id;
	}
	minors[id] = dev;
	dev->id = id;
	dev->devt = basedev + id;

	return 0;
}


/**
 * Release the char device minor is use by a given device
 */
void trtl_minor_put(struct device *dev)
{
	ida_simple_remove(&trtl_ida, dev->id);
}




/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * SYSFS * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


/**
 * Return the application ID
 */
static ssize_t application_id_show(struct device *dev,
				   struct device_attribute *attr,
				   char *buf)
{
	struct trtl_dev *trtl = to_trtl_dev(dev);

	return sprintf(buf, "0x%x\n", trtl->cfgrom.app_id);
}

/**
 * Return the number of CPU in the FPGA
 */
static ssize_t n_cpu_show(struct device *dev,
			  struct device_attribute *attr,
			  char *buf)
{
	struct trtl_dev *trtl = to_trtl_dev(dev);

	return sprintf(buf, "%d\n", trtl->cfgrom.n_cpu);
}


/**
 * Return the reset status of all CPUs as bitmask
 */
static ssize_t reset_mask_show(struct device *dev,
			       struct device_attribute *attr,
			       char *buf)
{
	struct trtl_dev *trtl = to_trtl_dev(dev);
	uint32_t reg_val;

	reg_val = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_RESET);

	return sprintf(buf, "0x%04x\n", reg_val);
}

/**
 * Set the reset status of all CPUs as bitmask
 */
static ssize_t reset_mask_store(struct device *dev,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct trtl_dev *trtl = to_trtl_dev(dev);
	long val;

	if (kstrtol(buf, 16, &val))
		return -EINVAL;

	trtl_iowrite(trtl, val, trtl->base_csr + MT_CPU_CSR_REG_RESET);

	return count;
}


DEVICE_ATTR(application_id, 0444, application_id_show, NULL);
DEVICE_ATTR(n_cpu, 0444, n_cpu_show, NULL);
DEVICE_ATTR(reset_mask, 0664, reset_mask_show, reset_mask_store);

static struct attribute *trtl_dev_attr[] = {
	&dev_attr_application_id.attr,
	&dev_attr_n_cpu.attr,
	&dev_attr_reset_mask.attr,
	NULL,
};

static const struct attribute_group trtl_dev_group = {
	.attrs = trtl_dev_attr,
};

static const struct attribute_group *trtl_dev_groups[] = {
	&trtl_dev_group,
	NULL,
};





/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * DEV CHAR DEVICE * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Things to do after device release
 */
static void trtl_dev_release(struct device *dev)
{
	struct trtl_dev *trtl = to_trtl_dev(dev);

	trtl_minor_put(dev);

	dev_info(dev, "%s: Application ID: 0x%08x\n",
		 __func__, trtl->cfgrom.app_id);
}

/**
 * ioctl command to read/write shared memory
 */
static long trtl_ioctl_io(struct trtl_dev *trtl, void __user *uarg)
{
	struct trtl_smem_io io;
	void *addr;
	int err;

	/* Copy the message from user space*/
	err = copy_from_user(&io, uarg, sizeof(struct trtl_smem_io));
	if (err)
		return err;
	if (io.addr >= trtl->cfgrom.smem_size * 4)
		return -EINVAL;

	addr = trtl->base_smem + io.addr;
	if (!io.is_input) {
		trtl_iowrite(trtl, io.mod,
			     trtl->base_csr + MT_CPU_CSR_REG_SMEM_OP);
		trtl_iowrite(trtl, io.value, addr);
	}

	/* read value from SMEM */
	io.value = trtl_ioread(trtl, addr);

	return copy_to_user(uarg, &io, sizeof(struct trtl_smem_io));
}

static long trtl_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{
	struct trtl_dev *trtl = f->private_data;
	void __user *uarg = (void __user *)arg;
	int err = 0;

	/* Check type and command number */
	if (_IOC_TYPE(cmd) != TRTL_IOCTL_MAGIC)
		return -ENOTTY;

#if KERNEL_VERSION(5, 0, 0) <= LINUX_VERSION_CODE
	err = !access_ok(uarg, _IOC_SIZE(cmd));
#else
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE, uarg, _IOC_SIZE(cmd));
	if (_IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ, uarg, _IOC_SIZE(cmd));
#endif
	if (err)
		return -EFAULT;

	/* Perform commands */
	switch (cmd) {
	case TRTL_IOCTL_SMEM_IO:
		err = trtl_ioctl_io(trtl, uarg);
		break;
	default:
		pr_warn("ual: invalid ioctl command %d\n", cmd);
		return -EINVAL;
	}

	return err;
}


/**
 * Open the char device on the top of the hierarchy
 */
static int trtl_dev_simple_open(struct inode *inode, struct file *file)
{
	int m = iminor(inode);

	file->private_data = to_trtl_dev(minors[m]);

	return 0;
}

static const struct file_operations trtl_dev_fops = {
	.owner = THIS_MODULE,
	.open  = trtl_dev_simple_open,
	.llseek = generic_file_llseek,
	.unlocked_ioctl = trtl_ioctl,
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * DRIVER (un)LOADING  * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Things to do after CPU device release
 */
static void trtl_cpu_release(struct device *dev)
{
	trtl_minor_put(dev);
}


#define TRTL_SLOT_CFG(_name, _val)                          \
	(1 << ((_val & TRTL_MQ_SLOT_STATUS_LOG2_##_name##_MASK) \
	       >> TRTL_MQ_SLOT_STATUS_LOG2_##_name##_SHIFT))

/**
 * Register a new CPU
 */
static int trtl_probe_cpu(struct trtl_dev *trtl, unsigned int cpu_idx)
{
	struct trtl_cpu *cpu = &trtl->cpu[cpu_idx];
	int err, i;

	cpu->index = cpu_idx;
	spin_lock_init(&cpu->lock);
	init_waitqueue_head(&cpu->q_notify);
	cpu->idx_r = 0;
	cpu->idx_w = 0;

	err = trtl_minor_get(&cpu->dev, TRTL_CPU);
	if (err)
		goto out_minor;
	err = dev_set_name(&trtl->cpu[cpu->index].dev, "%s-%02d",
			   dev_name(&trtl->dev), cpu->index);
	if (err)
		goto out_name;
	cpu->dev.class = &trtl_cdev_class;
	cpu->dev.parent = &trtl->dev;
	cpu->dev.groups = trtl_cpu_groups;
	cpu->dev.release = trtl_cpu_release;
	err = device_register(&cpu->dev);
	if (err)
		goto out_reg;

	for (i = 0; i < trtl->cfgrom.n_hmq[cpu->index]; ++i) {
		err = trtl_probe_hmq(cpu, i);
		if (err)
			goto out_hmq;
	}

	return 0;

out_hmq:
	i = trtl->cfgrom.n_hmq[cpu->index];
	while (--i >= 0)
		trtl_remove_hmq(cpu, i);
out_reg:
out_name:
	trtl_minor_put(&cpu->dev);
out_minor:
	return err;
}


/**
 * Unregister a CPU
 */
static void trtl_remove_cpu(struct trtl_dev *trtl, unsigned int cpu_idx)
{
	struct trtl_cpu *cpu = &trtl->cpu[cpu_idx];
	int i;

	for (i = 0; i < trtl->cfgrom.n_hmq[cpu->index]; ++i)
		trtl_remove_hmq(cpu, i);

	device_unregister(&cpu->dev);
}


static ssize_t trtl_config_rom_read(struct file *file, struct kobject *kobj,
				    struct bin_attribute *bin_attr,
				    char *buf, loff_t off, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct trtl_dev *trtl = to_trtl_dev(dev);

	memcpy(buf, &trtl->cfgrom, count);

	return count;
}

struct bin_attribute trtl_config_rom_sysfs = {
	.attr = {
		.name = "config-rom",
		.mode = 0444,
	},
	.size = sizeof(struct trtl_config_rom),
	.read = trtl_config_rom_read,
};

static int trtl_config_rom_dump(struct trtl_dev *trtl)
{

	uint32_t *rom = (uint32_t *)&trtl->cfgrom;
	int i;

	for (i = 0; i < sizeof(struct trtl_config_rom) / 4; ++i)
		rom[i] = trtl_ioread(trtl, trtl->base_cfg + i * 4);

	/* ROM validation */
	if (trtl->cfgrom.signature != TRTL_CONFIG_ROM_SIGNATURE) {
		dev_err(&trtl->dev,
			"Invalid configuration ROM; expected signature '0x%x' got '0x%x')\n",
			TRTL_CONFIG_ROM_SIGNATURE, trtl->cfgrom.signature);
		return -EINVAL;
	}

	if (trtl->cfgrom.n_cpu > TRTL_MAX_CPU) {
		dev_err(&trtl->dev,
			"Invalid configuration ROM; expected 'n_cpu' to be less than %d, got %d)\n",
			TRTL_MAX_CPU, trtl->cfgrom.n_cpu);
		return -EINVAL;
	}

	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		if (trtl->cfgrom.n_hmq[i] > TRTL_MAX_MQ_CHAN) {
			dev_err(&trtl->dev,
				"Invalid configuration ROM; expected 'n_hmq' to be less than %d, got %d)\n",
				TRTL_MAX_MQ_CHAN, trtl->cfgrom.n_hmq[i]);
			return -EINVAL;
		}

		if (trtl->cfgrom.n_rmq[i] > TRTL_MAX_MQ_CHAN) {
			dev_err(&trtl->dev,
				"Invalid configuration ROM; expected 'n_rmq' to be less than %d, got %d)\n",
				TRTL_MAX_MQ_CHAN, trtl->cfgrom.n_rmq[i]);
			return -EINVAL;
		}
	}

	return 0;
}

static int trtl_resource_validation(struct platform_device *pdev)
{
	struct resource *r;

	r = platform_get_resource(pdev, IORESOURCE_IRQ, TRTL_IRQ_HMQ_OUT);
	if (!r) {
		dev_err(&pdev->dev,
			"Mock Turtle need an interrupt source for the Host Message Queue - Output - in order to receive messages on IRQ (mandatory)\n");
		return -ENXIO;
	}

	r = platform_get_resource(pdev, IORESOURCE_MEM, TRTL_MEM_BASE);
	if (!r) {
		dev_err(&pdev->dev,
			"Mock Turtle need base address\n");
		return -ENXIO;
	}

	return 0;
}

static int trtl_endianess(struct trtl_dev *trtl)
{
	uint32_t signature;

	signature = ioread32(trtl->base_cfg);
	if (signature == TRTL_CONFIG_ROM_SIGNATURE)
		return 0;
	signature = ioread32be(trtl->base_cfg);
	if (signature == TRTL_CONFIG_ROM_SIGNATURE)
		return 1;

	return -1;

}

static int trtl_memops_detect(struct trtl_dev *trtl)
{
	switch (trtl_endianess(trtl)) {
	case 0:
		trtl->memops.read = ioread32;
		trtl->memops.write = iowrite32;
		return 0;
	case 1:
		trtl->memops.read = ioread32be;
		trtl->memops.write = iowrite32be;
		return 0;
	default:
		dev_err(&trtl->dev, "Invalid endianess\n");
		return -EINVAL;
	}
}


static unsigned int trtl_max_irq_loop(struct trtl_dev *trtl)
{
	unsigned int max = 0, size;
	int i, k;

	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		for (k = 0; k < trtl->cfgrom.n_hmq[i]; ++k) {
			size = TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(trtl->cfgrom.hmq[i][k].sizes);
			if (size > max)
				max = size;
		}
	}
	return max;
}

/**
 * It initialize the TRTL device (device, CPUs, HMQs)
 */
int trtl_probe(struct platform_device *pdev)
{
	struct trtl_dev *trtl;
	int err, i;
	struct resource *r;

	err = trtl_resource_validation(pdev);
	if (err)
		return err;

	/* Create a MockTurtle instance */
	trtl = devm_kzalloc(&pdev->dev, sizeof(struct trtl_dev), GFP_KERNEL);
	if (!trtl)
		return -ENOMEM;
	spin_lock_init(&trtl->lock_cpu_sel);
	spin_lock_init(&trtl->lock_hmq_sel);

	platform_set_drvdata(pdev, trtl);

	r = platform_get_resource(pdev, IORESOURCE_MEM, TRTL_MEM_BASE);
	trtl->base_core = devm_ioremap_resource(&pdev->dev, r);
	if (!trtl->base_core)
		return -EADDRNOTAVAIL;
	trtl->base_csr  = trtl->base_core + TRTL_ADDR_OFFSET_CSR;
	trtl->base_smem = trtl->base_core + TRTL_ADDR_OFFSET_SHM;
	trtl->base_hmq  = trtl->base_core + TRTL_ADDR_OFFSET_HMQ;
	trtl->base_cfg = trtl->base_core + TRTL_ADDR_OFFSET_CONFIG_ROM;

	/* Register the device */
	err = dev_set_name(&trtl->dev, "trtl-%04x", pdev->id);
	if (err)
		goto out_name;
	err = trtl_minor_get(&trtl->dev, TRTL_DEV);
	if (err)
		goto out_minor;
	trtl->dev.class = &trtl_cdev_class;
	trtl->dev.parent = &pdev->dev;
	trtl->dev.groups = trtl_dev_groups;
	trtl->dev.release = trtl_dev_release;
	err = device_register(&trtl->dev);
	if (err)
		goto out_reg;

	err = trtl_memops_detect(trtl);
	if (err)
		goto out_mops;

	err = trtl_config_rom_dump(trtl);
	if (err)
		goto out_rom;

	err = sysfs_create_bin_file(&trtl->dev.kobj, &trtl_config_rom_sysfs);
	if (err) {
		dev_err(&trtl->dev,
			"Can't create sysfs binary attribute for config ROM\n");
		goto out_cfg;
	}

	trtl->max_irq_loop = trtl_max_irq_loop(trtl);
	/* Reset all CPUs */
	trtl_cpu_reset_set(trtl, (1 << trtl->cfgrom.n_cpu) - 1);

	/* Configure CPUs */
	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		err = trtl_probe_cpu(trtl, i);
		if (err)
			goto out_cpu;
	}

	r = platform_get_resource(pdev, IORESOURCE_IRQ, TRTL_IRQ_HMQ_IN);
	err = request_any_context_irq(r->start, trtl_irq_handler_in, 0,
				      r->name, trtl);
	if (err < 0) {
		dev_err(&trtl->dev,
			"Cannot request IRQ %lld - we'll not receive messages\n",
			r->start);
	}

	r = platform_get_resource(pdev, IORESOURCE_IRQ, TRTL_IRQ_HMQ_OUT);
	err = request_any_context_irq(r->start, trtl_irq_handler_out, 0,
				      r->name, trtl);
	if (err < 0) {
		dev_err(&trtl->dev,
			"Cannot request IRQ %lld - we'll not receive messages\n",
			r->start);
	}

	r = platform_get_resource(pdev, IORESOURCE_IRQ, TRTL_IRQ_NOT);
	err = request_any_context_irq(r->start, trtl_cpu_irq_handler_not, 0,
				      r->name, trtl);
	if (err < 0) {
		dev_err(&trtl->dev,
			"Cannot request IRQ %lld - we'll not receive CPU notifications\n",
			r->start);
	}


	err = trtl_tty_probe(trtl);
	if (err)
		dev_err(&trtl->dev, "Console not available (err: %d)\n", -err);

	/* TODO Pin the carrier - do we need this? */

	dev_info(&trtl->dev, "%s: Application ID: 0x%08x\n",
		 __func__, trtl->cfgrom.app_id);

	trtl_debugfs_init(trtl);

	return 0;

out_cpu:
	while (--i >= 0)
		trtl_remove_cpu(trtl, i);
out_rom:
	sysfs_remove_bin_file(&trtl->dev.kobj, &trtl_config_rom_sysfs);
out_cfg:
out_mops:
	device_unregister(&trtl->dev);
out_reg:
	trtl_minor_put(&trtl->dev);
out_minor:
out_name:
	return err;
}

/**
 * It remove the TRTL device (device, CPUs, HMQs) and free irq handler
 */
int trtl_remove(struct platform_device *pdev)
{
	struct trtl_dev *trtl = platform_get_drvdata(pdev);
	int i;

	trtl_debugfs_exit(trtl);

	trtl_tty_remove(trtl);

	for (i = 0; i < trtl->cfgrom.n_cpu; ++i)
		trtl_remove_cpu(trtl, i);

	/* all IRQ signals already disabled by trtl_remove_hmq */
	free_irq(platform_get_irq(pdev, TRTL_IRQ_HMQ_IN), trtl);
	free_irq(platform_get_irq(pdev, TRTL_IRQ_HMQ_OUT), trtl);
	free_irq(platform_get_irq(pdev, TRTL_IRQ_NOT), trtl);

	sysfs_remove_bin_file(&trtl->dev.kobj, &trtl_config_rom_sysfs);

	/* FIXME cannot explain why, but without sleep the _kernel_ crash */
	msleep(50);
	device_unregister(&trtl->dev);

	/* TODO Release the carrier - symmetric to pin */

	return 0;
}

static const struct platform_device_id trtl_id[] = {
	{
		.name = "mock-turtle",
	},
	{
		.name = "mockturtle",
	},
	{ .name = "" }, /* last */
};

static struct platform_driver trtl_platform_driver = {
	.driver = {
		.name = KBUILD_MODNAME,
	},
	.probe = trtl_probe,
	.remove = trtl_remove,
	.id_table = trtl_id,
};


/**
 * Allocate resources for the driver. Char devices and platform driver
 */
static int trtl_init(void)
{
	int err, i;

	for (i = 0; i < TRTL_MAX_CPU_MINORS; ++i)
		minors[i] = NULL;

	err = class_register(&trtl_cdev_class);
	if (err) {
		pr_err("%s: unable to register class\n", __func__);
		return err;
	}

	/* Allocate a char device region for devices, CPUs and slots */
	err = alloc_chrdev_region(&basedev, 0, TRTL_MAX_MINORS, "trtl");
	if (err) {
		pr_err("%s: unable to allocate region for %i minors\n",
		       __func__, TRTL_MAX_CPU_MINORS);
		goto out_all;
	}

	/* Register the device char-device */
	cdev_init(&cdev_dev, &trtl_dev_fops);
	cdev_dev.owner = THIS_MODULE;
	err = cdev_add(&cdev_dev, basedev, TRTL_MAX_CARRIER);
	if (err)
		goto out_cdev_dev;
	/* Register the cpu char-device */
	cdev_init(&cdev_cpu, &trtl_cpu_fops);
	cdev_cpu.owner = THIS_MODULE;
	err = cdev_add(&cdev_cpu, basedev + TRTL_MAX_CARRIER,
		       TRTL_MAX_CPU_MINORS);
	if (err)
		goto out_cdev_cpu;
	/* Register the hmq char-device */
	cdev_init(&cdev_hmq, &trtl_hmq_fops);
	cdev_cpu.owner = THIS_MODULE;
	err = cdev_add(&cdev_hmq,
		       basedev + TRTL_MAX_CARRIER + TRTL_MAX_CPU_MINORS,
		       TRTL_MAX_HMQ_MINORS);
	if (err)
		goto out_cdev_hmq;

	/* Register the platform driver */
	err = platform_driver_register(&trtl_platform_driver);
	if (err)
		goto out_reg;

	return 0;



out_reg:
	cdev_del(&cdev_hmq);
out_cdev_hmq:
	cdev_del(&cdev_cpu);
out_cdev_cpu:
	cdev_del(&cdev_dev);
out_cdev_dev:
	unregister_chrdev_region(basedev, TRTL_MAX_MINORS);
out_all:
	class_unregister(&trtl_cdev_class);
	return err;
}


/**
 * Undo all the resource allocations
 */
static void trtl_exit(void)
{
	platform_driver_unregister(&trtl_platform_driver);

	cdev_del(&cdev_hmq);
	cdev_del(&cdev_cpu);
	cdev_del(&cdev_dev);
	unregister_chrdev_region(basedev, TRTL_MAX_MINORS);
	class_unregister(&trtl_cdev_class);
}

module_init(trtl_init);
module_exit(trtl_exit);

MODULE_AUTHOR("Federico Vaga <federico.vaga@cern.ch>");
MODULE_DESCRIPTION("Mock Turtle Linux Driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(GIT_VERSION);
MODULE_DEVICE_TABLE(platform, trtl_id);

ADDITIONAL_VERSIONS;
