// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) 2022 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mfd/core.h>
#include <linux/mod_devicetable.h>

enum trtl_demo_dev_offsets {
	TRTL_DEMO_TRTL_MEM_START = 0x0001C000,
	TRTL_DEMO_TRTL_MEM_END = 0x0003C000,
};

/* MFD devices */
enum trtl_demo_fpga_mfd_devs_enum {
	TRTL_DEMO_MFD_TRTL = 0,
};

static struct resource trtl_demo_trtl_res[] = {
	{
		.name = "mock-turtle-mem",
		.flags = IORESOURCE_MEM,
		.start = TRTL_DEMO_TRTL_MEM_START,
		.end = TRTL_DEMO_TRTL_MEM_END,
	}, {
		.name = "mock-turtle-irq_in",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 0,
	}, {
		.name = "mock-turtle-irq_out",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 1,
	}, {
		.name = "mock-turtle-irq_con",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 2,
	}, {
		.name = "mock-turtle-irq_not",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 3,
	},
};

static const struct mfd_cell trtl_demo_mfd_devs[] = {
	[TRTL_DEMO_MFD_TRTL] = {
		.name = "mock-turtle",
		.platform_data = NULL,
		.pdata_size = 0,
		.num_resources = ARRAY_SIZE(trtl_demo_trtl_res),
		.resources = trtl_demo_trtl_res,
	},
};


static int trtl_demo_probe(struct platform_device *pdev)
{
	struct resource *rmem;
	int irq;

	rmem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!rmem) {
		dev_err(&pdev->dev, "Missing memory resource\n");
		return -EINVAL;
	}

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(&pdev->dev, "Missing IRQ number\n");
		return -EINVAL;
	}

	/*
	 * We know that this design uses the HTVIC IRQ controller.
	 * This IRQ controller has a linear mapping, so it is enough
	 * to give the first one as input
	 */

	return mfd_add_devices(&pdev->dev, PLATFORM_DEVID_AUTO,
			       trtl_demo_mfd_devs,
			       ARRAY_SIZE(trtl_demo_mfd_devs),
			       rmem, irq, NULL);
}

static int trtl_demo_remove(struct platform_device *pdev)
{
	mfd_remove_devices(&pdev->dev);

	return 0;
}

/**
 * List of supported platform
 */
enum trtl_demo_version {
	TRTL_DEMO_VER = 0,
};

static const struct platform_device_id trtl_demo_id_table[] = {
	{
		.name = "svec-trtl-demo",
		.driver_data = TRTL_DEMO_VER,
	}, {
		.name = "id:000010DC4D544456",
		.driver_data = TRTL_DEMO_VER,
	}, {
		.name = "id:000010dc4d544456",
		.driver_data = TRTL_DEMO_VER,
	},
	{},
};

static struct platform_driver trtl_demo_driver = {
	.driver = {
		.name = "svec-trtl-demo",
		.owner = THIS_MODULE,
	},
	.id_table = trtl_demo_id_table,
	.probe = trtl_demo_probe,
	.remove = trtl_demo_remove,
};
module_platform_driver(trtl_demo_driver);

MODULE_AUTHOR("Federico Vaga <federico.vaga@cern.ch>");
MODULE_LICENSE("GPL");
MODULE_VERSION(GIT_VERSION);
MODULE_DESCRIPTION("Driver for the SVEC Mockturtle DEMO");
MODULE_DEVICE_TABLE(platform, trtl_demo_id_table);

MODULE_SOFTDEP("pre: mockturtle");
