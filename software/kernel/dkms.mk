# SPDX-License-Identifier: GPL-2.0-or-later
#
# SPDX-FileCopyrightText: 2019 CERN

TOPDIR ?= $(shell pwd)/../../
TRTL ?= $(TOPDIR)
include $(TRTL)/common.mk

NAME := mockturtle
FULL_NAME := $(NAME)-$(VERSION)
DST := /tmp/dkms-source-$(FULL_NAME)

all: dkms_install

hw_header:
	@$(MAKE) -C $(TRTL_SW)/include/

# copy necessary source file to build this driver
dkms_sources: hw_header
	@mkdir -p $(DST)
	@cp mockturtle-*.[ch] Kbuild Makefile dkms.conf \
	    $(TRTL)/LICENSES/GPL-2.0-only.txt \
	    $(DST)
	@cp -a $(TRTL_SW)/include/mockturtle $(DST)
	@rm -f $(DST)/mockturtle/hw/Makefile

# fix the dkms source file so that the build system is consistent
# with its new location
dkms_sources_prep: dkms_sources
# we do not have git, copy the version
	@sed -e 's,$$(GIT_VERSION),$(GIT_VERSION),' \
	     -i $(DST)/Makefile
	@sed -r -e "s/@PKGVER@/$(VERSION)/" \
	     -i $(DST)/dkms.conf
# wbgen headers are embedded, do not try to build them
	@sed -r -e 's,^TOPDIR [?]= .*$$,TOPDIR ?= ./,' \
	     -e 's/\shw_header//' \
	     -e '/^hw_header:$$/,+1d' \
	     -i $(DST)/Makefile
# headers are in the same directory
	@sed -r -e '/^ccflags-y += -I$$(src)\/..\/include$$/d' \
	     -i $(DST)/Kbuild
# we do not use the common.mk, but we need PREFIX
	@sed -r -e 's/^include.*common.mk$$/PREFIX ?= \//' \
	     -i $(DST)/Makefile
# we do not have custom environment
	@sed -r -e "/^-include.*$$/d" \
	     -e "/^(TRTL|TOPDIR)\s[?]=.*$$/d" \
	     -i $(DST)/Makefile

dkms_install: dkms_sources_prep
	@mkdir -p $(PREFIX)/usr/src/$(FULL_NAME)/
	@cp -a $(DST)/* $(PREFIX)/usr/src/$(FULL_NAME)
