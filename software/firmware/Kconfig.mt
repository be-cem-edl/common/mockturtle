# SPDX-License-Identifier: LGPL-2.1-or-later
#
# SPDX-FileCopyrightText: 2019 CERN

comment "Mock Turtle configuration"

config FPGA_APPLICATION_ID
	int "FPGA application ID"
	default 0
	help
	  It is the FPGA application ID (hardcoded in the FPGA gateware) where
	  this firmware is expected to run. A value of 0x0 will make this
	  firmware running on any Mock Turtle design (default 0x0).

config RT_APPLICATION_ID
	int "RT application ID"
	default 0
	help
	  The firmware version as 32bit integer (16bit major number,
	  16bit minor number)

config CFLAGS_OPT
	string "Optimization level for the compiler"
	default "-Os"
	help
	  The optimization level to be passed to the compiler

config CFLAGS_EXTRA
	string "Extra flags for the compiler"
	default "-ggdb"
	help
	  Extra flags to be passed to the compiler

config MOCKTURTLE_SIMULATION
	bool "It enables the simulation mode"
	default n
	help
	  It compiles the firmware in order to run it in simulation mode.
	  This removes some delay in the framework and in the library; it
	  can be used by any firmware to be compiled for different targets.

comment "Mock Turtle framework configuration"
config MOCKTURTLE_FRAMEWORK_ENABLE
	bool "Enable Mock Turtle framework"
	default y
	help
	  It enables the Mock Turtle firmware framework. This framework
	  provides a well structured way to develop Mock turtle firmware
	  as wall as helpers for the most common operations.

config MOCKTURTLE_FRAMEWORK_DEBUG_ENABLE
	bool "Enable debugging in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ENABLE && MOCKTURTLE_LIBRARY_PRINT_ENABLE
	default n
	help
	  It enables the debug messages in the framework. Useful to
	  debug messages exchange or other framework internals

config MOCKTURTLE_FRAMEWORK_ACTION_ENABLE
	bool "Enable Mock Turtle Actions"
	default y
	help
	  It enables the Mock Turtle actions in framework. The action
	  is similar to an RPC: when the firmware receives a special
	  message it triggers a function execution.


config MOCKTURTLE_FRAMEWORK_VARIABLE_ENABLE
	bool "Enable variable in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ACTION_ENABLE
	default n
	help
	  It enables the API for local variable exchange with the host system.

config MOCKTURTLE_FRAMEWORK_BUFFER_ENABLE
	bool "Enable buffer in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ACTION_ENABLE
	default n
	help
	  It enables the API for local buffers exchange with the host system.

config MOCKTURTLE_FRAMEWORK_PING_ENABLE
	bool "Enable ping message in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ACTION_ENABLE
	default y
	help
	  It enables the API to send ping message to firmware.

config MOCKTURTLE_FRAMEWORK_NAME_ENABLE
	bool "Enable to read the application name in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ACTION_ENABLE
	default n
	help
	  It enables the API to get the application name from the firmware.

config MOCKTURTLE_FRAMEWORK_VERSION_ENABLE
	bool "Enable version message in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ACTION_ENABLE
	default y
	help
	  It enables the API to send version message to firmware.

config MOCKTURTLE_FRAMEWORK_VALUE_SEND_ENABLE
	bool "Enable value message send in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ENABLE
	default n
	help
	  It enables the API to automatically build and send a message
	  with the given values

config MOCKTURTLE_FRAMEWORK_BUFFER_SEND_ENABLE
	bool "Enable buffer message send in Mock Turtle framework"
	depends on MOCKTURTLE_FRAMEWORK_ENABLE
	default n
	help
	  It enables the API to automatically build and send a message
	  with a given buffer

comment "Mock Turtle library configuration"

config MOCKTURTLE_LIBRARY_PRINT_ENABLE
	bool "Enable print in Mock Turtle library"
	default y
	help
	  It enables `pp_printf` messages on serial console.
	  It is a compact version of `printf` but still quite big.
	  If you need space, do not enable this option.

config MOCKTURTLE_LIBRARY_PRINT_DEBUG_ENABLE
	bool "Enable print debug in Mock Turtle library"
	depends on MOCKTURTLE_LIBRARY_PRINT_ENABLE
	default n
	help
	  It enables `pr_debug` messages on serial console. When
	  disabled, those print messages are not compiled.
	  (based on `pp_printf`)

config MOCKTURTLE_LIBRARY_PRINT_ERROR_ENABLE
	bool "Enable print error in Mock Turtle library"
	depends on MOCKTURTLE_LIBRARY_PRINT_ENABLE
	default n
	help
	  It enables `pr_error` messages on serial console. When
	  disabled, those print messages are not compiled.
	  (based on `pp_printf`)

config MOCKTURTLE_LIBRARY_PRINT_MESSAGE_ENABLE
	bool "Enable print message in Mock Turtle library"
	depends on MOCKTURTLE_LIBRARY_PRINT_ENABLE
	default n
	help
	  It enables `pr_message` messages on serial console. When
	  disabled, those print messages are not compiled.
	  (based on `pp_printf`)
