/**
 * @defgroup framework Mock Turtle Firmware Framework
 * @{
 * @copyright: SPDX-FileCopyrightText: 2015-2019 CERN (home.cern)
 * @author Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#ifndef __TRTL_FW_FRM_H__
#define __TRTL_FW_FRM_H__

#include <inttypes.h>
#include <generated/autoconf.h>
#include "mockturtle-rt.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Action prototype type
 * @typedef trtl_fw_action_t
 * @param[in] msg_i input message
 * @param[out] msg_o output message
 * @return 0 on success. -1 on error
 *
 * The header for the output message is prepared by the framework.
 * The header's fields that the user should touch are
 *
 * On error the message will be sent anyway to the host. This
 * is just in case of future development.
 */
typedef int (trtl_fw_action_t)(struct trtl_fw_msg *msg_i, struct trtl_fw_msg *msg_o);

/**
 * Application phase
 * @typedef trtl_fw_phase_t
 * @return 0 on success. -1 on error
 */
typedef int (trtl_fw_phase_t)(void);


/**
 * Variable flag. Register
 */
#define TRTL_FW_VARIABLE_FLAG_FLD (1 << 0)

/**
 * Description of a variable that we want to export to the external
 * world (host or network).
 */
struct trtl_fw_variable {
	void *addr; /**< variable address */
	uint32_t mask; /**< variable mask without offset applied */
	uint8_t offset; /**< variable offset within the word */
	uint32_t flags; /**< variable options */
};


/**
 * Description of a buffer that we want to export to the external world
 * (host or network)
 */
struct trtl_fw_buffer {
	void *buf; /**< structure location */
	uint32_t len; /**< data structure lenght */
	/* Maybe other option later in time */
};


/**
 * Firmware Application Descriptor.
 * Provide a set of useful information used by the framework to
 * provide services to users.
 */
struct trtl_fw_application {
	const char name[TRTL_NAME_LEN]; /**< Firmware name*/
	const uint32_t *fpga_id_compat; /**< list of compatible FPGA
					   application ID */
	const unsigned int fpga_id_compat_n; /**< number of entry in
						the fpga_id_compat list */
	const struct trtl_fw_version version; /**< version running */

	struct trtl_fw_buffer *buffers; /**< exported buffers */
	unsigned int n_buffers; /**< number or exported buffers */

	struct trtl_fw_variable *variables; /**< exported variables */
	unsigned int n_variables; /**< number or exported variables */

	trtl_fw_action_t **actions; /**< list of custum actions */
	unsigned int n_actions; /**< number of custum actions */

	const struct trtl_config_rom *cfgrom; /**< configuration ROM */

	/* Operations */
	trtl_fw_phase_t *init; /**< function where you prepare
				  the application */
	trtl_fw_phase_t *main; /**< function where you execute your
				  application */
	trtl_fw_phase_t *exit; /**< function where you clean-up firmware
				  status */
};


extern struct trtl_fw_application app;

/**
 * Return the application descriptor
 * @return application descriptor
 */
static inline struct trtl_fw_application *trtl_fw_application_get(void)
{
	return &app;
}


extern void trtl_fw_time(uint32_t *seconds, uint32_t *cycles);
extern int trtl_fw_mq_send_uint32(enum trtl_mq_type type,
				  unsigned int idx_mq,
				  uint8_t msg_id,
				  unsigned int n,
				  ...);
extern int trtl_fw_mq_send_buf(enum trtl_mq_type type,
			       unsigned int idx_mq,
			       uint8_t msg_id,
			       unsigned int n,
			       void *data);
extern int trtl_fw_mq_action_dispatch(enum trtl_mq_type type,
				      unsigned int idx_mq);
extern void trtl_fw_message_error(struct trtl_fw_msg *msg, int err);

#if (HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE == 1) && (HAS_MOCKTURTLE_FRAMEWORK_DEBUG_ENABLE == 1)
extern int pr_frm_debug(const char *fmt, ...)
        __attribute__((format(printf,1,2)));
#else
static inline int pr_frm_debug(const char *fmt, ...)
{
	return 0;
}
#endif

#ifdef __cplusplus
}
#endif

#endif
/**@}*/
