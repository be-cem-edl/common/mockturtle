/**
 * SPDX-FileCopyrightText: 2013-2019 CERN (home.cern)
 * @defgroup library Mock Turtle Firmware Library
 * @{
 * @author Federico Vaga <federico.vaga@cern.ch>
 * @author Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#ifndef __MOCKTURTLE_RT_H
#define __MOCKTURTLE_RT_H

#include <errno.h>
#include <stdint.h>
#include <stdarg.h>
#include <generated/autoconf.h>

#include <mockturtle/hw/mockturtle_addresses.h>
#include <mockturtle/hw/mockturtle_queue.h>
#include <mockturtle/hw/mockturtle_endpoint.h>
#include <mockturtle/hw/mockturtle_cpu_lr.h>

#include <urv/riscv.h>

#include <mockturtle/mockturtle.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Messages descriptor for firmware. We map directly the HDL buffer
 */
struct trtl_fw_msg {
	struct trtl_hmq_header *header; /**< points to HMQ header buffer */
	void *payload; /**< points to HMQ payload buffer */
};


/**
 * Print buffer size. This means that your printf string
 * should not exceed this value. If the string exceed this value
 * then it will be trouncated.
 */
#define CONFIG_PRINT_BUFSIZE 128


/**
 * Print a string on the serial interface.
 * Internally, it uses the puts() function.
 * @param[in] fmt string format
 * @param[in] ... argument according to the string format
 * @return number of printed characters
 */
#if HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE == 1
extern int pp_printf(const char *fmt, ...)
        __attribute__((format(printf,1,2)));
#else
static inline int pp_printf(const char *fmt, ...)
{
        return 0;
}
#endif


/**
 * Create a new string according to the given format
 * @param[out] s output string
 * @param[in] fmt string format
 * @param[in] ... argument according to the string format
 * @return number of printed characters
 */
#if HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE == 1
extern int pp_sprintf(char *s, const char *fmt, ...)
        __attribute__((format(printf,2,3)));
#else
static inline int pp_sprintf(char *s, const char *fmt, ...)
{
        return 0;
}
#endif


/**
 * Print a string on the serial interface.
 * Internally, it uses the puts() function.
 * @param[in] fmt string format
 * @param[in] args list of arguments according to the string format
 * @return number of printed characters
 */
#if HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE
extern int pp_vprintf(const char *fmt, va_list args);
#else
static inline int pp_vprintf(const char *fmt, va_list args)
{
        return 0;
}
#endif


/**
 * Create a new string according to the given format
 * @param[out] buf output string
 * @param[in] fmt string format
 * @param[in] args list of arguments according to the string format
 * @return number of printed characters
 */
#if HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE == 1
extern int pp_vsprintf(char *buf, const char *, va_list args)
        __attribute__ ((format (printf, 2, 0)));
#else
static inline int pp_vsprintf(char *buf, const char *fmt, va_list args)
{
        return 0;
}
#endif


/**
 * Print a string on the serial interface only when the support
 * for error messages is enable.
 *
 * Kconfig ->CONFIG_MOCKTURTLE_LIBRARY_PRINT_DEBUG_ENABLE
 *
 * Internally, it uses the puts() function.
 * @param[in] fmt string format
 * @param[in] ... argument according to the string format
 * @return number of printed characters
 */
#if (HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE == 1) && (HAS_MOCKTURTLE_LIBRARY_PRINT_DEBUG_ENABLE == 1)
extern int pr_debug(const char *fmt, ...)
        __attribute__((format(printf,1,2)));
#else
static inline int pr_debug(const char *fmt, ...)
{
        return 0;
}
#endif

/**
 * Print a string on the serial interface only when the support
 * for error messages is enable.
 *
 * Kconfig -> CONFIG_MOCKTURTLE_LIBRARY_PRINT_ERROR_ENABLE
 *
 * Internally, it uses the puts() function.
 * @param[in] fmt string format
 * @param[in] ... argument according to the string format
 * @return number of printed characters
 */
#if (HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE == 1) && (HAS_MOCKTURTLE_LIBRARY_PRINT_ERROR_ENABLE == 1)
extern int pr_error(const char *fmt, ...)
        __attribute__((format(printf,1,2)));
#else
static inline int pr_error(const char *fmt, ...)
{
        return 0;
}
#endif

extern int putchar (int c);
extern int puts(const char *p);


/**
 * Read a 32bit word value from the given address
 * @param[in] addr source address
 * @return the value fromt the register
 */
static inline uint32_t readl(void *addr)
{
	return *(volatile uint32_t *) (addr);
}


/**
 * Write a 32bit word value to the given address
 * @param[in] value value to write
 * @param[in] addr destination address
 */
static inline void writel(uint32_t value, void *addr)
{
	*(volatile uint32_t *) (addr) = value;
}

/**
 * Read a 32bit word value from the Dedicated Peripheral address space
 * @param[in] reg register offset within the Dedicated Peripheral
 * @return the value fromt the register
 */
static inline uint32_t dp_readl(uint32_t reg)
{
	return readl(TRTL_ADDR_DP(reg));
}


/**
 * Write a 32bit word value to the Dedicated Peripheral address space
 * @param[in] value value to write
 * @param[in] reg register offset within the Dedicated Peripheral
 */
static inline void dp_writel(uint32_t value, uint32_t reg)
{
	writel(value, TRTL_ADDR_DP(reg));
}


/**
 * Read 32bit word value from the CPU Local Registers address space
 * @param[in] reg register offset within the Local Registers
 * @return the value fromt the register
 */
static inline uint32_t lr_readl(uint32_t reg)
{
	return readl(TRTL_ADDR_LR(reg));
}


/**
 * Write 32bit word value to the CPU Local Registers address space
 * @param[in] value value to write
 * @param[in] reg register offset within the Local Registers
 */
static inline void lr_writel(uint32_t value, uint32_t reg)
{
	writel(value, TRTL_ADDR_LR(reg));
}


/**
 * Set a bit in the CPU GPIO Register
 * @param[in] pin GPIO pin to set
 */
static inline void gpio_set(int pin)
{
	lr_writel ((1 << pin), MT_CPU_LR_REG_GPIO_SET);
}


/**
 * Clear a bit in the CPU GPIO Register
 * @param[in] pin GPIO pin to clear
 */
static inline void gpio_clear(int pin)
{
	lr_writel ((1 << pin), MT_CPU_LR_REG_GPIO_CLEAR);
}


/**
 * Get the GPIO status
 * @param[in] pin GPIO pin to query
 * @return the GPIO status
 */
static inline unsigned int gpio_status(int pin)
{
	return !!(lr_readl(MT_CPU_LR_REG_GPIO_IN) & (1 << pin));
}

/**
 * Wait n CPU cycles. This means that the absolute wait time can be different
 * on different cores with different clock frequencies.
 * @todo: use Timing Unit, compute it accoring to CPU frequency
 * @param[in] n number of cycles to wait
 */
static inline void delay(int n)
{
	lr_writel(n, MT_CPU_LR_REG_DELAY_CNT);
	while(lr_readl(MT_CPU_LR_REG_DELAY_CNT))
		;
}


/**
 * Return a pointer to the config ROM
 * @return pointer to the configuration ROM
 */
static inline const struct trtl_config_rom *trtl_config_rom_get(void)
{
  return (const struct trtl_config_rom *)(TRTL_ADDR_CONFIG_ROM_BASE);
}


/**
 * Wait for n milli-seconds (more or less). Be carefull to not overflow
 * the 32bits with big delays (in principle you should never use big delays)
 * @param[in] n number of milli-seconds to wait
 */
static inline void mdelay(unsigned int n)
{
	const struct trtl_config_rom *cfg = trtl_config_rom_get();

	delay(cfg->clock_freq / 1000 * n);
}


/**
 * Wait for n micro-seconds (more or less). Be carefull to not overflow
 * the 32bits with big delays (in principle you should never use big delays)
 * @param[in] n number of micro-seconds to wait
 */
static inline void udelay(unsigned int n)
{
	const struct trtl_config_rom *cfg = trtl_config_rom_get();

	delay(cfg->clock_freq / 1000000 * n);
}


/**
 * Generate a notification signal (IRQ) to ask the HOST CPU
 * to take some action.
 * @param[in] id CPU notification identifier
 */
static inline void trtl_notify(uint8_t id)
{
	lr_writel(id, MT_CPU_LR_REG_NTF_INT);
}


/**
 * Generate a notification signal (IRQ) to ask the HOST CPU
 * to take some action.
 * @param[in] id CPU notification identifier
 */
static inline void trtl_notify_user(uint8_t id)
{
	if (id >= __TRTL_CPU_NOTIFY_APPLICATION_MAX) {
		pr_error("%s invalid user id %d. Replaced with %d\r\n",
			 __func__, id, TRTL_CPU_NOTIFY_APPLICATION);
		id = TRTL_CPU_NOTIFY_APPLICATION;
	}
	trtl_notify(id);
}


/**
 * Return the core ID on which the firmware is running
 * @return the core ID
 */
static inline uint32_t trtl_get_core_id(void)
{
	uint32_t core_id = lr_readl(MT_CPU_LR_REG_STAT);

	core_id  &= MT_CPU_LR_STAT_CORE_ID_MASK;
	core_id >>= MT_CPU_LR_STAT_CORE_ID_SHIFT;

	return core_id;
}

/**
 * Message queue slot base address input
 */
#define TRTL_MQ_SLOT_IN(slot)  (TRTL_MQ_BASE_IN + ((slot) << 16))

/**
 * Message queue slot base address output
 */
#define TRTL_MQ_SLOT_OUT(slot) (TRTL_MQ_BASE_OUT + ((slot) << 16))

/**
 * List of Message Queue types
 */
enum trtl_mq_type {
	TRTL_HMQ = 0, /**< Host Message Queue - Host-Firmware */
	TRTL_RMQ, /**< Remote Message Queue - Network-Firmware */
	__TRTL_MAX_MQ_TYPE,
};


/**
 * Get the Message Queue base address
 * @param[in] type MQ type
 * @return message queue base address
 */
static inline void *trtl_mq_base_address(enum trtl_mq_type type)
{
	return (void *)(type == TRTL_HMQ ? TRTL_ADDR_HMQ_BASE : TRTL_ADDR_RMQ_BASE);
}


/**
 * Write on a Message Queue register
 * @param[in] type MQ type to use
 * @param[in] val value to write
 * @param[in] reg register offset
 */
static inline void mq_writel(enum trtl_mq_type type, uint32_t val, uint32_t reg)
{
	*(volatile uint32_t *)((char *) trtl_mq_base_address(type) + reg) = val;
}


static inline uint32_t mq_readl(enum trtl_mq_type type, uint32_t reg)
{
	return *(volatile uint32_t *)((char *) trtl_mq_base_address(type) + reg);
}


/**
 * Get the output slot data field pointer
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 * @return pointer to the input buffer
 *
 * Note: uninitialized memory
 */
static inline void *mq_map_out_buffer(enum trtl_mq_type type, int slot)
{
	return (void *) ((char *) trtl_mq_base_address(type) +
			 TRTL_MQ_SLOT_OUT (slot) + TRTL_MQ_SLOT_DATA_START);
}


/**
 * Get the input slot data field pointer
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 * @return pointer to the input buffer
 */
static inline void *mq_map_in_buffer(enum trtl_mq_type type, int slot)
{
	return (void *) ((char *) trtl_mq_base_address(type) +
			 TRTL_MQ_SLOT_IN (slot) + TRTL_MQ_SLOT_DATA_START);
}


/**
 * Get the output slot header field pointer
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 * @return pointer to the output header
 *
 * Note: uninitialized memory
 */
static inline void *mq_map_out_header(enum trtl_mq_type type, int slot)
{
	return (void *) ((char *) trtl_mq_base_address(type) +
			 TRTL_MQ_SLOT_OUT (slot) + TRTL_MQ_SLOT_HEADER_START);
}


/**
 * Get the input slot header field pointer
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 * @return pointer to the input header
 */
static inline void *mq_map_in_header(enum trtl_mq_type type, int slot)
{
	return (void *) ((char *) trtl_mq_base_address(type) +
			 TRTL_MQ_SLOT_IN (slot) + TRTL_MQ_SLOT_HEADER_START);
}


/**
 * @copydoc TRTL_MQ_CMD_CLAIM
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 * @return 0 on success, -EBUSY if queue busy/full
 */
static inline int mq_claim(enum trtl_mq_type type, int slot)
{
	uint32_t status = mq_readl(type, TRTL_MQ_SLOT_OUT(slot) + TRTL_MQ_SLOT_STATUS);

	if( status & TRTL_MQ_SLOT_STATUS_FULL )
		return -EBUSY;

	mq_writel(type, TRTL_MQ_CMD_CLAIM,
		  TRTL_MQ_SLOT_OUT(slot) + TRTL_MQ_SLOT_COMMAND);
	return 0;
}

/**
 * @copydoc TRTL_MQ_CMD_PURGE
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 */
static inline void mq_purge(enum trtl_mq_type type, int slot)
{
	mq_writel(type, TRTL_MQ_CMD_PURGE, TRTL_MQ_SLOT_OUT(slot) + TRTL_MQ_SLOT_COMMAND);
	mq_writel(type, TRTL_MQ_CMD_PURGE, TRTL_MQ_SLOT_IN(slot) + TRTL_MQ_SLOT_COMMAND);

}


/**
 * @copydoc TRTL_MQ_CMD_READY
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 */
static inline void mq_send(enum trtl_mq_type type, int slot)
{
	mq_writel(type, TRTL_MQ_CMD_READY, TRTL_MQ_SLOT_OUT(slot) + TRTL_MQ_SLOT_COMMAND);
}

/**
 * Type of ethernet address.
 */
struct trtl_ep_eth_address {
  uint32_t filter;
  uint8_t dst_mac[6];
  uint16_t vlan_id;
  uint16_t ethertype;
  uint32_t dst_ip;
  uint16_t dst_port;
  uint32_t src_ip;
  uint16_t src_port;
};


/**
 * Binds an RMQ slot (input) to a particular IP configuration. Used when
 * the Ethernet/UDP endpoint is enabled.
 * @param[in] slot slot number
 * @param[in] type EP type to use
 * @param[in] addr bind address
 */
extern int rmq_bind_in(unsigned int slot, enum trtl_ep_type type, const void *addr);

/**
 * Binds an RMQ slot (output) to a particular IP configuration. Used when
 * the Ethernet/UDP endpoint is enabled.
 * @param[in] slot slot number
 * @param[in] type EP type to use
 * @param[in] addr bind address
 */
extern int rmq_bind_out(unsigned int slot, enum trtl_ep_type type, const void *addr);


/**
 * @copydoc TRTL_MQ_CMD_DISCARD
 * @param[in] type MQ type to use
 * @param[in] slot slot number
 */
static inline void mq_discard(enum trtl_mq_type type, int slot)
{
	mq_writel(type, TRTL_MQ_CMD_DISCARD, TRTL_MQ_SLOT_IN(slot));
}


/**
 * Map a given MQ for outcoming messages
 * @param[in] type MQ type to use
 * @param[in] idx_mq MQ index
 * @param[out] msg where to map the message
 */
static inline void mq_map_out_message(enum trtl_mq_type type,
				     unsigned idx_mq,
				     struct trtl_fw_msg *msg)
{
	msg->header = (struct trtl_hmq_header*) mq_map_out_header(type, idx_mq);
	msg->payload = mq_map_out_buffer(type, idx_mq);
}


/**
 * Map a given MQ for incoming messages
 * @param[in] type MQ type to use
 * @param[in] idx_mq MQ index
 * @param[out] msg where to map the message
 */
static inline void mq_map_in_message(enum trtl_mq_type type,
				     unsigned idx_mq,
				     struct trtl_fw_msg *msg)
{
	msg->header = (struct trtl_hmq_header*) mq_map_in_header(type, idx_mq);
	msg->payload = mq_map_in_buffer(type, idx_mq);
}

/**
 * Get the current MQ input status
 * @param[in] type MQ type to use
 * @param[in] mask bitmask to set the bit of interest that corresponds
 *            to a MQ number
 * @return message queues input status bitmask
 */
static inline uint32_t mq_poll_in(enum trtl_mq_type type, uint32_t mask)
{
	uint32_t poll = lr_readl(MT_CPU_LR_REG_HMQ_STAT + (type * 4));

	/* HMQ and RMQ have the same format -> use the same mask */
	poll  &= MT_CPU_LR_HMQ_STAT_IN_MASK;
	poll >>= MT_CPU_LR_HMQ_STAT_IN_SHIFT;

	poll &= mask;
	return poll;
}

/**
 * Get the current MQ output status
 * @param[in] type MQ type to use
 * @param[in] mask bitmask to set the bit of interest that corresponds
 *            to a MQ number
 * @return message queues output status bitmask
 */
static inline uint32_t mq_poll_out(enum trtl_mq_type type, uint32_t mask)
{
	uint32_t poll = lr_readl(MT_CPU_LR_REG_HMQ_STAT + (type * 4));

	/* HMQ and RMQ have the same format -> use the same mask */
	poll  &= MT_CPU_LR_HMQ_STAT_OUT_MASK;
	poll >>= MT_CPU_LR_HMQ_STAT_OUT_SHIFT;

	poll &= mask;
	return poll;
}


/**
 * Get the current MQ input status
 * @param[in] type MQ type to use
 * @param[in] mask bitmask to set the bit of interest that corresponds
 *            to a MQ number
 * @param[in] us_timeout timeout in micro-seconds
 * @return message queues input status bitmask, 0 on timeout
 */
static inline uint32_t mq_poll_in_wait(enum trtl_mq_type type, uint32_t mask,
				       uint32_t us_timeout)
{
	const struct trtl_config_rom *cfg = trtl_config_rom_get();
	uint32_t p;

	us_timeout *= cfg->clock_freq / 1000000;
	lr_writel(us_timeout, MT_CPU_LR_REG_DELAY_CNT);

	do {
		p = mq_poll_in(type, mask);
	} while(!p && (us_timeout == 0 || lr_readl(MT_CPU_LR_REG_DELAY_CNT)));

	return p;
}


/**
 * Get the current MQ input status
 * @param[in] type MQ type to use
 * @param[in] mask bitmask to set the bit of interest that corresponds
 *            to a MQ number
 * @param[in] us_timeout timeout in micro-seconds
 * @return message queues input status bitmask, 0 on timeout
 */
static inline uint32_t mq_poll_out_wait(enum trtl_mq_type type, uint32_t mask,
					uint32_t us_timeout)
{
	const struct trtl_config_rom *cfg = trtl_config_rom_get();
	uint32_t p;

	us_timeout *= cfg->clock_freq / 1000000;
	lr_writel(us_timeout, MT_CPU_LR_REG_DELAY_CNT);

	do {
		p = mq_poll_out(type, mask);
	} while(!p && (us_timeout == 0 || lr_readl(MT_CPU_LR_REG_DELAY_CNT)));

	return p;
}

static inline uint32_t trtl_get_runtime_milliseconds()
{
    return riscv_rdtime();
}

static inline uint32_t trtl_get_runtime_cycles()
{
    return riscv_rdcycle();
}


/**
 * Shared Memory Size
 */
#define TRTL_SMEM_SIZE 0x10000

/**
 * Shared Memory address for the
 * @copydoc TRTL_SMEM_TYPE_BASE
 * memory access
 */
#define TRTL_SMEM_RANGE_BASE 0x00000

/**
 * Generate the SHM address range for a given SHM type
 */
#define TRTL_SMEM_TYPE_TO_RANGE(_type) \
	(TRTL_SMEM_RANGE_BASE + (_type) * TRTL_SMEM_SIZE)

/**
 * Shared Memory base address for the
 * @copydoc TRTL_SMEM_TYPE_ADD
 * memory access
 */
#define TRTL_SMEM_RANGE_ADD \
	TRTL_SMEM_TYPE_TO_RANGE(TRTL_SMEM_TYPE_ADD)

/**
 * Shared Memory base address for the
 * @copydoc TRTL_SMEM_TYPE_SUB
 * memory access
 */
#define TRTL_SMEM_RANGE_SUB \
	TRTL_SMEM_TYPE_TO_RANGE(TRTL_SMEM_TYPE_SUB)

/**
 * Shared Memory address for the
 * @copydoc TRTL_SMEM_TYPE_SET
 * memory access
 */
#define TRTL_SMEM_RANGE_SET \
	TRTL_SMEM_TYPE_TO_RANGE(TRTL_SMEM_TYPE_SET)

/**
 * Shared Memory address for the
 * @copydoc TRTL_SMEM_TYPE_CLR
 * memory access
 */
#define TRTL_SMEM_RANGE_CLEAR \
	TRTL_SMEM_TYPE_TO_RANGE(TRTL_SMEM_TYPE_CLR)

/**
 * Shared Memory address for the
 * @copydoc TRTL_SMEM_TYPE_FLP
 * memory access
 */
#define TRTL_SMEM_RANGE_FLIP \
	TRTL_SMEM_TYPE_TO_RANGE(TRTL_SMEM_TYPE_FLP)

/**
 * Shared Memory address for the
 * @copydoc TRTL_SMEM_TYPE_TST_SET
 * memory access
 */
#define TRTL_SMEM_RANGE_TEST_AND_SET \
	TRTL_SMEM_TYPE_TO_RANGE(TRTL_SMEM_TYPE_TST_SET)

/**
 * Compiler attribute to allocate variable on the Share Memory.
 * Add this to the variable declaration to put it on the Shared Memory.
 *
 *     SMEM int x;
 */
#define SMEM volatile __attribute__((section(".smem")))


/**
 * Perform an operations on the shared memory. What the function
 * performs can be summerized as:
 *
 *     (*p) = (*p) <operation-type> x
 *
 * Use this function only when the operation type that you want to use
 * is not supported yet by the library directly. Otherwise,
 * use the other functions.
 *
 * @param[in] p address on the shared memory of the first operator and store location
 * @param[in] x second operation argument
 * @param[in] type operation type
 */
static inline void __smem_atomic_op(volatile int *p, int x,
				    enum trtl_smem_modifier type)
{
#pragma GCC diagnostic push
/* The way SMEM atomic operations are implemented, it looks like we are trying
   to access way beyond the allocated size of p (TRTL_SMEM_TYPE_TO_RANGE here will
   add some multiple of TRTL_SMEM_SIZE). GCC -Wall will catch this but in this particular
   case it's a false alarm, the Mock Turtle HDL will use the extra address bits to determine
   the operation to perform on 'p' */
#pragma GCC diagnostic ignored "-Warray-bounds"
	*(volatile int *)(p + (TRTL_SMEM_TYPE_TO_RANGE(type) >> 2)) = x;
#pragma GCC diagnostic pop
}


/**
 * Perform an
 * @copydoc TRTL_SMEM_TYPE_ADD
 *
 *     (*p) = (*p) + x
 *
 * @param[in] p address on the shared memory of the first operator and store location
 * @param[in] x second operation argument
 */
static inline void smem_atomic_add(volatile int *p, int x)
{
	__smem_atomic_op(p, x, TRTL_SMEM_TYPE_ADD);
}


/**
 * Perform an
 * @copydoc TRTL_SMEM_TYPE_SUB
 *
 *     (*p) = (*p) - x
 *
 * @param[in] p address on the shared memory of the first operator and store location
 * @param[in] x second operation argument
 */
static inline void smem_atomic_sub(volatile int *p, int x)
{
	__smem_atomic_op(p, x, TRTL_SMEM_TYPE_SUB);
}


/**
 * Perform an
 * @copydoc TRTL_SMEM_TYPE_SET
 *
 *     (*p) = (*p) | x
 *
 * @param[in] p address on the shared memory of the first operator and store location
 * @param[in] x second operation argument
 */
static inline void smem_atomic_or(volatile int *p, int x)
{
	__smem_atomic_op(p, x, TRTL_SMEM_TYPE_SET);
}


/**
 * Perform an
 * @copydoc TRTL_SMEM_TYPE_CLR
 *
 *     (*p) = (*p) & (~x)
 *
 * @param[in] p address on the shared memory of the first operator and store location
 * @param[in] x second operation argument
 */
static inline void smem_atomic_and_not(volatile int *p, int x)
{
	__smem_atomic_op(p, x, TRTL_SMEM_TYPE_CLR);
}


/**
 * Perform an
 * @copydoc TRTL_SMEM_TYPE_FLP
 *
 *     (*p) = (*p) ^ x
 *
 * @param[in] p address on the shared memory of the first operator and store location
 * @param[in] x second operation argument
 */
static inline void smem_atomic_xor(int *p, int x)
{
	__smem_atomic_op(p, x, TRTL_SMEM_TYPE_FLP);
}


/**
 * Perform an:
 * @copydoc TRTL_SMEM_TYPE_TST_SET
 *
 *     val = (*p);
 *     if (val == 0) {
 *        (*p) = 1;
 *     }
 *     return val;
 *
 * This is useful to implement mutex
 *
 * @param[in] p address on the shared memory
 * @return the value before the set
 */
static inline int smem_atomic_test_and_set(volatile int *p)
{
	/* shift right translates range in bytes into range in words */
	return *(volatile int *)(p + (TRTL_SMEM_TYPE_TO_RANGE(TRTL_SMEM_TYPE_TST_SET) >> 2));
}

extern void pr_message(struct trtl_fw_msg *msg);

#ifdef __cplusplus
}
#endif

#endif
/**@}*/
