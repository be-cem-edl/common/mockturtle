/*
 * SPDX-FileCopyrightText: 2013-2019 CERN (home.cern)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "mockturtle-rt.h"


/**
 * Send a character to the UART interface
 * @param[in] c the character to sent
 * @return 0 on success (for the time being it does not fail)
 */
int putchar(int c)
{
	lr_writel(c, MT_CPU_LR_REG_UART_CHR);

	return 0;
}


/**
 * Send a string over the serial interface
 * @param[in] p string to send
 * @return number of sent characters
 *
 * The function does not add any special character.
 * If you neeed the new-line or the carriage-return
 * you have to add them to your strings.
 */
int puts(const char *p)
{
	const char *p_orig = p;
	char c;

	while ((c = *(p++)))
		putchar(c);

	return p - p_orig;
}


/**
 * Print on the serial console the given message
 * @param[in] msg a mock turtle message
 */
void pr_message(struct trtl_fw_msg *msg)
{
	struct trtl_hmq_header *h = msg->header;
	uint32_t *d = msg->payload;
	int i;

	if (!HAS_MOCKTURTLE_LIBRARY_PRINT_MESSAGE_ENABLE)
		return;

	pp_printf("Message\n\r");
	pp_printf("\tapp_id 0x%04"PRIx16" | flags   0x%02"PRIx8" | msg_id 0x%02"PRIx8"\r\n",
		 h->rt_app_id, h->flags, h->msg_id);
	pp_printf("\tlen 0x%04"PRIx16" | sync_id 0x%04"PRIx16"\r\n",
		 h->len, h->sync_id);
	pp_printf("\tseq 0x%08"PRIx32"\r\n",
		 h->seq);

	for (i = 0; i < h->len; i++)
		pp_printf("%s: data[%d] = 0x%"PRIx32"\n\r", __func__, i , d[i]);
}


static inline uint32_t ep_eth_build_mac_hi(const struct trtl_ep_eth_address* addr)
{
	return ((uint32_t)addr->dst_mac[0] << 8 ) |
		((uint32_t)addr->dst_mac[1] << 0);
}

static inline uint32_t ep_eth_build_mac_lo(const struct trtl_ep_eth_address* addr)
{
	return ((uint32_t)addr->dst_mac[2] << 24) |
		((uint32_t)addr->dst_mac[3] << 16) |
		((uint32_t)addr->dst_mac[4] << 8) |
		((uint32_t)addr->dst_mac[5] << 0);
}

static inline void rmq_ep_out_writel(int slot, uint32_t data, uint32_t addr)
{
	addr += TRTL_MQ_SLOT_ENDPOINT_CONFIG_START + TRTL_MQ_SLOT_OUT(slot);
	mq_writel(TRTL_RMQ, data, addr);
}

static inline void rmq_ep_in_writel(int slot, uint32_t data, uint32_t addr)
{
	addr += TRTL_MQ_SLOT_ENDPOINT_CONFIG_START + TRTL_MQ_SLOT_IN(slot);
	mq_writel(TRTL_RMQ, data, addr);
}

static int rmq_ep_eth_bind_in(unsigned int slot, const struct trtl_ep_eth_address *addr)
{
	uint32_t tmp;

	rmq_ep_in_writel(slot, addr->filter, TRTL_EP_IN_CONFIG);
	tmp = ep_eth_build_mac_hi(addr);
	rmq_ep_in_writel(slot, tmp, TRTL_EP_IN_DST_MAC_HI);
	tmp = ep_eth_build_mac_lo(addr);
	rmq_ep_in_writel(slot, tmp, TRTL_EP_IN_DST_MAC_LO);
	rmq_ep_in_writel(slot, addr->vlan_id, TRTL_EP_IN_VLAN_ID);
	rmq_ep_in_writel(slot, addr->ethertype, TRTL_EP_IN_ETHERTYPE);
	rmq_ep_in_writel(slot, addr->dst_ip, TRTL_EP_IN_DST_IP);
	rmq_ep_in_writel(slot, addr->dst_port, TRTL_EP_IN_DST_PORT);

	return 0;
}

static int rmq_ep_eth_bind_out(unsigned int slot, const struct trtl_ep_eth_address *addr)
{
	uint32_t tmp;

	rmq_ep_out_writel(slot, addr->filter, TRTL_EP_OUT_CONFIG);

	tmp = ep_eth_build_mac_hi(addr);
	rmq_ep_out_writel(slot, tmp, TRTL_EP_OUT_DST_MAC_HI);
	tmp = ep_eth_build_mac_lo(addr);
	rmq_ep_out_writel(slot, tmp, TRTL_EP_OUT_DST_MAC_LO);
	rmq_ep_out_writel(slot, addr->vlan_id, TRTL_EP_OUT_VLAN_ID);
	rmq_ep_out_writel(slot, addr->ethertype, TRTL_EP_OUT_ETHERTYPE);
	rmq_ep_out_writel(slot, addr->src_ip, TRTL_EP_OUT_SRC_IP);
	rmq_ep_out_writel(slot, addr->dst_ip, TRTL_EP_OUT_DST_IP);
	rmq_ep_out_writel(slot, addr->src_port, TRTL_EP_OUT_SRC_PORT);
	rmq_ep_out_writel(slot, addr->dst_port, TRTL_EP_OUT_DST_PORT);

	return 0;
}

int rmq_bind_in(unsigned int slot, enum trtl_ep_type type, const void *addr)
{
        if (type != TRTL_EP_ETH)
		return -EINVAL;

        return rmq_ep_eth_bind_in(slot, addr);
}

int rmq_bind_out(unsigned int slot, enum trtl_ep_type type, const void *addr)
{
        if (type != TRTL_EP_ETH)
		return -EINVAL;
        return rmq_ep_eth_bind_out(slot, addr);
}
