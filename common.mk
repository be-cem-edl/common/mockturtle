# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

PREFIX ?= /usr/local

GIT_VERSION := $(shell git describe --always --dirty --long --tags)
VERSION := $(shell git describe --tags --abbrev=0 | tr -d 'v')

TRTL_SW := $(TRTL)/software
