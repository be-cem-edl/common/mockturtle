# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

board  = "svec"
action = "synthesis"
target = "xilinx"

syn_device  = "xc6slx150t"
syn_grade   = "-3"
syn_package = "fgg900"
syn_top     = "svec_mt_demo"
syn_project = "svec_mt_demo.xise"
syn_tool    = "ise"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../ip_cores"

# Ideally this should be done by hdlmake itself, to allow downstream Manifests to be able to use the
# fetchto variable independent of where those Manifests reside in the filesystem.
import os
fetchto = os.path.abspath(fetchto)

files = [
    "buildinfo_pkg.vhd",
    "sourceid_{}_pkg.vhd".format(syn_top),
]

modules = {
    "local" : [
        "../../top/svec_mt_demo",
    ],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
        "https://ohwr.org/project/vme64x-core.git",
        "https://ohwr.org/project/ddr3-sp6-core.git",
        "https://ohwr.org/project/svec.git",
    ],
}

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass


try:
    exec(open(fetchto + "/general-cores/tools/gen_sourceid.py").read(),
         None, {'project': syn_top})
except Exception as e:
    print("Error: cannot generate source id file")
    raise

syn_post_project_cmd = "$(TCL_INTERPRETER) syn_extra_steps.tcl $(PROJECT_FILE)"

svec_base_ucf = ['led', 'gpio']

# needs to be defined, even if not used
ctrls = ["bank4_64b_32b"]
