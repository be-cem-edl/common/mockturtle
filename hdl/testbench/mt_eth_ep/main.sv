// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   main
//
// description: A SystemVerilog testbench to test the ethernet endpoint of MT

`include "mock_turtle_driver.svh"
`include "vhd_wishbone_master.svh"
`include "if_wb_master.svh"
`include "if_wb_slave.svh"
`include "wb_packet_sink.svh"
`include "wb_packet_source.svh"


import mock_turtle_pkg::*;
import wr_fabric_pkg::*;
import mt_mqueue_pkg::*;


`timescale 1ns/1ps

module main;

   reg rst_n = 0;
   reg clk_sys = 0;

   t_mt_rmq_endpoint_iface_out mt2ep;
   t_mt_rmq_endpoint_iface_in ep2mt;
   t_wrf_source_out eth_src_out;
   t_wrf_source_in eth_src_in;
   t_wrf_sink_out eth_snk_out;
   t_wrf_sink_in eth_snk_in;

   always #8ns clk_sys <= ~clk_sys;

   initial begin
      repeat(20) @(posedge clk_sys);
      rst_n = 1;
   end

   IWishboneMaster
     #(
       .g_data_width(16),
       .g_addr_width(2))
   U_wrf_source
     (
      .clk_i(clk_sys),
      .rst_n_i(rst_n)
      );

   assign eth_snk_in.adr = U_wrf_source.adr;
   assign eth_snk_in.dat = U_wrf_source.dat_o;
   assign eth_snk_in.cyc = U_wrf_source.cyc;
   assign eth_snk_in.stb = U_wrf_source.stb;
   assign eth_snk_in.we  = U_wrf_source.we;
   assign eth_snk_in.sel = U_wrf_source.sel;
   assign U_wrf_source.ack   = eth_snk_out.ack;
   assign U_wrf_source.stall = eth_snk_out.stall;
   assign U_wrf_source.err   = eth_snk_out.err;
   assign U_wrf_source.rty   = eth_snk_out.rty;

   IWishboneSlave
     #(
       .g_data_width(16),
       .g_addr_width(2))
   U_wrf_sink
     (
      .clk_i(clk_sys),
      .rst_n_i(rst_n)
      );

   assign U_wrf_sink.adr   = eth_src_out.adr;
   assign U_wrf_sink.dat_i = eth_src_out.dat;
   assign U_wrf_sink.cyc   = eth_src_out.cyc;
   assign U_wrf_sink.stb   = eth_src_out.stb;
   assign U_wrf_sink.we    = eth_src_out.we;
   assign U_wrf_sink.sel   = eth_src_out.sel;
   assign eth_src_in.ack   = U_wrf_sink.ack;
   assign eth_src_in.stall = U_wrf_sink.stall;
   assign eth_src_in.err   = U_wrf_sink.err;
   assign eth_src_in.rty   = U_wrf_sink.rty;

   mock_turtle_core #()
   DUT (
	.clk_i         (clk_sys),
	.rst_n_i       (rst_n),
	.host_slave_i  (Host.out),
	.host_slave_o  (Host.in),
	.sp_master_o   (),
	.sp_master_i   (),
	.dp_master_o   (),
	.dp_master_i   (),

	.rmq_endpoint_o(mt2ep),
	.rmq_endpoint_i(ep2mt),

	.clk_ref_i     (),
	.tm_i          (),
	.gpio_o        (),
	.gpio_i        (),
	.hmq_in_irq_o  (),
	.hmq_out_irq_o (),
	.notify_irq_o  (),
	.console_irq_o ()
	);

   mt_ep_ethernet_single
     EP (
	 .clk_i  (clk_sys),
	 .rst_n_i(rst_n),

         .rmq_src_i(mt2ep.snk_out[0][0]),
         .rmq_src_o(ep2mt.snk_in[0][0]),
         .rmq_src_config_i(mt2ep.snk_config_out[0][0]),
         .rmq_src_config_o(ep2mt.snk_config_in[0][0]),

         .rmq_snk_i(mt2ep.src_out[0][0]),
         .rmq_snk_o(ep2mt.src_in[0][0]),
         .rmq_snk_config_i(mt2ep.src_config_out[0][0]),
         .rmq_snk_config_o(ep2mt.src_config_in[0][0]),

	 .eth_src_o (eth_src_out),
	 .eth_src_i (eth_src_in),

	 .eth_snk_o (eth_snk_out),
	 .eth_snk_i (eth_snk_in)
	 );

   IVHDWishboneMaster Host (clk_sys, rst_n);

   IMockTurtleIRQ IrqMonitor (`MT_ATTACH_IRQ(DUT));

   string fw = "../../../tests/firmware/rmq-udp-send/fw-rmq-udp-send.bin";

   const uint64_t mt_base = 'h0000;

   MockTurtleDriver drv;

   CWishboneAccessor acc;

   initial begin

      automatic WBPacketSink sink = new(U_wrf_sink.get_accessor());
      automatic WBPacketSource src  = new(U_wrf_source.get_accessor());

      $timeformat (-6, 3, "us", 10);

      acc = Host.get_accessor();

      acc.set_mode (PIPELINED);

      #10us;

      drv = new (acc, mt_base, IrqMonitor);

      drv.init();

      drv.enable_console_irq (0, 1);

      drv.enable_hmqi_irq (0, 0, 1);

      drv.load_firmware (0, fw, 1'b0);

      drv.reset_core (0, 0);

      fork

	 forever begin
            t_notify_queue ntf;

            // Check notifications.
            drv.get_cpu_notifications (0, ntf);
            if (ntf.size) begin
	       $display ("Simulation PASSED");
	       $finish;
            end

	    # 1us;
	 end

         //  Send the packet that has been received, but negate payload.
	 forever begin
            EthPacket pkt;

            sink.recv(pkt);
            pkt.dump;
            for(int i = 24; i < pkt.payload.size(); i++)
              pkt.payload[i] = ~pkt.payload[i];
            src.send(pkt);
	 end

         begin
            #800us;
	    $fatal (1, "Simulation FAILED");
         end
      join
   end // initial begin

endmodule // main
