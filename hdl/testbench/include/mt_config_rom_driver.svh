// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   MTConfigRom
//
// description: A ROM to hold the various configuration values of the current
// MockTurtle implementation.

`ifndef __MOCK_TURTLE_CONFIG_ROM_INCLUDED
 `define __MOCK_TURTLE_CONFIG_ROM_INCLUDED

 `include "simdrv_defs.svh"

 `define TRTL_CONFIG_ROM_MQ_ENTRIES_SHIFT 16
 `define TRTL_CONFIG_ROM_MQ_ENTRIES_MASK  32'h00FF0000
 `define TRTL_CONFIG_ROM_MQ_PAYLOAD_SHIFT 8
 `define TRTL_CONFIG_ROM_MQ_PAYLOAD_MASK  32'h0000FF00
 `define TRTL_CONFIG_ROM_MQ_HEADER_SHIFT  0
 `define TRTL_CONFIG_ROM_MQ_HEADER_MASK   32'h000000FF

 `define TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(_size) (1 << ((_size & `TRTL_CONFIG_ROM_MQ_ENTRIES_MASK) >> \
						       `TRTL_CONFIG_ROM_MQ_ENTRIES_SHIFT))

 `define TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(_size) (1 << ((_size & `TRTL_CONFIG_ROM_MQ_PAYLOAD_MASK) >> \
						       `TRTL_CONFIG_ROM_MQ_PAYLOAD_SHIFT))

 `define TRTL_CONFIG_ROM_MQ_SIZE_HEADER(_size)  (1 << ((_size & `TRTL_CONFIG_ROM_MQ_HEADER_MASK) >> \
						       `TRTL_CONFIG_ROM_MQ_HEADER_SHIFT))

class MTConfigRom;
   protected CBusAccessor m_acc;
   protected uint64_t     m_base;
   protected uint32_t     m_size;
   protected uint32_t     m_data [];

   function new ( CBusAccessor acc, uint64_t base, uint64_t size = 512);
      m_acc  = acc;
      m_base = base;
      m_size = size;
      m_data = new[m_size];
   endfunction // new

   task _readl (  uint32_t r, ref uint32_t v );
      uint64_t tmp;
      m_acc.read ( m_base + r, tmp );
      v= tmp;
   endtask // _readl

   task init ( );
      int i;

      for ( i = 0; i < m_size; i++ )
	begin
	   _readl ( 4*i, m_data[i] );
	end
   endtask // init

   function uint32_t read ( uint32_t pos );
      return m_data[pos];
   endfunction // read

   function uint32_t getAppID();
      return read(5);
   endfunction // getAppID

   function uint32_t getCoreCount();
      return read(6);
   endfunction // getCoreCount

   function uint32_t getHmqSlotCount(int core);
      return read(16 + core);
   endfunction // getHmqSlotCount

   function uint32_t getHmqDimensions(int core, int hmq);
      return read(128 + 16 * core + 2 * hmq);
   endfunction // getHmqDimensions

   function uint32_t getHmqEndpoint(int core, int hmq);
      return read(128 + 16 * core + 2 * hmq + 1);
   endfunction // getHmqEndpoint

   function uint32_t getRmqSlotCount(int core);
      return read(24 + core);
   endfunction // getRmqSlotCount

   function uint32_t getRmqDimensions(int core, int rmq);
      return read(256 + 16 * core + 2 * rmq);
   endfunction // getRmqDimensions

   function uint32_t getRmqEndpoint(int core, int rmq);
      return read(256 + 16 * core + 2 * rmq + 1);
   endfunction // getRmqEndpoint

   task dump ();
      int i;

      for ( i = 0; i < m_size; i++ )
	begin
	   $display ("%4d: %8x", i, read(i));
	end
   endtask // dump

   task pretty_print ( );
      int i, j;
      int cpu_count = getCoreCount();

      $display ( "Configuration ROM contents:" );
      $display ( "  Magic Value     : %0s", read(0) );
      $display ( "  MT revision     : %0d", read(1) );
      $display ( "  Clock Frequency : %0d", read(3) );
      $display ( "  WR support      : %0s", read(4) & 1 ? "yes" : "no" );
      $display ( "  Application ID  : %0x", getAppID() );
      $display ( "  Shared mem size : %0d", read(7) );
      $display ( "  CPU count       : %0d", cpu_count );

      for ( i = 0; i < cpu_count; i++ )
	begin
	   int hmq_slots = getHmqSlotCount(i);
	   int rmq_slots = getRmqSlotCount(i);
	   $display ( "    CPU #%0d", i );
	   $display ( "      Local mem size : %0d", read(8+i) );
	   $display ( "      HMQ slots      : %0d", hmq_slots );
	   for ( j = 0; j < hmq_slots; j++ )
	     begin
		uint32_t val;
		$display ( "        HMQ #%0d", j );
		val = getHmqDimensions(i, j);
		$display ( "          Entries bits : %0d", `TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(val));
		$display ( "          Width bits   : %0d", `TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(val));
		$display ( "          Header bits  : %0d", `TRTL_CONFIG_ROM_MQ_SIZE_HEADER(val));
		val = getHmqEndpoint(i, j);
		$display ( "          Endpoint ID  : %0x", val );
	     end
	   $display ( "      RMQ slots      : %0d", rmq_slots );
	   for ( j = 0; j < rmq_slots; j++ )
	     begin
		uint32_t val;
		$display ( "        RMQ #%0d", j );
		val = getRmqDimensions(i, j);
		$display ( "          Entries bits : %0d", `TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(val));
		$display ( "          Width bits   : %0d", `TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(val));
		$display ( "          Header bits  : %0d", `TRTL_CONFIG_ROM_MQ_SIZE_HEADER(val));
		val = getRmqEndpoint(i, j);
		$display ( "          Endpoint ID  : %0x", val );
	     end
	end

   endtask // pretty_print

endclass // MockTurtleConfigRom

`endif // `ifndef __MOCK_TURTLE_CONFIG_ROM_DRIVER_INCLUDED
