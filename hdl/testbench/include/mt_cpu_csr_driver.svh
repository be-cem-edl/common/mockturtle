// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   MTCPUControl
//
// description: A SystemVerilog Class to provide an abstraction of the
// MockTurtle core's CSR, including functions such as CPU reset, firmware
// loading, debug UART monitoring, etc.

`ifndef __MT_CPU_CSR_INCLUDED
 `define __MT_CPU_CSR_INCLUDED

 `include "regs/mt_cpu_csr_regs.vh"

 `define SMEM_OP_DIRECT       0
 `define SMEM_OP_ADD          1
 `define SMEM_OP_SUB          2
 `define SMEM_OP_SET          3
 `define SMEM_OP_CLEAR        4
 `define SMEM_OP_FLIP         5
 `define SMEM_OP_TEST_AND_SET 6

 `define TRTL_CPU_NOTIFY_APPLICATION_MAX 64

enum {
      TRTL_CPU_NOTIFY_APPLICATION = `TRTL_CPU_NOTIFY_APPLICATION_MAX,
      TRTL_CPU_NOTIFY_INIT,
      TRTL_CPU_NOTIFY_MAIN,
      TRTL_CPU_NOTIFY_EXIT,
      TRTL_CPU_NOTIFY_ERR
} trtl_cpu_notification;

typedef uint32_t t_notify_queue[$];

class MTCPUControl;
   protected string name;
   protected CBusAccessor bus;
   protected uint32_t base;
   protected uint32_t core_count;
   protected string dbg_msg_queue[];

   t_notify_queue notify_queue[];

   function new ( CBusAccessor bus, input uint32_t base,
		  int core_count, string name = "" );
      this.name       = name;
      this.base       = base;
      this.bus        = bus;
      this.core_count = core_count;
   endfunction // new

   task mdisplay ( string str );
      string	    tmp;
      if (this.name == "")
	tmp = $sformatf("<%t> %s", $realtime, str);
      else
	tmp = $sformatf("[%s] <%t> %s", this.name, $realtime, str);
      $display (tmp);
   endtask // mdisplay

   task writel ( uint32_t r, uint32_t v );
      bus.write ( base + r, v );
   endtask // _write

   task readl (  uint32_t r, ref uint32_t v );
      uint64_t tmp;
      bus.read (base + r, tmp );
      v= tmp;
   endtask // readl

   task init();
      dbg_msg_queue   = new[core_count];
      notify_queue    = new[core_count];
      for (int i = 0; i < core_count; i++)
	   this.notify_queue[i]  = {};
   endtask // init

   task reset_core(int core, int reset);
      uint32_t rstr;

      readl(`ADDR_MT_CPU_CSR_RESET, rstr);

      if(reset)
	rstr |= (1<<core);
      else
	rstr &= ~(1<<core);
      writel(`ADDR_MT_CPU_CSR_RESET, rstr);
   endtask // enable_cpu

   task uart_int_enable(int core, int enable);
      uint32_t imsk;

      readl(`ADDR_MT_CPU_CSR_UART_IMSK, imsk);
      if(enable)
	imsk |= (1 << core);
      else
	imsk &= ~(1 << core);
      writel(`ADDR_MT_CPU_CSR_UART_IMSK, imsk);
   endtask // uart_int_enable

   task load_firmware(int core, string filename, bit check);
      integer f = $fopen(filename, "r");
      int     i, addr = 0;

      // Stop in case of error.
      if (!f)
	$fatal (1, "Missing firmware \"%s\"", filename);

      reset_core(core, 1);

      writel(`ADDR_MT_CPU_CSR_CORE_SEL, core);

      while(!$feof(f))
	begin
	   uint32_t tmp, data;

	   tmp = $fread(data, f);

	   writel(`ADDR_MT_CPU_CSR_UADDR, addr);
	   writel(`ADDR_MT_CPU_CSR_UDATA, data);
	   addr ++;
	end // while (!$feof(f))

      if (check)
	begin
	   void'($rewind(f));

	   for(i = 0; i < addr; i++)
	     begin
		uint32_t rv, data;
		writel(`ADDR_MT_CPU_CSR_UADDR, i);
		readl(`ADDR_MT_CPU_CSR_UDATA, rv);

		void'($fread(data, f));
		assert (rv == data) else
		  $fatal("verification error at %x, got %x, expected %x\n",
			 i, rv, data);

	     end
	end
      $fclose(f);
   endtask // load_firmware

   task check_consoles();
      int i,j;
      uint32_t rval, cpu_mask;

      readl(`ADDR_MT_CPU_CSR_UART_POLL , cpu_mask);

      if (cpu_mask == 0)
	return;

      for(i = 0; i < core_count; i++)
	begin
	   if(!(cpu_mask & (1 << i)))
	     continue;

	   writel(`ADDR_MT_CPU_CSR_CORE_SEL, i);
	   readl(`ADDR_MT_CPU_CSR_UART_MSG, rval);

	   if(rval == 0 || rval == 13 || rval == 10)
	     begin
		mdisplay ($sformatf("UART MSG from core %0d: %s", i, dbg_msg_queue[i]));
		dbg_msg_queue[i] = "";
	     end
	   else
	     dbg_msg_queue[i] = {dbg_msg_queue[i], rval[7:0]};
	end
   endtask

   task get_notification_irq (ref uint32_t val);
      readl(`ADDR_MT_CPU_CSR_INT, val);
   endtask // get_notification_irq

   task get_notification_value (int core, ref uint32_t val);
      if (core < 4)
	readl(`ADDR_MT_CPU_CSR_INT_VAL_LO, val);
      else
	readl(`ADDR_MT_CPU_CSR_INT_VAL_HI, val);
      val = val >> ((core % 4) * 8) & 'hff;
   endtask // get_notification_value

   task handle_notification_irq ();
      uint32_t irq_val, notify_val;
      int core = 0;
      get_notification_irq(irq_val);
      while (irq_val)
	begin
	   if (irq_val & 1'b1)
	     begin
		get_notification_value(core, notify_val);
		notify_queue[core].push_back(notify_val);
	     end
	   irq_val >>= 1;
	   core++;
	end
   endtask // handle_notification_irq

   task hmq_in_int_enable(int core, int queue, int enable);
      uint32_t imsk;

      if (core < 4)
	readl(`ADDR_MT_CPU_CSR_HMQI_INTEN_LO, imsk);
      else
	readl(`ADDR_MT_CPU_CSR_HMQI_INTEN_HI, imsk);
      if(enable)
	imsk |= (1 << (core * 8 + queue));
      else
	imsk &= ~(1 << (core * 8 + queue));
      if (core < 4)
	writel(`ADDR_MT_CPU_CSR_HMQI_INTEN_LO, imsk);
      else
	writel(`ADDR_MT_CPU_CSR_HMQI_INTEN_HI, imsk);
   endtask // hmq_in_int_enable

   task hmq_out_int_enable(int core, int queue, int enable);
      uint32_t imsk;

      if (core < 4)
	readl(`ADDR_MT_CPU_CSR_HMQO_INTEN_LO, imsk);
      else
	readl(`ADDR_MT_CPU_CSR_HMQO_INTEN_HI, imsk);
      if(enable)
	imsk |= (1 << (core * 8 + queue));
      else
	imsk &= ~(1 << (core * 8 + queue));
      if (core < 4)
	writel(`ADDR_MT_CPU_CSR_HMQO_INTEN_LO, imsk);
      else
	writel(`ADDR_MT_CPU_CSR_HMQO_INTEN_HI, imsk);
   endtask // hmq_out_int_enable

   task get_hmq_in_status (ref uint64_t val);
      uint32_t tmp;
      readl(`ADDR_MT_CPU_CSR_HMQI_STATUS_LO, tmp);
      val = tmp;
      readl(`ADDR_MT_CPU_CSR_HMQI_STATUS_HI, tmp);
      val |= tmp << 32;
   endtask // get_hmq_in_status

   task get_hmq_in_int_en (ref uint64_t val);
      uint32_t tmp;
      readl(`ADDR_MT_CPU_CSR_HMQI_INTEN_LO, tmp);
      val = tmp;
      readl(`ADDR_MT_CPU_CSR_HMQI_INTEN_HI, tmp);
      val |= tmp << 32;
   endtask // get_hmq_in_int_en

   task hmq_select(uint32_t core, uint32_t queue);
      uint32_t val = (core & 'b111) << 8 | (queue & 'b111);
      writel(`ADDR_MT_CPU_CSR_HMQ_SEL, val);
   endtask; // hmq_select

   task update();
      check_consoles();
   endtask // update

   task set_smem_op(int op);
      writel(`ADDR_MT_CPU_CSR_SMEM_OP, op);
   endtask // set_smem_op

endclass

`endif //  `ifndef __MT_CPU_CSR_INCLUDED
