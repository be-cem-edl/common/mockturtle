// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   main
//
// description: A SystemVerilog testbench for the supplied SPEC MT demo.

`timescale 1ns/1ps

`include "gn4124_bfm.svh"
`include "mock_turtle_driver.svh"

module main;

   IGN4124PCIMaster Host ();

   reg clk_125m = 0;

   always #4ns clk_125m <= ~clk_125m;

   // the Device Under Test
   spec_mt_demo
     DUT
     (
      .clk_125m_pllref_p_i (clk_125m),
      .clk_125m_pllref_n_i (~clk_125m),
      .led_act_o           (),
      .led_link_o          (),
      .button1_n_i         (1'b1),
      .aux_leds_o          (),
      .pcbrev_i            (4'b0),
      .fmc0_prsnt_m2c_n_i  (1'b1),
      .fmc0_scl_b          (),
      .fmc0_sda_b          (),
      .gn_rst_n_i          (Host.rst_n),
      .gn_p2l_clk_n_i      (Host.p2l_clk_n),
      .gn_p2l_clk_p_i      (Host.p2l_clk_p),
      .gn_p2l_rdy_o        (Host.p2l_rdy),
      .gn_p2l_dframe_i     (Host.p2l_dframe),
      .gn_p2l_valid_i      (Host.p2l_valid),
      .gn_p2l_data_i       (Host.p2l_data),
      .gn_p_wr_req_i       (Host.p_wr_req),
      .gn_p_wr_rdy_o       (Host.p_wr_rdy),
      .gn_rx_error_o       (Host.rx_error),
      .gn_l2p_clk_n_o      (Host.l2p_clk_n),
      .gn_l2p_clk_p_o      (Host.l2p_clk_p),
      .gn_l2p_dframe_o     (Host.l2p_dframe),
      .gn_l2p_valid_o      (Host.l2p_valid),
      .gn_l2p_edb_o        (Host.l2p_edb),
      .gn_l2p_data_o       (Host.l2p_data),
      .gn_l2p_rdy_i        (Host.l2p_rdy),
      .gn_l_wr_rdy_i       (Host.l_wr_rdy),
      .gn_p_rd_d_rdy_i     (Host.p_rd_d_rdy),
      .gn_tx_error_i       (Host.tx_error),
      .gn_vc_rdy_i         (Host.vc_rdy),
      .gn_gpio_b           ());

   IMockTurtleIRQ IrqMonitor (`MT_ATTACH_IRQ(DUT.U_Mock_Turtle));

   string fw = "../../../demos/hello_world/firmware/fw-01/fw-hello.bin";

   const uint64_t mt_base = 'h2_0000;

   MockTurtleDriver drv;

   CBusAccessor acc;

   initial begin

      $timeformat (-6, 3, "us", 10);

      wait (Host.ready == 1'b1);

      acc = Host.get_accessor();

      drv = new (acc, mt_base, IrqMonitor);

      // 1us is the default update delay value, this serves as an example
      // on how to change the update loop delay
      drv.init(.update_delay(1us));

      drv.enable_console_irq (0, 1);

      drv.load_firmware(0, fw, 1'b0);

      drv.reset_core(0, 0);

   end // initial begin

endmodule // main
