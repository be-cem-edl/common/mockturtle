# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

set defaultsimlibpath "/opt/sim_libs/ise/14.7"

if { [info exists ::env(SIM_LIB_PATH)] } {
    set simlibpath $env(SIM_LIB_PATH)
} else {
    set simlibpath $defaultsimlibpath

}

vsim -quiet -Ldir $simlibpath -L unisim work.main -voptargs=+acc

set StdArithNoWarnings 1
set NumericStdNoWarnings 1

radix -hexadecimal

log -r /*

run -all

