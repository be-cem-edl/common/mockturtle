// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   main
//
// description: A SystemVerilog testbench for the supplied SVEC MT demo.

`include "vme64x_bfm.svh"
`include "svec_vme_buffers.svh"
`include "mock_turtle_driver.svh"

`timescale 1ns/1ps

`define VME_OFFSET      'h8000_0000
`define MT_BASE         `VME_OFFSET + 'h0002_0000

module main;

   reg rst_n = 0;
   reg clk_125m = 0;

   always #4ns clk_125m <= ~clk_125m;

   initial begin
      repeat(40) @(posedge clk_125m);
      rst_n = 1;
   end

   IVME64X VME(rst_n);

   `DECLARE_VME_BUFFERS(VME.slave);

   bit [4:0] slot_id = 8;

   // the Device Under Test
   svec_mt_demo #
     (
      .g_SIMULATION (1)
      )
   DUT
     (
      .rst_n_i             (1'b1),
      .clk_125m_pllref_p_i (clk_125m),
      .clk_125m_pllref_n_i (~clk_125m),
      .fp_led_line_oen_o   (),
      .fp_led_line_o       (),
      .fp_led_column_o     (),
      .fp_gpio1_a2b_o      (),
      .fp_gpio2_a2b_o      (),
      .fp_gpio34_a2b_o     (),
      .fp_gpio1_b          (),
      .fp_gpio2_b          (),
      .fp_gpio3_b          (),
      .fp_gpio4_b          (),
      .fp_term_en_o        (),
      .pcbrev_i            (5'b0),
      .onewire_b           (),
      .carrier_scl_b       (),
      .carrier_sda_b       (),
      .spi_miso_i          (1'b0),
      .spi_sclk_o          (),
      .spi_ncs_o           (),
      .spi_mosi_o          (),
      .fmc0_prsnt_m2c_n_i  (1'b0),
      .fmc0_scl_b          (),
      .fmc0_sda_b          (),
      .fmc1_prsnt_m2c_n_i  (1'b0),
      .fmc1_scl_b          (),
      .fmc1_sda_b          (),
      .vme_sysreset_n_i    (VME_RST_n),
      .vme_as_n_i          (VME_AS_n),
      .vme_write_n_i       (VME_WRITE_n),
      .vme_am_i            (VME_AM),
      .vme_ds_n_i          (VME_DS_n),
      .vme_gap_i           (^slot_id),
      .vme_ga_i            (~slot_id),
      .vme_berr_o          (VME_BERR),
      .vme_dtack_n_o       (VME_DTACK_n),
      .vme_retry_n_o       (VME_RETRY_n),
      .vme_retry_oe_o      (VME_RETRY_OE),
      .vme_lword_n_b       (VME_LWORD_n),
      .vme_addr_b          (VME_ADDR),
      .vme_data_b          (VME_DATA),
      .vme_irq_o           (VME_IRQ_n),
      .vme_iack_n_i        (VME_IACK_n),
      .vme_iackin_n_i      (VME_IACKIN_n),
      .vme_iackout_n_o     (VME_IACKOUT_n),
      .vme_dtack_oe_o      (VME_DTACK_OE),
      .vme_data_dir_o      (VME_DATA_DIR),
      .vme_data_oe_n_o     (VME_DATA_OE_N),
      .vme_addr_dir_o      (VME_ADDR_DIR),
      .vme_addr_oe_n_o     (VME_ADDR_OE_N));

   IMockTurtleIRQ IrqMonitor (`MT_ATTACH_IRQ(DUT.U_Mock_Turtle));

   string fw = "../../../demos/hello_world/firmware/fw-01/fw-hello.bin";

   MockTurtleDriver drv;

   CBusAccessor_VME64x acc;

   initial begin

      acc = new(VME.tb);

      $timeformat (-6, 3, "us", 10);

      /* map func0 to 0x80000000, A32 */
      acc.write('h7ff63, 'h80, A32|CR_CSR|D08Byte3);
      acc.write('h7ff67, 0, CR_CSR|A32|D08Byte3);
      acc.write('h7ff6b, 0, CR_CSR|A32|D08Byte3);
      acc.write('h7ff6f, 36, CR_CSR|A32|D08Byte3);
      acc.write('h7ff33, 1, CR_CSR|A32|D08Byte3);
      acc.write('h7fffb, 'h10, CR_CSR|A32|D08Byte3); /* enable module (BIT_SET = 0x10) */

      acc.set_default_modifiers(A32 | D32 | SINGLE);
      acc.set_default_xfer_size(A32 | D32 | SINGLE);

      drv = new (acc, `MT_BASE, IrqMonitor);

      // 1us is the default update delay value, this serves as an example
      // on how to change the update loop delay
      drv.init(.update_delay(1us));

      drv.enable_console_irq (0, 1);

      drv.load_firmware(0, fw, 1'b0);

      drv.reset_core(0, 0);

      # 1000us;
   end // initial begin

endmodule // main
