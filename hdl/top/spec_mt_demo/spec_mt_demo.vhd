-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

-- unit name:   spec_mt_demo
--
-- description: Top entity for Mock Turtle SPEC demo.
--
-- This demo instantiates a Mock Turtle CPU with two cores, each connected to
-- a WB GPIO peripheral. Reading and writing to the GPIOs controls the LEDs
-- of the SPEC.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.gn4124_core_pkg.all;
use work.mock_turtle_pkg.all;
use work.mt_mqueue_pkg.all;
use work.wr_board_pkg.all;
use work.wr_fabric_pkg.all;
use work.sourceid_spec_mt_demo_pkg;

entity spec_mt_demo is
  generic (
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the
    -- testbench. Its purpose is to reduce some internal counters/timeouts
    -- to speed up simulations.
    g_SIMULATION : integer := 0);
  port (
    -- resets and clocks
    clk_125m_pllref_p_i : in    std_logic;
    clk_125m_pllref_n_i : in    std_logic;
    -- SPEC front panel LEDs
    led_act_o           : out   std_logic;
    led_link_o          : out   std_logic;
    -- SPEC board LEDs and buttons
    button1_n_i         : in    std_logic;
    aux_leds_o          : out   std_logic_vector(3 downto 0);
    -- PCB version
    pcbrev_i            : in    std_logic_vector(3 downto 0);
    -- Gennum PCI interface
    gn_rst_n_i          : in    std_logic;
    gn_p2l_clk_n_i      : in    std_logic;
    gn_p2l_clk_p_i      : in    std_logic;
    gn_p2l_rdy_o        : out   std_logic;
    gn_p2l_dframe_i     : in    std_logic;
    gn_p2l_valid_i      : in    std_logic;
    gn_p2l_data_i       : in    std_logic_vector(15 downto 0);
    gn_p_wr_req_i       : in    std_logic_vector(1 downto 0);
    gn_p_wr_rdy_o       : out   std_logic_vector(1 downto 0);
    gn_rx_error_o       : out   std_logic;
    gn_l2p_clk_n_o      : out   std_logic;
    gn_l2p_clk_p_o      : out   std_logic;
    gn_l2p_dframe_o     : out   std_logic;
    gn_l2p_valid_o      : out   std_logic;
    gn_l2p_edb_o        : out   std_logic;
    gn_l2p_data_o       : out   std_logic_vector(15 downto 0);
    gn_l2p_rdy_i        : in    std_logic;
    gn_l_wr_rdy_i       : in    std_logic_vector(1 downto 0);
    gn_p_rd_d_rdy_i     : in    std_logic_vector(1 downto 0);
    gn_tx_error_i       : in    std_logic;
    gn_vc_rdy_i         : in    std_logic_vector(1 downto 0);
    gn_gpio_b           : inout std_logic_vector(1 downto 0);
    -- FMC slot management
    fmc0_prsnt_m2c_n_i  : in    std_logic;
    fmc0_scl_b          : inout std_logic;
    fmc0_sda_b          : inout std_logic);
end spec_mt_demo;

architecture arch of spec_mt_demo is

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------

  -- MT Node identification (MTDC)
  constant c_MT_NODE_ID : std_logic_vector(31 downto 0) := x"4D54_4443";

  constant c_MT_CONFIG : t_mt_config := (
    app_id     => c_MT_NODE_ID,
    cpu_count  => 2,
    cpu_config => (others =>
                   (memsize => 8192,
                    hmq_config => (2, (0      => (7, 3, 2, x"0000_0000", false),
                                       1      => (5, 4, 3, x"0000_0000", false),
                                       others => (c_DUMMY_MT_MQUEUE_SLOT))),
                    rmq_config => (1, (0      => (7, 2, 2, x"0000_0000", false),
                                       others => (c_DUMMY_MT_MQUEUE_SLOT))))),
    shared_mem_size => 2048);

  -- Number of masters attached to the primary wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 1;

  -- Number of slaves attached to the primary wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 2;

  -- Primary Wishbone master(s) offsets
  constant c_WB_MASTER_GENNUM : integer := 0;

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_METADATA : integer := 0;
  constant c_WB_SLAVE_MT       : integer := 1;

  -- Convention metadata base address
  constant c_METADATA_ADDR : t_wishbone_address := x"0000_2000";

  -- Primary wishbone crossbar layout
  constant c_WB_LAYOUT_ADDR :
    t_wishbone_address_array(c_NUM_WB_SLAVES - 1 downto 0) := (
      c_WB_SLAVE_METADATA => c_METADATA_ADDR,
      c_WB_SLAVE_MT       => x"0002_0000");

  constant c_WB_LAYOUT_MASK :
    t_wishbone_address_array(c_NUM_WB_SLAVES - 1 downto 0) := (
      c_WB_SLAVE_METADATA => x"0003_ffc0",  --    0x40 bytes
      c_WB_SLAVE_MT       => x"0002_0000"); -- 0x20000 bytes

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  -- Clocks and resets
  signal clk_sys_62m5       : std_logic;
  signal rst_sys_62m5_n     : std_logic := '0';

  -- Wishbone buse(s) from master(s) to crossbar slave port(s)
  signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

  -- Wishbone buse(s) from crossbar master port(s) to slave(s)
  signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
  signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

  -- MT Dedicated WB interfaces to peripherals
  signal dp_wb_out : t_wishbone_master_out_array(1 downto 0);
  signal dp_wb_in  : t_wishbone_master_in_array(1 downto 0);

  signal cpu0_gpio_out, cpu1_gpio_out : std_logic_vector(7 downto 0);
  signal cpu_gpio_out, cpu_gpio_in    : std_logic_vector(7 downto 0);

  signal irq_vector : std_logic_vector(3 downto 0);

begin  -- architecture arch

  cmp_xwb_metadata : entity work.xwb_metadata
    generic map (
      g_VENDOR_ID    => x"0000_10DC",
      g_DEVICE_ID    => c_MT_NODE_ID,
      g_VERSION      => sourceid_spec_mt_demo_pkg.version,
      g_CAPABILITIES => x"0000_0000",
      g_COMMIT_ID    => sourceid_spec_mt_demo_pkg.sourceid)
    port map (
      clk_i   => clk_sys_62m5,
      rst_n_i => rst_sys_62m5_n,
      wb_i    => cnx_slave_in(c_WB_SLAVE_METADATA),
      wb_o    => cnx_slave_out(c_WB_SLAVE_METADATA));

  inst_spec_base : entity work.spec_base_wr
    generic map (
      g_WITH_VIC      => TRUE,
      g_WITH_ONEWIRE  => FALSE,
      g_WITH_SPI      => FALSE,
      g_WITH_WR       => FALSE,
      g_WITH_DDR      => FALSE,
      g_APP_OFFSET    => c_METADATA_ADDR,
      g_NUM_USER_IRQ  => 4,
      g_SIMULATION    => f_int2bool(g_SIMULATION))
    port map (
      clk_125m_pllref_p_i => clk_125m_pllref_p_i,
      clk_125m_pllref_n_i => clk_125m_pllref_n_i,
      gn_rst_n_i          => gn_rst_n_i,
      gn_p2l_clk_n_i      => gn_p2l_clk_n_i,
      gn_p2l_clk_p_i      => gn_p2l_clk_p_i,
      gn_p2l_rdy_o        => gn_p2l_rdy_o,
      gn_p2l_dframe_i     => gn_p2l_dframe_i,
      gn_p2l_valid_i      => gn_p2l_valid_i,
      gn_p2l_data_i       => gn_p2l_data_i,
      gn_p_wr_req_i       => gn_p_wr_req_i,
      gn_p_wr_rdy_o       => gn_p_wr_rdy_o,
      gn_rx_error_o       => gn_rx_error_o,
      gn_l2p_clk_n_o      => gn_l2p_clk_n_o,
      gn_l2p_clk_p_o      => gn_l2p_clk_p_o,
      gn_l2p_dframe_o     => gn_l2p_dframe_o,
      gn_l2p_valid_o      => gn_l2p_valid_o,
      gn_l2p_edb_o        => gn_l2p_edb_o,
      gn_l2p_data_o       => gn_l2p_data_o,
      gn_l2p_rdy_i        => gn_l2p_rdy_i,
      gn_l_wr_rdy_i       => gn_l_wr_rdy_i,
      gn_p_rd_d_rdy_i     => gn_p_rd_d_rdy_i,
      gn_tx_error_i       => gn_tx_error_i,
      gn_vc_rdy_i         => gn_vc_rdy_i,
      gn_gpio_b           => gn_gpio_b,
      spi_miso_i          => '0',
      fmc0_scl_b          => fmc0_scl_b,
      fmc0_sda_b          => fmc0_sda_b,
      fmc0_prsnt_m2c_n_i  => fmc0_prsnt_m2c_n_i,
      pcbrev_i            => pcbrev_i,
      button1_n_i         => button1_n_i,
      clk_62m5_sys_o      => clk_sys_62m5,
      rst_62m5_sys_n_o    => rst_sys_62m5_n,
      irq_user_i          => irq_vector,
      app_wb_o            => cnx_master_out(c_WB_MASTER_GENNUM),
      app_wb_i            => cnx_master_in(c_WB_MASTER_GENNUM));

  cmp_crossbar : xwb_crossbar
    generic map (
      g_VERBOSE     => FALSE,
      g_NUM_MASTERS => c_NUM_WB_MASTERS,
      g_NUM_SLAVES  => c_NUM_WB_SLAVES,
      g_REGISTERED  => TRUE,
      g_ADDRESS     => c_WB_LAYOUT_ADDR,
      g_MASK        => c_WB_LAYOUT_MASK)
    port map (
      clk_sys_i => clk_sys_62m5,
      rst_n_i   => rst_sys_62m5_n,
      slave_i   => cnx_master_out,
      slave_o   => cnx_master_in,
      master_i  => cnx_slave_out,
      master_o  => cnx_slave_in);

  U_Mock_Turtle : mock_turtle_core
    generic map (
      g_CONFIG => c_MT_CONFIG)
    port map (
      clk_i         => clk_sys_62m5,
      rst_n_i       => rst_sys_62m5_n,
      dp_master_o   => dp_wb_out,
      dp_master_i   => dp_wb_in,
      host_slave_i  => cnx_slave_in(c_WB_SLAVE_MT),
      host_slave_o  => cnx_slave_out(c_WB_SLAVE_MT),
      hmq_in_irq_o  => irq_vector(0),
      hmq_out_irq_o => irq_vector(1),
      notify_irq_o  => irq_vector(3),
      console_irq_o => irq_vector(2));

  U_GPIO_CPU0 : xwb_gpio_port
    generic map (
      g_INTERFACE_MODE         => PIPELINED,
      g_ADDRESS_GRANULARITY    => BYTE,
      g_NUM_PINS               => 8,
      g_WITH_BUILTIN_SYNC      => FALSE,
      g_WITH_BUILTIN_TRISTATES => FALSE)
    port map (
      clk_sys_i  => clk_sys_62m5,
      rst_n_i    => rst_sys_62m5_n,
      slave_i    => dp_wb_out(0),
      slave_o    => dp_wb_in(0),
      gpio_out_o => cpu0_gpio_out,
      gpio_in_i  => cpu_gpio_in,
      gpio_oen_o => open);

  U_GPIO_CPU1 : xwb_gpio_port
    generic map (
      g_INTERFACE_MODE         => PIPELINED,
      g_ADDRESS_GRANULARITY    => BYTE,
      g_NUM_PINS               => 8,
      g_WITH_BUILTIN_SYNC      => FALSE,
      g_WITH_BUILTIN_TRISTATES => FALSE)
    port map (
      clk_sys_i  => clk_sys_62m5,
      rst_n_i    => rst_sys_62m5_n,
      slave_i    => dp_wb_out(1),
      slave_o    => dp_wb_in(1),
      gpio_out_o => cpu1_gpio_out,
      gpio_in_i  => cpu_gpio_in,
      gpio_oen_o => open);

  cpu_gpio_out <= cpu0_gpio_out or cpu1_gpio_out;

  cpu_gpio_in <= cpu_gpio_out;

  led_link_o <= cpu_gpio_out(7);
  led_act_o  <= cpu_gpio_out(6);
  aux_leds_o <= cpu_gpio_out(5 downto 2);

end architecture arch;
