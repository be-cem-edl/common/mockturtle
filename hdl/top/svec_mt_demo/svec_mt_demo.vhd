-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

-- unit name:   svec_mt_demo
--
-- description: Top entity for Mock Turtle SVEC demo.
--
-- This demo instantiates a Mock Turtle CPU with two cores, each connected to
-- a WB GPIO peripheral. Reading and writing to the GPIOs controls the LEDs
-- and LEMO I/O on the front panel of the SVEC.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.vme64x_pkg.all;
use work.mock_turtle_pkg.all;
use work.mt_mqueue_pkg.all;
use work.wr_board_pkg.all;
use work.wr_fabric_pkg.all;
use work.sourceid_svec_mt_demo_pkg;

entity svec_mt_demo is
  generic (
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the
    -- testbench. Its purpose is to reduce some internal counters/timeouts
    -- to speed up simulations.
    g_SIMULATION : integer := 0);
  port (
    -- resets and clocks
    rst_n_i             : in    std_logic;
    clk_125m_pllref_p_i : in    std_logic;
    clk_125m_pllref_n_i : in    std_logic;
    -- SVEC Front panel LEDs and GPIO
    fp_led_line_oen_o   : out   std_logic_vector(1 downto 0);
    fp_led_line_o       : out   std_logic_vector(1 downto 0);
    fp_led_column_o     : out   std_logic_vector(3 downto 0);
    fp_gpio1_a2b_o      : out   std_logic;
    fp_gpio2_a2b_o      : out   std_logic;
    fp_gpio34_a2b_o     : out   std_logic;
    fp_gpio1_b          : inout std_logic;
    fp_gpio2_b          : inout std_logic;
    fp_gpio3_b          : inout std_logic;
    fp_gpio4_b          : inout std_logic;
    fp_term_en_o        : out   std_logic_vector(4 downto 1);
    -- PCB revision
    pcbrev_i            : in    std_logic_vector(4 downto 0);
    -- Onewire interface
    onewire_b           : inout std_logic;
    -- Carrier I2C eeprom
    carrier_scl_b       : inout std_logic;
    carrier_sda_b       : inout std_logic;
    -- Flash memory SPI interface
    spi_sclk_o          : out   std_logic;
    spi_ncs_o           : out   std_logic;
    spi_mosi_o          : out   std_logic;
    spi_miso_i          : in    std_logic;
    -- VME Interface pins
    vme_write_n_i       : in    std_logic;
    vme_sysreset_n_i    : in    std_logic;
    vme_retry_oe_o      : out   std_logic;
    vme_retry_n_o       : out   std_logic;
    vme_lword_n_b       : inout std_logic;
    vme_iackout_n_o     : out   std_logic;
    vme_iackin_n_i      : in    std_logic;
    vme_iack_n_i        : in    std_logic;
    vme_gap_i           : in    std_logic;
    vme_dtack_oe_o      : out   std_logic;
    vme_dtack_n_o       : out   std_logic;
    vme_ds_n_i          : in    std_logic_vector(1 downto 0);
    vme_data_oe_n_o     : out   std_logic;
    vme_data_dir_o      : out   std_logic;
    vme_berr_o          : out   std_logic;
    vme_as_n_i          : in    std_logic;
    vme_addr_oe_n_o     : out   std_logic;
    vme_addr_dir_o      : out   std_logic;
    vme_irq_o           : out   std_logic_vector(7 downto 1);
    vme_ga_i            : in    std_logic_vector(4 downto 0);
    vme_data_b          : inout std_logic_vector(31 downto 0);
    vme_am_i            : in    std_logic_vector(5 downto 0);
    vme_addr_b          : inout std_logic_vector(31 downto 1);
    -- FMC slot management
    fmc0_prsnt_m2c_n_i  : in    std_logic;
    fmc1_prsnt_m2c_n_i  : in    std_logic;
    fmc0_scl_b          : inout std_logic;
    fmc0_sda_b          : inout std_logic;
    fmc1_scl_b          : inout std_logic;
    fmc1_sda_b          : inout std_logic);
end svec_mt_demo;

architecture arch of svec_mt_demo is

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------

  -- MT Node identification (MTDV)
  constant c_MT_NODE_ID : std_logic_vector(31 downto 0) := x"4D54_4456";

  constant c_MT_CONFIG : t_mt_config := (
    app_id     => c_MT_NODE_ID,
    cpu_count  => 2,
    cpu_config => (others =>
                   (memsize => 8192,
                    hmq_config => (2, (0      => (7, 3, 2, x"0000_0000", false),
                                       1      => (5, 4, 3, x"0000_0000", false),
                                       others => (c_DUMMY_MT_MQUEUE_SLOT))),
                    rmq_config => (1, (0      => (7, 2, 2, x"0000_0000", false),
                                       others => (c_DUMMY_MT_MQUEUE_SLOT))))),
    shared_mem_size => 2048);


  -- Number of masters attached to the primary wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 1;

  -- Number of slaves attached to the primary wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 2;

  -- Primary Wishbone master(s) offsets
  constant c_WB_MASTER_VME : integer := 0;

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_METADATA : integer := 0;
  constant c_WB_SLAVE_MT       : integer := 1;

  -- Convention metadata base address
  constant c_METADATA_ADDR : t_wishbone_address := x"0000_4000";

  -- Primary wishbone crossbar layout
  constant c_WB_LAYOUT_ADDR :
    t_wishbone_address_array(c_NUM_WB_SLAVES - 1 downto 0) := (
      c_WB_SLAVE_METADATA => c_METADATA_ADDR,
      c_WB_SLAVE_MT       => x"0002_0000");

  constant c_WB_LAYOUT_MASK :
    t_wishbone_address_array(c_NUM_WB_SLAVES - 1 downto 0) := (
      c_WB_SLAVE_METADATA => x"0003_ffc0",  --    0x40 bytes
      c_WB_SLAVE_MT       => x"0002_0000"); -- 0x20000 bytes

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  -- Clocks and resets
  signal areset_n       : std_logic := '0';
  signal clk_sys_62m5   : std_logic;
  signal rst_sys_62m5_n : std_logic := '0';

  -- Wishbone buse(s) from master(s) to crossbar slave port(s)
  signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

  -- Wishbone buse(s) from crossbar master port(s) to slave(s)
  signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
  signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

  -- MT Dedicated WB interfaces to peripherals
  signal dp_wb_out : t_wishbone_master_out_array(1 downto 0);
  signal dp_wb_in  : t_wishbone_master_in_array(1 downto 0);

  signal cpu0_gpio_oen, cpu1_gpio_oen : std_logic_vector(23 downto 0);
  signal cpu0_gpio_out, cpu1_gpio_out : std_logic_vector(23 downto 0);
  signal cpu_gpio_oen                 : std_logic_vector(23 downto 0);
  signal cpu_gpio_out, cpu_gpio_in    : std_logic_vector(23 downto 0);

  signal irq_vector : std_logic_vector(3 downto 0);

begin  -- architecture arch

  areset_n <= vme_sysreset_n_i and rst_n_i;

  cmp_xwb_metadata : entity work.xwb_metadata
    generic map (
      g_VENDOR_ID    => x"0000_10DC",
      g_DEVICE_ID    => c_MT_NODE_ID,
      g_VERSION      => sourceid_svec_mt_demo_pkg.version,
      g_CAPABILITIES => x"0000_0000",
      g_COMMIT_ID    => sourceid_svec_mt_demo_pkg.sourceid)
    port map (
      clk_i   => clk_sys_62m5,
      rst_n_i => rst_sys_62m5_n,
      wb_i    => cnx_slave_in(c_WB_SLAVE_METADATA),
      wb_o    => cnx_slave_out(c_WB_SLAVE_METADATA));

  inst_svec_base : entity work.svec_base_wr
    generic map (
      g_DECODE_AM     => FALSE,
      g_WITH_VIC      => TRUE,
      g_WITH_ONEWIRE  => FALSE,
      g_WITH_SPI      => FALSE,
      g_WITH_WR       => FALSE,
      g_WITH_DDR4     => FALSE,
      g_WITH_DDR5     => FALSE,
      g_APP_OFFSET    => c_METADATA_ADDR,
      g_NUM_USER_IRQ  => 4,
      g_SIMULATION    => g_SIMULATION)
    port map (
      rst_n_i              => areset_n,
      clk_125m_pllref_p_i  => clk_125m_pllref_p_i,
      clk_125m_pllref_n_i  => clk_125m_pllref_n_i,
      vme_write_n_i        => vme_write_n_i,
      vme_sysreset_n_i     => vme_sysreset_n_i,
      vme_retry_oe_o       => vme_retry_oe_o,
      vme_retry_n_o        => vme_retry_n_o,
      vme_lword_n_b        => vme_lword_n_b,
      vme_iackout_n_o      => vme_iackout_n_o,
      vme_iackin_n_i       => vme_iackin_n_i,
      vme_iack_n_i         => vme_iack_n_i,
      vme_gap_i            => vme_gap_i,
      vme_dtack_oe_o       => vme_dtack_oe_o,
      vme_dtack_n_o        => vme_dtack_n_o,
      vme_ds_n_i           => vme_ds_n_i,
      vme_data_oe_n_o      => vme_data_oe_n_o,
      vme_data_dir_o       => vme_data_dir_o,
      vme_berr_o           => vme_berr_o,
      vme_as_n_i           => vme_as_n_i,
      vme_addr_oe_n_o      => vme_addr_oe_n_o,
      vme_addr_dir_o       => vme_addr_dir_o,
      vme_irq_o            => vme_irq_o,
      vme_ga_i             => vme_ga_i,
      vme_data_b           => vme_data_b,
      vme_am_i             => vme_am_i,
      vme_addr_b           => vme_addr_b,
      fmc0_scl_b           => fmc0_scl_b,
      fmc0_sda_b           => fmc0_sda_b,
      fmc1_scl_b           => fmc1_scl_b,
      fmc1_sda_b           => fmc1_sda_b,
      fmc0_prsnt_m2c_n_i   => fmc0_prsnt_m2c_n_i,
      fmc1_prsnt_m2c_n_i   => fmc1_prsnt_m2c_n_i,
      pcbrev_i             => pcbrev_i,
      onewire_b            => onewire_b,
      carrier_scl_b        => carrier_scl_b,
      carrier_sda_b        => carrier_sda_b,
      spi_sclk_o           => spi_sclk_o,
      spi_ncs_o            => spi_ncs_o,
      spi_mosi_o           => spi_mosi_o,
      spi_miso_i           => spi_miso_i,
      clk_sys_62m5_o       => clk_sys_62m5,
      rst_sys_62m5_n_o     => rst_sys_62m5_n,
      irq_user_i           => irq_vector,
      app_wb_o             => cnx_master_out(c_WB_MASTER_VME),
      app_wb_i             => cnx_master_in(c_WB_MASTER_VME));

  cmp_crossbar : xwb_crossbar
    generic map (
      g_VERBOSE     => FALSE,
      g_NUM_MASTERS => c_NUM_WB_MASTERS,
      g_NUM_SLAVES  => c_NUM_WB_SLAVES,
      g_REGISTERED  => TRUE,
      g_ADDRESS     => c_WB_LAYOUT_ADDR,
      g_MASK        => c_WB_LAYOUT_MASK)
    port map (
      clk_sys_i => clk_sys_62m5,
      rst_n_i   => rst_sys_62m5_n,
      slave_i   => cnx_master_out,
      slave_o   => cnx_master_in,
      master_i  => cnx_slave_out,
      master_o  => cnx_slave_in);

  U_Mock_Turtle : mock_turtle_core
    generic map (
      g_CONFIG => c_MT_CONFIG)
    port map (
      clk_i           => clk_sys_62m5,
      rst_n_i         => rst_sys_62m5_n,
      dp_master_o     => dp_wb_out,
      dp_master_i     => dp_wb_in,
      host_slave_i    => cnx_slave_in(c_WB_SLAVE_MT),
      host_slave_o    => cnx_slave_out(c_WB_SLAVE_MT),
      hmq_in_irq_o    => irq_vector(0),
      hmq_out_irq_o   => irq_vector(1),
      notify_irq_o    => irq_vector(3),
      console_irq_o   => irq_vector(2));

  U_GPIO_CPU0 : xwb_gpio_port
    generic map (
      g_INTERFACE_MODE         => PIPELINED,
      g_ADDRESS_GRANULARITY    => BYTE,
      g_NUM_PINS               => 24,
      g_WITH_BUILTIN_SYNC      => FALSE,
      g_WITH_BUILTIN_TRISTATES => FALSE)
    port map (
      clk_sys_i  => clk_sys_62m5,
      rst_n_i    => rst_sys_62m5_n,
      slave_i    => dp_wb_out(0),
      slave_o    => dp_wb_in(0),
      gpio_out_o => cpu0_gpio_out,
      gpio_in_i  => cpu_gpio_in,
      gpio_oen_o => cpu0_gpio_oen);

  U_GPIO_CPU1 : xwb_gpio_port
    generic map (
      g_INTERFACE_MODE         => PIPELINED,
      g_ADDRESS_GRANULARITY    => BYTE,
      g_NUM_PINS               => 24,
      g_WITH_BUILTIN_SYNC      => FALSE,
      g_WITH_BUILTIN_TRISTATES => FALSE)
    port map (
      clk_sys_i  => clk_sys_62m5,
      rst_n_i    => rst_sys_62m5_n,
      slave_i    => dp_wb_out(1),
      slave_o    => dp_wb_in(1),
      gpio_out_o => cpu1_gpio_out,
      gpio_in_i  => cpu_gpio_in,
      gpio_oen_o => cpu1_gpio_oen);

  cpu_gpio_out <= cpu0_gpio_out or cpu1_gpio_out;
  cpu_gpio_oen <= cpu0_gpio_oen or cpu1_gpio_oen;

  -- FP GPIO directions and termination
  fp_gpio1_a2b_o  <= cpu_gpio_oen(0);
  fp_gpio2_a2b_o  <= cpu_gpio_oen(1);
  fp_gpio34_a2b_o <= cpu_gpio_oen(2);
  fp_term_en_o    <= (others => '0');

  -- FP GPIO bidir in/out (3 and 4 share the same direction line)
  fp_gpio1_b <= cpu_gpio_out(0) when cpu_gpio_oen(0) = '1' else 'Z';
  fp_gpio2_b <= cpu_gpio_out(1) when cpu_gpio_oen(1) = '1' else 'Z';
  fp_gpio3_b <= cpu_gpio_out(2) when cpu_gpio_oen(2) = '1' else 'Z';
  fp_gpio4_b <= cpu_gpio_out(3) when cpu_gpio_oen(2) = '1' else 'Z';

  -- gpio inputs (same for both CPUs)
  cpu_gpio_in(0) <= fp_gpio1_b;
  cpu_gpio_in(1) <= fp_gpio2_b;
  cpu_gpio_in(2) <= fp_gpio3_b;
  cpu_gpio_in(3) <= fp_gpio4_b;

  cpu_gpio_in(23 downto 4) <= cpu_gpio_out(23 downto 4);

  U_LED_Controller : gc_bicolor_led_ctrl
    generic map(
      g_NB_COLUMN    => 4,
      g_NB_LINE      => 2,
      g_CLK_FREQ     => 62500000,    -- in Hz
      g_REFRESH_RATE => 250)         -- in Hz
    port map(
      rst_n_i         => rst_sys_62m5_n,
      clk_i           => clk_sys_62m5,
      led_intensity_i => "1100100",  -- in %
      led_state_i     => cpu_gpio_out(23 downto 8),
      column_o        => fp_led_column_o,
      line_o          => fp_led_line_o,
      line_oen_o      => fp_led_line_oen_o);

end architecture arch;
