-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_per_cpu_csr_pkg
--
-- description: Per CPU CSR ports, not to be confused with the wbgen generated
-- file that describes the whole CSR interface.

library ieee;
use ieee.std_logic_1164.all;

package mt_per_cpu_csr_pkg is

  -- Per CPU CSR inputs (from urv to mt)
  type t_mt_per_cpu_csr_in is record
    udata_i          : std_logic_vector(31 downto 0);
    dbg_cpu_status_i : std_logic;
    dbg_insn_ready_i : std_logic;
    dbg_mbx_i        : std_logic_vector(31 downto 0);
  end record t_mt_per_cpu_csr_in;

  type t_mt_per_cb_csr_in is record
    cpu_i            : t_mt_per_cpu_csr_in;
    host_int_i       : std_logic;
    host_int_val_i   : std_logic_vector(7 downto 0);
    hmqi_status_i    : std_logic_vector(7 downto 0);
    hmqo_status_i    : std_logic_vector(7 downto 0);
  end record t_mt_per_cb_csr_in;

  -- Per CPU CSR outputs (from mt to urv)
  type t_mt_per_cpu_csr_out is record
    reset_o         : std_logic;
    uaddr_addr_o    : std_logic_vector(19 downto 0);
    core_sel_o      : std_logic;
    udata_o         : std_logic_vector(31 downto 0);
    udata_load_o    : std_logic;
    dbg_cpu_force_o : std_logic;
    dbg_insn_o      : std_logic_vector(31 downto 0);
    dbg_insn_wr_o   : std_logic;
    dbg_mbx_o       : std_logic_vector(31 downto 0);
    dbg_mbx_wr_o    : std_logic;
  end record t_mt_per_cpu_csr_out;

  type t_mt_per_cb_csr_out is record
    cpu_o            : t_mt_per_cpu_csr_out;
  end record t_mt_per_cb_csr_out;

end package mt_per_cpu_csr_pkg;
