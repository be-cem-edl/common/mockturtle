#!/bin/bash

# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

mkdir -p doc
wbgen2 -V mt_cpu_csr_wb.vhd -p mt_cpu_csr_wbgen2_pkg.vhd --hstyle record -Z --lang vhdl -K ../../testbench/include/regs/mt_cpu_csr_regs.vh mt_cpu_csr.wb
wbgen2 -V mt_cpu_lr_wb.vhd  -p mt_cpu_lr_wbgen2_pkg.vhd  --hstyle record -Z --lang vhdl -K ../../testbench/include/regs/mt_cpu_lr_regs.vh  mt_cpu_lr.wb
