# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

files = [
    "mt_cpu_cb.vhd",
    "mt_cpu_csr_wbgen2_pkg.vhd",
    "mt_cpu_csr_wb.vhd",
    "mt_per_cpu_csr_pkg.vhd",
    "mt_cpu_lr_wbgen2_pkg.vhd",
    "mt_cpu_lr_wb.vhd",
    "mt_urv_wrapper.vhd",
]
