-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_cpu_cb
--
-- description: MT CPU Core block top level. Connects CPU, dedicated peripheral,
-- program/data memory and control registers.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.gencores_pkg.all;
use work.genram_pkg.all;
use work.mt_mqueue_pkg.all;
use work.mt_cpu_lr_wbgen2_pkg.all;
use work.mt_per_cpu_csr_pkg.all;
use work.mock_turtle_pkg.all;

entity mt_cpu_cb is
  generic (
    g_CPU_ID            : integer;
    g_CPU_CONFIG        : t_mt_cpu_config;
    g_CPU_IRAM_INIT     : string;
    g_SYSTEM_CLOCK_FREQ : integer;
    g_WITH_WHITE_RABBIT : boolean);
  port (
    clk_sys_i           : in  std_logic;
    rst_n_i             : in  std_logic;
    cpu_ext_rst_n_i     : in  std_logic;
    clk_ref_i           : in  std_logic;
    rst_n_ref_i         : in  std_logic;
    tm_i                : in  t_mt_timing_if;

    sh_master_i         : in  t_wishbone_master_in := c_DUMMY_WB_MASTER_IN;
    sh_master_o         : out t_wishbone_master_out;
    dp_master_i         : in  t_wishbone_master_in := c_DUMMY_WB_MASTER_IN;
    dp_master_o         : out t_wishbone_master_out;

    hmq_slave_i         : in  t_wishbone_slave_in;
    hmq_slave_o         : out t_wishbone_slave_out;

    rmq_src_o           : out t_mt_stream_source_out_array(0 to g_CPU_CONFIG.rmq_config.slot_count-1);
    rmq_src_i           : in  t_mt_stream_source_in_array(0 to g_CPU_CONFIG.rmq_config.slot_count-1);
    rmq_snk_o           : out t_mt_stream_sink_out_array(0 to g_CPU_CONFIG.rmq_config.slot_count-1);
    rmq_snk_i           : in  t_mt_stream_sink_in_array(0 to g_CPU_CONFIG.rmq_config.slot_count-1);

    rmq_src_config_o    : out t_mt_stream_config_out_array( 0 to g_CPU_CONFIG.rmq_config.slot_count-1);
    rmq_src_config_i    : in t_mt_stream_config_in_array( 0 to g_CPU_CONFIG.rmq_config.slot_count-1);
    rmq_snk_config_o    : out t_mt_stream_config_out_array( 0 to g_CPU_CONFIG.rmq_config.slot_count-1);
    rmq_snk_config_i    : in t_mt_stream_config_in_array( 0 to g_CPU_CONFIG.rmq_config.slot_count-1);

    cb_csr_i            : in  t_mt_per_cb_csr_out;
    cb_csr_o            : out t_mt_per_cb_csr_in;

    gpio_i              : in  std_logic_vector(31 downto 0);
    gpio_o              : out std_logic_vector(31 downto 0);

    uart_drdy_o         : out std_logic;
    uart_dack_i         : in  std_logic;
    uart_data_o         : out std_logic_vector(7 downto 0));
end entity mt_cpu_cb;

architecture arch of mt_cpu_cb is

  constant c_SLAVE_NUM : natural := 5;
  constant c_SLAVE_LR  : integer := 0;
  constant c_SLAVE_HMQ : integer := 1;
  constant c_SLAVE_RMQ : integer := 2;
  constant c_SLAVE_DP  : integer := 3;
  constant c_SLAVE_SI  : integer := 4;

  constant c_CNX_ADDRESS : t_wishbone_address_array(c_SLAVE_NUM-1 downto 0) := (
    c_SLAVE_LR  => x"00100000",  -- local regs
    c_SLAVE_HMQ => x"00200000",  -- host message queue
    c_SLAVE_RMQ => x"00300000",  -- remote message queue
    c_SLAVE_DP  => x"20000000",  -- dedicated peripheral port
    c_SLAVE_SI  => x"40000000"   -- shared interconnect
    );

  constant c_CNX_MASK : t_wishbone_address_array(c_SLAVE_NUM-1 downto 0) := (
    c_SLAVE_LR  => x"fff00000",
    c_SLAVE_HMQ => x"fff00000",
    c_SLAVE_RMQ => x"fff00000",
    c_SLAVE_DP  => x"f0000000",
    c_SLAVE_SI  => x"c0000000"
    );

  signal cnx_master_in  : t_wishbone_master_in_array(c_SLAVE_NUM-1 downto 0);
  signal cnx_master_out : t_wishbone_master_out_array(c_SLAVE_NUM-1 downto 0);

  signal tai_sec_rd_ack : std_logic;
  signal local_regs_in  : t_mt_cpu_lr_in_registers;
  signal local_regs_out : t_mt_cpu_lr_out_registers;

  signal cpu_dwb_out : t_wishbone_master_out;
  signal cpu_dwb_in  : t_wishbone_master_in;

  signal tai_sys                   : std_logic_vector(31 downto 0);
  signal delay_cnt                 : std_logic_vector(31 downto 0);
  signal cycles_sys, cycles_sys_d0 : std_logic_vector(27 downto 0);

  signal tai_ref    : std_logic_vector(31 downto 0);
  signal cycles_ref : std_logic_vector(27 downto 0);

  signal tm_p_ref, tm_ready_ref, tm_p_sys : std_logic;

  signal uart_fifo_empty   : std_logic;
  signal uart_fifo_full    : std_logic;
  signal uart_fifo_wr      : std_logic;
  signal uart_fifo_reset_n : std_logic;

  alias a_rmq_config : t_mt_mqueue_config is g_CPU_CONFIG.rmq_config;
  alias a_hmq_config : t_mt_mqueue_config is g_CPU_CONFIG.hmq_config;

  constant c_HMQ_COUNT : natural := a_hmq_config.slot_count;

  -- necessary to relax timing on p_tm_cross process
  attribute keep : string;
  attribute keep of tm_p_sys : signal is "true";

begin  -- arch

  gen_with_wr_1 : if g_WITH_WHITE_RABBIT generate
    U_Sync1 : gc_pulse_synchronizer2
      port map (
        clk_in_i    => clk_ref_i,
        rst_in_n_i  => rst_n_ref_i,
        clk_out_i   => clk_sys_i,
        rst_out_n_i => rst_n_i,
        d_ready_o   => tm_ready_ref,
        d_p_i       => tm_p_ref,
        q_p_o       => tm_p_sys);

    U_Sync2 : gc_sync_ffs
      port map (
        clk_i    => clk_sys_i,
        rst_n_i  => rst_n_i,
        data_i   => tm_i.link_up,
        synced_o => local_regs_in.wr_stat_link_ok_i);

    U_Sync3 : gc_sync_ffs
      port map (
        clk_i    => clk_sys_i,
        rst_n_i  => rst_n_i,
        data_i   => tm_i.time_valid,
        synced_o => local_regs_in.wr_stat_time_ok_i);

    U_Sync4 : gc_sync_register
      generic map (
        g_WIDTH => 8)
      port map (
        clk_i     => clk_sys_i,
        rst_n_a_i => rst_n_i,
        d_i       => tm_i.aux_locked,
        q_o       => local_regs_in.wr_stat_aux_clock_ok_i);

    process(clk_ref_i)
    begin
      if rising_edge(clk_ref_i) then
        if rst_n_ref_i = '0' then
          tm_p_ref <= '0';
        else
          tm_p_ref <= not tm_p_ref and tm_ready_ref;

          if tm_p_ref = '1' then
            tai_ref    <= tm_i.tai (31 downto 0);
            cycles_ref <= tm_i.cycles;
          end if;
        end if;
      end if;
    end process;

    p_tm_cross: process(clk_sys_i)
    begin
      if rising_edge(clk_sys_i) then
        if tm_p_sys = '1' then
          cycles_sys <= cycles_ref;
          tai_sys    <= tai_ref;
        end if;
        cycles_sys_d0 <= cycles_sys;
      end if;
    end process p_tm_cross;

    -- ugly hack!
    process(clk_sys_i)
    begin
      if rising_edge(clk_sys_i) then
        if tai_sec_rd_ack = '1' then
          local_regs_in.tai_cycles_i <= cycles_sys_d0;
        end if;
      end if;
    end process;

    local_regs_in.tai_sec_i <= tai_sys;

  end generate gen_with_wr_1;

  gen_without_wr_1 : if not g_WITH_WHITE_RABBIT generate

    process(clk_sys_i)
    begin
      if rising_edge(clk_sys_i) then
        if rst_n_i = '0' then
          cycles_sys <= (others => '0');
          tai_sys    <= (others => '0');
        else
          if unsigned(cycles_sys) = g_SYSTEM_CLOCK_FREQ - 1 then
            cycles_sys <= (others => '0');
            tai_sys    <= std_logic_vector(unsigned(tai_sys) + 1);
          else
            cycles_sys <= std_logic_vector(unsigned(cycles_sys) + 1);
          end if;
          cycles_sys_d0 <= cycles_sys;
        end if;
      end if;
    end process;

    process(clk_sys_i)
    begin
      if rising_edge(clk_sys_i) then
        if tai_sec_rd_ack = '1' then
          local_regs_in.tai_cycles_i <= cycles_sys_d0;
        end if;
      end if;
    end process;

    local_regs_in.tai_sec_i              <= tai_sys;
    local_regs_in.wr_stat_link_ok_i      <= '0';
    local_regs_in.wr_stat_time_ok_i      <= '0';
    local_regs_in.wr_stat_aux_clock_ok_i <= x"00";

  end generate gen_without_wr_1;

  p_host_int : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        cb_csr_o.host_int_i     <= '0';
        cb_csr_o.host_int_val_i <= (others => '0');
      else
        cb_csr_o.host_int_i <= local_regs_out.ntf_int_load_o;
        if local_regs_out.ntf_int_load_o = '1' then
          cb_csr_o.host_int_val_i <= local_regs_out.ntf_int_o;
        end if;
      end if;
    end if;
  end process p_host_int;

  p_delay_counter : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        delay_cnt <= (others => '0');
      else
        if local_regs_out.delay_cnt_load_o = '1' then
          delay_cnt <= local_regs_out.delay_cnt_o;
        elsif unsigned(delay_cnt) /= 0 then
          delay_cnt <= std_logic_vector(unsigned(delay_cnt) - 1);
        end if;

        local_regs_in.delay_cnt_i <= delay_cnt;

      end if;
    end if;
  end process p_delay_counter;

  p_gpio : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        gpio_o <= (others => '0');
      else
        for i in 0 to 31 loop
          local_regs_in.gpio_in_i(i) <= gpio_i(i);

          if local_regs_out.gpio_set_wr_o = '1' and local_regs_out.gpio_set_o(i) = '1' then
            gpio_o(i) <= '1';
          end if;

          if local_regs_out.gpio_clear_wr_o = '1' and local_regs_out.gpio_clear_o(i) = '1' then
            gpio_o(i) <= '0';
          end if;
        end loop;
      end if;
    end if;
  end process p_gpio;

  U_TheCoreCPU : entity work.mt_urv_wrapper
    generic map (
      g_IRAM_SIZE     => g_CPU_CONFIG.memsize,
      g_IRAM_INIT     => g_CPU_IRAM_INIT,
      g_CPU_ID        => g_CPU_ID)
    port map (
      clk_sys_i       => clk_sys_i,
      rst_n_i         => rst_n_i,
      cpu_ext_rst_n_i => cpu_ext_rst_n_i,
      irq_i           => x"00000000",  -- no irqs, we want to be deterministic...
      dwb_o           => cpu_dwb_out,
      dwb_i           => cpu_dwb_in,
      cpu_csr_i       => cb_csr_i.cpu_o,
      cpu_csr_o       => cb_csr_o.cpu_i);

  U_Local_Registrers : entity work.mt_cpu_lr_wb_slave
    port map (
      rst_n_i          => rst_n_i,
      clk_sys_i        => clk_sys_i,
      wb_adr_i         => cnx_master_out(c_SLAVE_LR).adr(7 downto 2),
      wb_dat_i         => cnx_master_out(c_SLAVE_LR).dat,
      wb_dat_o         => cnx_master_in(c_SLAVE_LR).dat,
      wb_cyc_i         => cnx_master_out(c_SLAVE_LR).cyc,
      wb_sel_i         => cnx_master_out(c_SLAVE_LR).sel,
      wb_stb_i         => cnx_master_out(c_SLAVE_LR).stb,
      wb_we_i          => cnx_master_out(c_SLAVE_LR).we,
      wb_ack_o         => cnx_master_in(c_SLAVE_LR).ack,
      wb_stall_o       => cnx_master_in(c_SLAVE_LR).stall,
      tai_sec_rd_ack_o => tai_sec_rd_ack,
      regs_i           => local_regs_in,
      regs_o           => local_regs_out);

  cnx_master_in(c_SLAVE_LR).err <= '0';
  cnx_master_in(c_SLAVE_LR).rty <= '0';

  U_Host_MQ : entity work.mt_mqueue_host
    generic map (
      g_CONFIG => g_CPU_CONFIG.hmq_config)
    port map (
      clk_i             => clk_sys_i,
      rst_n_i           => rst_n_i,
      cpu_slave_i       => cnx_master_out(c_SLAVE_HMQ),
      cpu_slave_o       => cnx_master_in(c_SLAVE_HMQ),
      host_slave_i      => hmq_slave_i,
      host_slave_o      => hmq_slave_o,
      cpu_in_status_o   => local_regs_in.hmq_stat_in_i(c_HMQ_COUNT-1 downto 0),
      cpu_out_status_o  => local_regs_in.hmq_stat_out_i(c_HMQ_COUNT-1 downto 0),
      host_in_status_o  => cb_csr_o.hmqi_status_i(c_HMQ_COUNT-1 downto 0),
      host_out_status_o => cb_csr_o.hmqo_status_i(c_HMQ_COUNT-1 downto 0));

  local_regs_in.hmq_stat_in_i(7 downto c_HMQ_COUNT)  <= (others => '0');
  local_regs_in.hmq_stat_out_i(7 downto c_HMQ_COUNT) <= (others => '0');
  cb_csr_o.hmqi_status_i(7 downto c_HMQ_COUNT)       <= (others => '0');
  cb_csr_o.hmqo_status_i(7 downto c_HMQ_COUNT)       <= (others => '0');

  U_Remote_MQ : entity work.mt_mqueue_remote
    generic map (
      g_CONFIG => a_rmq_config
      )
    port map (
      clk_i        => clk_sys_i,
      rst_n_i      => rst_n_i,
      slave_i      => cnx_master_out(c_SLAVE_RMQ),
      slave_o      => cnx_master_in(c_SLAVE_RMQ),
      src_o        => rmq_src_o,
      src_i        => rmq_src_i,
      snk_i        => rmq_snk_i,
      snk_o        => rmq_snk_o,
      src_config_o => rmq_src_config_o,
      src_config_i => rmq_src_config_i,
      snk_config_o => rmq_snk_config_o,
      snk_config_i => rmq_snk_config_i,
      rmq_in_status_o  => local_regs_in.rmq_stat_in_i(a_rmq_config.slot_count-1 downto 0),
      rmq_out_status_o => local_regs_in.rmq_stat_out_i(a_rmq_config.slot_count-1 downto 0));

  local_regs_in.rmq_stat_in_i(7 downto a_rmq_config.slot_count) <= (others => '0');
  local_regs_in.rmq_stat_out_i(7 downto a_rmq_config.slot_count) <= (others => '0');

  U_Local_Interconnect : xwb_crossbar
    generic map (
      g_VERBOSE     => FALSE,
      g_NUM_MASTERS => 1,
      g_NUM_SLAVES  => c_SLAVE_NUM,
      g_REGISTERED  => TRUE,
      g_ADDRESS     => c_CNX_ADDRESS,
      g_MASK        => c_CNX_MASK)
    port map (
      clk_sys_i  => clk_sys_i,
      rst_n_i    => rst_n_i,
      slave_i(0) => cpu_dwb_out,
      slave_o(0) => cpu_dwb_in,
      master_i   => cnx_master_in,
      master_o   => cnx_master_out);

  dp_master_o               <= cnx_master_out(c_SLAVE_DP);
  cnx_master_in(c_SLAVE_DP) <= dp_master_i;

  sh_master_o               <= cnx_master_out(c_SLAVE_SI);
  cnx_master_in(c_SLAVE_SI) <= sh_master_i;

  uart_fifo_wr <= not uart_fifo_full and local_regs_out.uart_chr_wr_o;

  U_Uart_Message_FIFO : generic_sync_fifo
    generic map (
      g_DATA_WIDTH             => 8,
      g_SIZE                   => c_MT_UART_MESSAGE_FIFO_SIZE,
      g_SHOW_AHEAD_LEGACY_MODE => FALSE,
      g_SHOW_AHEAD             => TRUE,
      g_WITH_COUNT             => TRUE)
    port map (
      rst_n_i => uart_fifo_reset_n,
      clk_i   => clk_sys_i,
      d_i     => local_regs_out.uart_chr_o,
      we_i    => uart_fifo_wr,
      q_o     => uart_data_o,
      rd_i    => uart_dack_i,
      empty_o => uart_fifo_empty,
      full_o  => uart_fifo_full);

  p_uart_fifo_overflow : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' or cb_csr_i.cpu_o.reset_o = '1' then
        uart_fifo_reset_n <= '0';
        uart_drdy_o       <= '0';
      else
        if uart_fifo_full = '1' then
          uart_fifo_reset_n <= '0';
          uart_drdy_o       <= '0';
        else
          uart_fifo_reset_n <= '1';
          uart_drdy_o       <= not uart_fifo_empty;
        end if;
      end if;
    end if;
  end process p_uart_fifo_overflow;

  local_regs_in.stat_core_id_i <= std_logic_vector(to_unsigned(g_CPU_ID, 4));

end architecture arch;
