-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_cpu_lr
--
-- description: MT CPU Per-Core Local Registers block layout (wbgen2)
--
--------------------------------------------------------------------------------

-- -*- Mode: LUA; tab-width: 2 -*-

peripheral {

   name = "Mock Turtle CPU Per-Core Local Registers";
   prefix = "mt_cpu_lr";
   hdl_entity = "mt_cpu_lr_wb_slave";

   reg {
      name = "Status Register";
      prefix = "STAT";

      field {
	 name = "ID (number) of the CPU core owning this register.";
	 prefix = "CORE_ID";
	 type = SLV;
	 size = 4;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

   };

   reg {
      name = "Notification Interrupt Register";
      prefix = "NTF_INT";

      field {
	 name = "Write a value to send a notification interrupt to the host.";
	 size = 8;
	 type = SLV;
	 access_bus = READ_WRITE;
	 access_dev = READ_WRITE;
	 load = LOAD_EXT;
      };
   };

   reg {
      name = "Serial Console Output";
      prefix = "UART_CHR";

      field {
	 name = "Write port for serial console.";
	 size = 8;
	 type = PASS_THROUGH;
      };
   };

   reg {
      align = 16;
      name = "HMQ Status Register";
      prefix = "HMQ_STAT";

      field {
	 name = "HMQ IN Slot Status";
	 description = "Returns 1 if not empty (a message is available)";
	 prefix = "IN";
	 size = 8;
	 type = SLV;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

      field {
	 align = 16;
	 name = "HMQ OUT Slot Status";
	 description = "Returns 1 if not full (a message can be sent)";
	 prefix = "OUT";
	 size = 8;
	 type = SLV;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

   };

   reg {
      name = "RMQ Status Register";
      prefix = "RMQ_STAT";

      field {
	 name = "RMQ IN Slot Status";
	 description = "Returns 1 if not empty (a message is available)";
	 prefix = "IN";
	 size = 8;
	 type = SLV;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

      field {
	 align = 16;
	 name = "RMQ OUT Slot Status";
	 description = "Returns 1 if not full (a message can be sent)";
	 prefix = "OUT";
	 size = 8;
	 type = SLV;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

   };

   reg {
      align = 16;
      name = "White Rabbit Status Register";
      prefix = "WR_STAT";

      field {
	 name = "WR Link Up";
	 prefix = "LINK_OK";
	 type = BIT;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

      field {
	 name = "WR Time OK";
	 prefix = "TIME_OK";
	 type = BIT;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

      field {
	 align = 8;
	 name = "WR Aux Clock OK";
	 prefix = "AUX_CLOCK_OK";
	 type = SLV;
	 size = 8;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };

   };

   reg {
      name = "TAI Cycles";
      prefix = "TAI_CYCLES";
      field {
	 name = "When White Rabbit is enabled, this returns the TAI clock ticks.";
	 description = "Without WR, it just counts ticks of the system clock";
	 size = 28;
	 type = SLV;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };
   };

   reg {
      name = "TAI Seconds";
      prefix = "TAI_SEC";
      field {
	 name = "When White Rabbit is enabled, this returns the TAI seconds.";
	 description = "Without WR, it just counts seconds based on ticks of the system clock";
	 size = 32;
	 type = SLV;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
	 ack_read = "tai_sec_rd_ack_o";
      };
   };

   reg {
      name = "Delay Counter Register";
      prefix = "DELAY_CNT";

      field {
	 name = "Counts down at every system clock cycle and stops at 0.";
	 description = "Useful for generating delays.";
	 size = 32;
	 type = SLV;
	 access_bus = READ_WRITE;
	 access_dev = READ_WRITE;
	 load = LOAD_EXT;
      };
   };


   reg {
      align = 16;
      name = "GPIO Input";
      prefix = "GPIO_IN";
      field {
	 name = "GPIO In";
	 size = 32;
	 type = SLV;
	 access_bus = READ_ONLY;
	 access_dev = WRITE_ONLY;
      };
   };

   reg {
      name = "GPIO Set";
      prefix = "GPIO_SET";

      field {
	 name = "GPIO Set";
	 size = 32;
	 type = PASS_THROUGH;
      };
   };

   reg {
      name = "GPIO Clear";
      prefix = "GPIO_CLEAR";

      field {
	 name = "GPIO Clear";
	 size = 32;
	 type = PASS_THROUGH;
      };
   };

};
