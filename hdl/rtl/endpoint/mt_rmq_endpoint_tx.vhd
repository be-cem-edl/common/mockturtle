-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_outgoing_slot
--
-- description: Single outgoing slot (MT->world) of the Remote Message Queue.
--  Decode configuration registers.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.genram_pkg.all;
use work.mt_mqueue_pkg.all;
use work.mt_endpoint_pkg.all;

entity mt_rmq_endpoint_tx is
  port (
    clk_i: in std_logic;
    rst_n_i : in std_logic;

    snk_config_o : out t_mt_stream_config_in;
    snk_config_i : in t_mt_stream_config_out;

    config_o : out t_rmq_ep_tx_config
    );
end mt_rmq_endpoint_tx;

architecture arch of mt_rmq_endpoint_tx is

  constant c_ADDR_CONFIG    : integer := 0;
  constant c_ADDR_MAC_HI    : integer := 1;
  constant c_ADDR_MAC_LO    : integer := 2;
  constant c_ADDR_VLAN_ID   : integer := 3;
  constant c_ADDR_ETHERTYPE : integer := 4;
  constant c_ADDR_DST_IP    : integer := 5;
  constant c_ADDR_DST_PORT  : integer := 6;
  constant c_ADDR_SRC_IP    : integer := 7;
  constant c_ADDR_SRC_PORT  : integer := 8;

  signal config : t_rmq_ep_tx_config;

begin  -- arch


  p_write_config_regs : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
      else
        if snk_config_i.we = '1' then
          case to_integer(unsigned(snk_config_i.adr(9 downto 2))) is
            when c_ADDR_CONFIG =>
              config.is_udp  <= snk_config_i.dat(1);
              config.vlan_en <= snk_config_i.dat(3);
            when c_ADDR_MAC_HI =>
              config.dst_mac(47 downto 32) <= snk_config_i.dat(15 downto 0);
            when c_ADDR_MAC_LO =>
              config.dst_mac(31 downto 0) <= snk_config_i.dat;
            when c_ADDR_VLAN_ID =>
              config.vlan_id <= snk_config_i.dat(15 downto 0);
            when c_ADDR_ETHERTYPE =>
              config.ethertype <= snk_config_i.dat(15 downto 0);
            when c_ADDR_DST_IP =>
              config.dst_ip <= snk_config_i.dat;
            when c_ADDR_DST_PORT =>
              config.dst_port <= snk_config_i.dat(15 downto 0);
            when c_ADDR_SRC_IP =>
              config.src_ip <= snk_config_i.dat;
            when c_ADDR_SRC_PORT =>
              config.src_port <= snk_config_i.dat(15 downto 0);
            when others => null;
          end case;
        end if;
      end if;
    end if;
  end process p_write_config_regs;

  p_read_config_regs : process(config, snk_config_i)
  begin
    case to_integer(unsigned(snk_config_i.adr(9 downto 2))) is
      when c_ADDR_CONFIG =>
        snk_config_o.dat <= (1 => config.is_udp, 3 => config.vlan_en, others => '0');
      when c_ADDR_MAC_HI =>
        snk_config_o.dat <= x"0000" & config.dst_mac(47 downto 32);
      when c_ADDR_MAC_LO =>
        snk_config_o.dat <= config.dst_mac(31 downto 0);
      when c_ADDR_ETHERTYPE =>
        snk_config_o.dat <= x"0000" & config.ethertype;
      when c_ADDR_VLAN_ID =>
        snk_config_o.dat <= x"0000" & config.vlan_id;
      when c_ADDR_DST_IP =>
        snk_config_o.dat <= config.dst_ip;
      when c_ADDR_DST_PORT =>
        snk_config_o.dat <= x"0000" & config.dst_port;
      when c_ADDR_SRC_IP =>
        snk_config_o.dat <= config.src_ip;
      when c_ADDR_SRC_PORT =>
        snk_config_o.dat <= x"0000" & config.src_port;
      when others =>
        snk_config_o.dat <= (others => '0');
    end case;
  end process p_read_config_regs;

  config_o <= config;

end architecture arch;
