-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_ethernet_endpoint
--
-- description: Ethernet endpoint for the Mock Turtle Core - top level
--   mux and demux all endpoints to ethernet.

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mock_turtle_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;
use work.gencores_pkg.all;
use work.mt_endpoint_pkg.all;
use work.wr_fabric_pkg.all;

entity mt_rmq_ethernet_endpoint is
  generic (
    g_CONFIG : t_mt_config := c_DEFAULT_MT_CONFIG
    );
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    mt_rmq_i : in t_mt_rmq_endpoint_iface_out;
    mt_rmq_o : out t_mt_rmq_endpoint_iface_in;

    -- Fabric interface to the WR Core
    eth_src_o : out t_wrf_source_out;
    eth_src_i : in  t_wrf_source_in;
    eth_snk_o : out t_wrf_sink_out;
    eth_snk_i : in  t_wrf_sink_in
    );
end mt_rmq_ethernet_endpoint;

architecture arch of mt_rmq_ethernet_endpoint is

  type t_outgoing_config_array2d is array(0 to 7, 0 to 7) of t_rmq_ep_tx_config;


  function f_count_out_slots return integer is
    variable cnt : integer := 0;
  begin
    for i in 0 to g_CONFIG.cpu_count-1 loop
      for j in 0 to g_CONFIG.cpu_config(i).rmq_config.slot_count-1 loop
        cnt := cnt + 1;
      end loop;
    end loop;
    return cnt;
  end function;

  function f_slot_index(cpu : integer; slot : integer) return integer is
    variable cnt : integer := 0;
  begin
    for i in 0 to g_CONFIG.cpu_count-1 loop
      for j in 0 to g_CONFIG.cpu_config(i).rmq_config.slot_count-1 loop
        if(i = cpu and slot = j) then
          return cnt;
        else
          cnt := cnt + 1;
        end if;
      end loop;
    end loop;
    return cnt;
  end function;

  function f_prio_encode(x : std_logic_vector) return integer is
  begin
    for i in 0 to x'length-1 loop
      if x(i) = '1' then
        return i;
      end if;
    end loop;
    return 0;
  end f_prio_encode;


  type t_out_config_array is array(0 to f_count_out_slots-1) of t_rmq_ep_tx_config;

  signal config_out       : t_out_config_array;
  signal config_out_muxed : t_rmq_ep_tx_config;
  signal req_out          : std_logic_vector(f_count_out_slots-1 downto 0);
  signal grant_out        : std_logic_vector(f_count_out_slots-1 downto 0);

  signal out_snk_in_flat  : t_mt_stream_sink_in_array(f_count_out_slots-1 downto 0);
  signal out_snk_out_flat : t_mt_stream_sink_out_array(f_count_out_slots-1 downto 0);

  signal wrsrc_snk_in, snk_muxed_in, incoming_snk_in    : t_mt_stream_sink_in;
  signal wrsrc_snk_out, snk_muxed_out, incoming_snk_out : t_mt_stream_sink_out;
  signal wrsnk_src_in                                   : t_mt_stream_source_in;
  signal wrsnk_src_out                                  : t_mt_stream_source_out;

  signal active_out : integer range 0 to f_count_out_slots-1;

  type t_output_arb_state is (IDLE, SEND);

  signal out_state             : t_output_arb_state;
  signal incoming_header_valid : std_logic;
  signal incoming_header       : t_rmq_ep_rx_header;
begin

  gen_foreach_cpu : for i in 0 to g_CONFIG.cpu_count-1 generate
    gen_foreach_slot : for j in 0 to g_CONFIG.cpu_config(i).rmq_config.slot_count-1 generate

      U_TX : entity work.mt_rmq_endpoint_tx
        port map (
          clk_i        => clk_i,
          rst_n_i      => rst_n_i,
          snk_config_i => mt_rmq_i.src_config_out(i)(j),
          snk_config_o => mt_rmq_o.src_config_in(i)(j),
          config_o     => config_out(f_slot_index(i, j))
          );

      U_RX : entity work.mt_rmq_endpoint_rx
        port map (
          clk_i          => clk_i,
          rst_n_i        => rst_n_i,
          header_valid_i => incoming_header_valid,
          header_i       => incoming_header,
          framer_snk_i   => incoming_snk_in,
--          framer_snk_o   => incoming_snk_out,
          mq_src_o       => mt_rmq_o.snk_in(i)(j),
          mq_src_i       => mt_rmq_i.snk_out(i)(j),
          snk_config_i   => mt_rmq_i.snk_config_out(i)(j),
          snk_config_o   => mt_rmq_o.snk_config_in(i)(j));

      req_out(f_slot_index(i, j))         <= mt_rmq_i.src_out(i)(j).req;
      out_snk_in_flat(f_slot_index(i, j)) <= mt_rmq_i.src_out(i)(j);
      mt_rmq_o.src_in(i)(j)                <= out_snk_out_flat(f_slot_index(i, j));

    end generate gen_foreach_slot;
  end generate gen_foreach_cpu;


  U_TX_Path : entity work.mt_rmq_tx_path
    port map (
      clk_i    => clk_i,
      rst_n_i  => rst_n_i,
      snk_i    => snk_muxed_in,
      snk_o    => snk_muxed_out,
      src_i    => wrsrc_snk_out,
      src_o    => wrsrc_snk_in,
      config_i => config_out_muxed
      );

  U_WR_Source : entity work.mt_wr_source
    port map (
      clk_i   => clk_i,
      rst_n_i => rst_n_i,
      snk_i   => wrsrc_snk_in,
      snk_o   => wrsrc_snk_out,
      src_i   => eth_src_i,
      src_o   => eth_src_o);

  p_arbitrate_outputs_fsm : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        out_state <= IDLE;
      else
        case out_state is
          when IDLE =>
            active_out <= f_prio_encode(req_out);
            if unsigned(req_out) /= 0 then
              out_state <= SEND;
            end if;
          when SEND =>
            if req_out(active_out) = '0' then
              out_state <= IDLE;
            end if;

            config_out_muxed <= config_out(active_out);
          when others => null;
        end case;

      end if;
    end if;
  end process;

  p_mux_streams_out : process(active_out, out_snk_in_flat, snk_muxed_out)
  begin
    snk_muxed_in <= out_snk_in_flat(active_out);
    for i in 0 to f_count_out_slots-1 loop
      if active_out = i then
        out_snk_out_flat(i) <= snk_muxed_out;
      else
        out_snk_out_flat(i).ready     <= '0';
        out_snk_out_flat(i).pkt_ready <= '0';
      end if;
    end loop;
  end process;


  ---------------------
  -- RX Part
  --------------------

  U_WR_Sink : entity work.mt_wr_sink
    port map (
      clk_i   => clk_i,
      rst_n_i => rst_n_i,
      snk_i   => eth_snk_i,
      snk_o   => eth_snk_o,
      src_o   => wrsnk_src_out,
      src_i   => wrsnk_src_in);

  U_RX_Path : entity work.mt_rmq_rx_path
    port map (
      clk_i            => clk_i,
      rst_n_i          => rst_n_i,
      snk_i            => wrsnk_src_out,
      snk_o            => wrsnk_src_in,
      src_i            => incoming_snk_out,
      src_o            => incoming_snk_in,
      p_header_valid_o => incoming_header_valid,
      p_header_o       => incoming_header);


  incoming_snk_out.ready <= '1';


end arch;
