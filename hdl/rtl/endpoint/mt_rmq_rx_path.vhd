-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_rx_path
--
-- description: Remote MQ RX path.  Deframe (extract header).

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;
use work.mt_endpoint_pkg.all;

entity mt_rmq_rx_path is
  port (
    clk_i            : in  std_logic;
    rst_n_i          : in  std_logic;
    snk_i            : in  t_mt_stream_sink_in;
    snk_o            : out t_mt_stream_sink_out;
    src_i            : in  t_mt_stream_source_in;
    src_o            : out t_mt_stream_source_out;
    p_header_valid_o : out std_logic;
    p_header_o       : out t_rmq_ep_rx_header);
end entity mt_rmq_rx_path;

architecture arch of mt_rmq_rx_path is

  signal fwd_pipe : t_mt_stream_source_out;
  signal rev_pipe : t_mt_stream_source_in;

begin

  U_rmq_rx_deframer : entity work.mt_rmq_rx_deframer
    port map (
      clk_i            => clk_i,
      rst_n_i          => rst_n_i,
      snk_i            => snk_i,
      snk_o            => snk_o,
      src_i            => rev_pipe,
      src_o            => fwd_pipe,
      p_header_valid_o => p_header_valid_o,
      p_is_udp_o       => p_header_o.is_udp,
      p_is_raw_o       => p_header_o.is_raw,
      p_src_mac_o      => p_header_o.src_mac,
      p_dst_mac_o      => p_header_o.dst_mac,
      p_vlan_id_o      => p_header_o.vlan_id,
      p_ethertype_o    => p_header_o.ethertype,
      p_src_port_o     => p_header_o.src_port,
      p_dst_port_o     => p_header_o.dst_port,
      p_src_ip_o       => p_header_o.src_ip,
      p_dst_ip_o       => p_header_o.dst_ip,
      p_udp_length_o   => p_header_o.udp_length);

  U_stream_register : entity work.mt_rmq_stream_register
    port map (
      clk_i   => clk_i,
      rst_n_i => rst_n_i,
      snk_i   => fwd_pipe,
      snk_o   => rev_pipe,
      src_i   => src_i,
      src_o   => src_o);

end arch;
