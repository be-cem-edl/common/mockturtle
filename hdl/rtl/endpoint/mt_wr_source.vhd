-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_wr_source
--
-- description: Mock Turtle White Rabbit source
--  MT stream to WR fabric.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.genram_pkg.all;
use work.wr_fabric_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_wr_source is
  port (
    clk_i   : in  std_logic;
    rst_n_i : in  std_logic;
    snk_i   : in  t_mt_stream_sink_in;
    snk_o   : out t_mt_stream_sink_out;
    src_i   : in  t_wrf_source_in;
    src_o   : out t_wrf_source_out);
end mt_wr_source;

architecture arch of mt_wr_source is

  constant c_FIFO_WIDTH : integer := 16 + 2 + 4;

  signal q_valid, full, we, rd, rd_d0 : std_logic;
  signal fin, fout                    : std_logic_vector(c_FIFO_WIDTH-1 downto 0);

  signal pre_dvalid : std_logic;
  signal pre_eof    : std_logic;
  signal pre_data   : std_logic_vector(15 downto 0);
  signal pre_addr   : std_logic_vector(1 downto 0);

  signal post_dvalid, post_eof, post_bytesel, post_sof : std_logic;

  signal err_status        : t_wrf_status_reg;
  signal cyc_int, cyc_next : std_logic;

  signal status_sent : std_logic;

  type t_state is (IDLE, SEND_STATUS, SEND_DATA, WAIT_ACKS);
  signal state : t_state;

  signal ack_count : unsigned(10 downto 0);
  signal src_out   : t_wrf_source_out;

begin  -- arch

  err_status.error <= '1';

  snk_o.ready <= not full;

  snk_o.pkt_ready <= '0';

  rd <= (not src_i.stall) and status_sent;
  we <= (snk_i.valid or snk_i.error) and (not full);

  pre_dvalid <= snk_i.valid or snk_i.error;
  pre_data   <= snk_i.data(15 downto 0) when snk_i.error = '0' else f_marshall_wrf_status(err_status);
  pre_addr   <= c_WRF_DATA               when snk_i.error = '0' else c_WRF_STATUS;
  pre_eof    <= snk_i.valid and snk_i.last;

  fin(15 downto 0)  <= pre_data;
  fin(17 downto 16) <= "00";
  fin(20)           <= pre_eof;

  U_FIFO : generic_shiftreg_fifo
    generic map (
      g_DATA_WIDTH => c_FIFO_WIDTH,
      g_SIZE       => 16)
    port map (
      rst_n_i       => rst_n_i,
      clk_i         => clk_i,
      d_i           => fin,
      we_i          => we,
      q_o           => fout,
      rd_i          => rd,
      almost_full_o => full,
      q_valid_o     => q_valid);

  post_eof <= fout(20);

  p_fsm : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        state <= IDLE;
      else
        case state is

          when IDLE =>
            cyc_int     <= '0';
            status_sent <= '0';
            if q_valid = '1' then
              cyc_int <= '1';
              state   <= SEND_STATUS;
            end if;

          when SEND_STATUS =>
            if src_i.stall = '0' then
              state       <= SEND_DATA;
              status_sent <= '1';
            end if;

          when SEND_DATA =>
            if q_valid = '1' and post_eof = '1' and src_i.stall = '0' then
              state <= WAIT_ACKS;
            end if;

          when WAIT_ACKS =>
            if ack_count = 0 then
              cyc_int <= '0';
              state   <= IDLE;
            end if;

        end case;
      end if;
    end if;
  end process p_fsm;

  p_gen_cyc : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        ack_count <= (others => '0');
      else
        if src_out.cyc = '1' then
          if src_out.stb = '1' and src_i.stall = '0' and src_i.ack = '0' then
            ack_count <= ack_count + 1;
          elsif (src_out.stb = '0' or src_i.stall = '1') and src_i.ack = '1' then
            ack_count <= ack_count - 1;
          end if;
        else
          ack_count <= (others => '0');
        end if;
      end if;
    end if;
  end process p_gen_cyc;

  src_out.cyc <= cyc_int;
  src_out.we  <= '1';
  src_out.stb <= q_valid when state /= WAIT_ACKS else '0';
  src_out.sel <= "11";

  p_src_out : process(status_sent, fout)
  begin
    if status_sent = '0' then
      src_out.dat <= (others => '0');
      src_out.adr <= c_WRF_STATUS;
    else
      src_out.dat <= fout(15 downto 0);
      src_out.adr <= fout(17 downto 16);
    end if;
  end process p_src_out;

  src_o <= src_out;

end arch;
