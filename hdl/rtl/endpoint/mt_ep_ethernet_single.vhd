-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_ep_ethernet_single
--
-- description: Ethernet endpoint for a single Mock Turtle RMQ.

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mock_turtle_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;
use work.gencores_pkg.all;
use work.mt_endpoint_pkg.all;
use work.wr_fabric_pkg.all;

entity mt_ep_ethernet_single is
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    --  Rx (eth -> RMQ)
    rmq_src_i         : in  t_mt_stream_source_in;
    rmq_src_o         : out t_mt_stream_source_out;
    rmq_src_config_i  : in  t_mt_stream_config_out;
    rmq_src_config_o  : out t_mt_stream_config_in;

    --  TX (RMQ -> eth)
    rmq_snk_i         : in  t_mt_stream_sink_in;
    rmq_snk_o         : out t_mt_stream_sink_out;
    rmq_snk_config_i  : in  t_mt_stream_config_out;
    rmq_snk_config_o  : out t_mt_stream_config_in;

    -- Fabric interface to the WR Core
    eth_src_o : out t_wrf_source_out;
    eth_src_i : in  t_wrf_source_in;
    eth_snk_o : out t_wrf_sink_out;
    eth_snk_i : in  t_wrf_sink_in
    );
end mt_ep_ethernet_single;

architecture arch of mt_ep_ethernet_single is

  signal config_out       : t_rmq_ep_tx_config;

  signal wrsrc_snk_in, incoming_snk_in   : t_mt_stream_sink_in;
  signal wrsrc_snk_out, incoming_snk_out : t_mt_stream_sink_out;
  signal wrsnk_src_in                    : t_mt_stream_source_in;
  signal wrsnk_src_out                   : t_mt_stream_source_out;
  signal incoming_header_valid : std_logic;
  signal incoming_header       : t_rmq_ep_rx_header;
begin

  --  TX

  --  Configuration registers
  U_TX : entity work.mt_rmq_endpoint_tx
    port map (
      clk_i        => clk_i,
      rst_n_i      => rst_n_i,
      snk_config_i => rmq_snk_config_i,
      snk_config_o => rmq_snk_config_o,
      config_o     => config_out
      );

  --  Tx pipeline
  U_TX_Path : entity work.mt_rmq_tx_path
    port map (
      clk_i    => clk_i,
      rst_n_i  => rst_n_i,
      snk_i    => rmq_snk_i,
      snk_o    => rmq_snk_o,
      src_i    => wrsrc_snk_out,
      src_o    => wrsrc_snk_in,
      config_i => config_out
      );

  --  To WR core
  U_WR_Source : entity work.mt_wr_source
    port map (
      clk_i   => clk_i,
      rst_n_i => rst_n_i,
      snk_i   => wrsrc_snk_in,
      snk_o   => wrsrc_snk_out,
      src_i   => eth_src_i,
      src_o   => eth_src_o);

  --  RX

  --  To endpoint + filter
  U_RX : entity work.mt_rmq_endpoint_rx
    port map (
      clk_i          => clk_i,
      rst_n_i        => rst_n_i,
      header_valid_i => incoming_header_valid,
      header_i       => incoming_header,
      framer_snk_i   => incoming_snk_in,
      framer_snk_o   => incoming_snk_out,
      mq_src_o       => rmq_src_o,
      mq_src_i       => rmq_src_i,
      snk_config_i   => rmq_src_config_i,
      snk_config_o   => rmq_src_config_o);

  U_RX_Path : entity work.mt_rmq_rx_path
    port map (
      clk_i            => clk_i,
      rst_n_i          => rst_n_i,
      snk_i            => wrsnk_src_out,
      snk_o            => wrsnk_src_in,
      src_i            => incoming_snk_out,
      src_o            => incoming_snk_in,
      p_header_valid_o => incoming_header_valid,
      p_header_o       => incoming_header);

  U_WR_Sink : entity work.mt_wr_sink
    port map (
      clk_i   => clk_i,
      rst_n_i => rst_n_i,
      snk_i   => eth_snk_i,
      snk_o   => eth_snk_o,
      src_o   => wrsnk_src_out,
      src_i   => wrsnk_src_in);


end arch;
