-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_ethernet_tx_framer
--
-- description: Remote MQ Ethernet framer.

library ieee;
use ieee.std_logic_1164.all;

use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;
use work.wr_fabric_pkg.all;

entity mt_ethernet_tx_framer is
  port (
    clk_i         : in  std_logic;
    rst_n_i       : in  std_logic;
    snk_i         : in  t_mt_stream_sink_in;
    snk_o         : out t_mt_stream_sink_out;
    src_i         : in  t_mt_stream_source_in;
    src_o         : out t_mt_stream_source_out;
    p_dst_mac_i   : in  std_logic_vector(47 downto 0);
    p_ethertype_i : in  std_logic_vector(15 downto 0);
    p_vlan_en     : in  std_logic;
    p_vlan_id     : in  std_logic_vector(15 downto 0));
end entity mt_ethernet_tx_framer;

architecture arch of mt_ethernet_tx_framer is
  type t_state is (IDLE, DMAC0, DMAC1, SMAC0, SMAC1, SMAC2, VLAN_TYPE, VLAN_ID, ETHERTYPE, PAYLOAD);
  signal state  : t_state;
begin  -- arch

  p_comb : process(state, snk_i, src_i)
  begin
    if state = PAYLOAD then
      snk_o.ready <= src_i.ready;
    else
      snk_o.ready <= '0';
    end if;
  end process p_comb;

  p_fsm : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        state       <= IDLE;
        src_o.valid <= '0';
        src_o.last  <= '0';
        src_o.data  <= (others => '0');
      else
        case state is
          when IDLE =>
            if src_i.ready = '1' then
              src_o.valid <= '0';
            end if;

            if snk_i.valid = '1' then
              state                   <= DMAC0;
              src_o.last              <= '0';
              src_o.data(15 downto 0) <= p_dst_mac_i(47 downto 32);
              src_o.valid             <= '1';
            end if;

          when DMAC0 =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= p_dst_mac_i(31 downto 16);
              src_o.valid             <= '1';
              state                   <= DMAC1;
            end if;

          when DMAC1 =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= p_dst_mac_i(15 downto 0);
              src_o.valid             <= '1';
              state                   <= SMAC0;
            end if;

          when SMAC0 =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= (others => '0');
              src_o.valid             <= '1';
              state                   <= SMAC1;
            end if;

          when SMAC1 =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= (others => '0');
              src_o.valid             <= '1';
              state                   <= SMAC2;
            end if;

          when SMAC2 =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= (others => '0');
              src_o.valid             <= '1';
              if p_vlan_en = '1' then
                state <= VLAN_TYPE;
              else
                state <= ETHERTYPE;
              end if;
            end if;

          when VLAN_TYPE =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= x"8100";
              src_o.valid             <= '1';
              state                   <= VLAN_ID;
            end if;

          when VLAN_ID =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= p_vlan_id;
              src_o.valid             <= '1';
              state                   <= ETHERTYPE;
            end if;

          when ETHERTYPE =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= p_ethertype_i;
              src_o.valid             <= '1';
              state                   <= PAYLOAD;
            end if;

          when PAYLOAD =>
            if src_i.ready = '1' then
              src_o.data(15 downto 0) <= snk_i.data(15 downto 0);
              src_o.valid             <= snk_i.valid;
              src_o.last              <= snk_i.last;
              if snk_i.last = '1' and snk_i.valid = '1' then
                state <= IDLE;
              end if;
            end if;

        end case;
      end if;
    end if;
  end process p_fsm;

  src_o.error <= '0';
  src_o.hdr   <= '0';

  snk_o.pkt_ready <= '0';

end architecture arch;
