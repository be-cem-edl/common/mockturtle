-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_wr_sink
--
-- description: Mock Turtle White Rabbit sink: WR fabric to MT stream.

library ieee;
use ieee.std_logic_1164.all;

use work.genram_pkg.all;
use work.wr_fabric_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_wr_sink is

  port (
    clk_i   : in  std_logic;
    rst_n_i : in  std_logic;
    -- Wishbone Fabric Interface I/O
    snk_i   : in  t_wrf_sink_in;
    snk_o   : out t_wrf_sink_out;
    -- MT internal fabric source
    src_o   : out t_mt_stream_source_out;
    src_i   : in  t_mt_stream_source_in);
end mt_wr_sink;

architecture arch of mt_wr_sink is

  constant c_FIFO_WIDTH : integer := 16 + 1;

  signal q_valid, full, we, rd : std_logic;
  signal fin, fout   : std_logic_vector(c_FIFO_WIDTH-1 downto 0);
  signal cyc_d0, rd_d0         : std_logic;

  signal pre_eof     : std_logic;
  signal pre_dvalid  : std_logic;

  signal snk_out      : t_wrf_sink_out;
  signal data_present : std_logic;
  signal is_data      : std_logic;

begin  -- arch

  p_delay_cyc_and_rd : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        cyc_d0 <= '0';
        rd_d0  <= '0';
      else
        if full = '0' then
          cyc_d0 <= snk_i.cyc;
        end if;

        rd_d0 <= rd;
      end if;
    end if;
  end process p_delay_cyc_and_rd;

  is_data <= '1' when snk_i.adr = c_WRF_DATA else '0';

  pre_eof     <= not snk_i.cyc and cyc_d0;  -- eof
  pre_dvalid  <= is_data and snk_i.stb and snk_i.we and snk_i.cyc and not snk_out.stall;  -- data valid

  snk_out.stall <= full or (snk_i.cyc and not cyc_d0);
  snk_out.err   <= '0';
  snk_out.rty   <= '0';

  p_gen_ack : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        snk_out.ack <= '0';
      else
        snk_out.ack <= snk_i.cyc and snk_i.stb and snk_i.we and not snk_out.stall;
      end if;
    end if;
  end process p_gen_ack;

  fin(16) <= pre_eof;

  p_latch_data : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        data_present <= '0';
      else

        if pre_dvalid = '1' then
          fin(15 downto 0)  <= snk_i.dat;
          data_present      <= '1';
        elsif pre_eof = '1' then
          data_present <= '0';
        end if;
      end if;
    end if;
  end process p_latch_data;

  we <= data_present and (pre_dvalid or pre_eof);

  snk_o <= snk_out;

  rd <= q_valid and src_i.ready;

  -- fixme: we assume this doesn't get full.
  U_FIFO : generic_shiftreg_fifo
    generic map (
      g_DATA_WIDTH => c_FIFO_WIDTH,
      g_SIZE       => 16)
    port map (
      rst_n_i       => rst_n_i,
      clk_i         => clk_i,
      d_i           => fin,
      we_i          => we,
      q_o           => fout,
      rd_i          => rd,
      almost_full_o => full,
      q_valid_o     => q_valid);

  src_o.data(31 downto 16) <= (others => '0');
  src_o.data(15 downto 0)  <= fout(15 downto 0);
  src_o.last               <= q_valid and fout(16);
  src_o.error              <= '0';  -- fixme
  src_o.valid              <= q_valid;
  src_o.hdr                <= '0';

end arch;
