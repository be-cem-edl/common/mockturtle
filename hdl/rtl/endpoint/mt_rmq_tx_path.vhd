-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_tx_path
--
-- description: Remote MQ TX packet assembler: add eth, ip, udp headers.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;
use work.mt_endpoint_pkg.all;

entity mt_rmq_tx_path is
  port (
    clk_i             : in  std_logic;
    rst_n_i           : in  std_logic;
    snk_i             : in  t_mt_stream_sink_in;
    snk_o             : out t_mt_stream_sink_out;
    src_i             : in  t_mt_stream_source_in;
    src_o             : out t_mt_stream_source_out;
    config_i          : t_rmq_ep_tx_config
    );
end mt_rmq_tx_path;

architecture arch of mt_rmq_tx_path is

  type t_mt_stream_source_out_array is array(integer range<>) of t_mt_stream_source_out;
  type t_mt_stream_source_in_array is array(integer range<>) of t_mt_stream_source_in;

  signal fwd_pipe : t_mt_stream_source_out_array(0 to 2);
  signal rev_pipe : t_mt_stream_source_in_array(0 to 2);


begin

  fwd_pipe(0) <= snk_i;
  snk_o       <= rev_pipe(0);

  U_UDPFramer : entity work.mt_udp_tx_framer
    port map (
      clk_i           => clk_i,
      rst_n_i         => rst_n_i,
      snk_i           => fwd_pipe(0),
      snk_o           => rev_pipe(0),
      src_i           => rev_pipe(1),
      src_o           => fwd_pipe(1),
      p_src_port_i    => config_i.src_port,
      p_dst_port_i    => config_i.dst_port,
      p_src_ip_i      => config_i.src_ip,
      p_dst_ip_i      => config_i.dst_ip);

--  p_payload_len <= std_logic_vector(unsigned(p_payload_words_i) sll 1);

  U_EthernetFramer : entity work.mt_ethernet_tx_framer
    port map (
      clk_i         => clk_i,
      rst_n_i       => rst_n_i,
      snk_i         => fwd_pipe(1),
      snk_o         => rev_pipe(1),
      src_i         => rev_pipe(2),
      src_o         => fwd_pipe(2),
      p_dst_mac_i   => config_i.dst_mac,
      p_ethertype_i => config_i.ethertype,
      p_vlan_en     => config_i.vlan_en,
      p_vlan_id     => config_i.vlan_id);

  src_o       <= fwd_pipe(2);
  rev_pipe(2) <= src_i;

end arch;
