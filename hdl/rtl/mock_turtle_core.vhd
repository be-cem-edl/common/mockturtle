-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mock_turtle_core
--
-- description: top level, interconnecting the CPU cores, Message Queues, Host
-- interface and the Shared Memory.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mock_turtle_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.mt_cpu_csr_wbgen2_pkg.all;
use work.mt_per_cpu_csr_pkg.all;
use work.mt_mqueue_pkg.all;
use work.gencores_pkg.all;

entity mock_turtle_core is

  generic (
    -- Message Queue and CPU configuration
    g_CONFIG            : t_mt_config := c_DEFAULT_MT_CONFIG;
    -- Frequency of clk_i, in Hz
    g_SYSTEM_CLOCK_FREQ : integer     := 62500000;
    -- Optional instruction memory init files. Unfortunately prior to VHDL2008,
    -- strings are not allowed within records, otherwise these could be embedded
    -- in g_CONFIG.cpu_config.
    g_CPU0_IRAM_INITF   : string      := "none";
    g_CPU1_IRAM_INITF   : string      := "none";
    g_CPU2_IRAM_INITF   : string      := "none";
    g_CPU3_IRAM_INITF   : string      := "none";
    g_CPU4_IRAM_INITF   : string      := "none";
    g_CPU5_IRAM_INITF   : string      := "none";
    g_CPU6_IRAM_INITF   : string      := "none";
    g_CPU7_IRAM_INITF   : string      := "none";
    -- Enables/disables WR support
    g_WITH_WHITE_RABBIT : boolean     := FALSE);
  port (
    clk_i           : in  std_logic;
    rst_n_i         : in  std_logic;
    cpu_ext_rst_n_i : in  std_logic_vector(g_CONFIG.cpu_count-1 downto 0) := (others=>'1');
    -- shared peripheral port
    sp_master_o     : out t_wishbone_master_out;
    sp_master_i     : in  t_wishbone_master_in;
    -- dedicated (per-cpu) peripheral port
    dp_master_o     : out t_wishbone_master_out_array(0 to g_CONFIG.cpu_count-1);
    dp_master_i     : in  t_wishbone_master_in_array(0 to g_CONFIG.cpu_count-1);

    -- Endpoint interface
    rmq_endpoint_o  : out t_mt_rmq_endpoint_iface_out;
    rmq_endpoint_i  : in  t_mt_rmq_endpoint_iface_in := c_MT_RMQ_ENDPOINT_IFACE_IN_DEFAULT_VALUE;

    host_slave_i    : in  t_wishbone_slave_in;
    host_slave_o    : out t_wishbone_slave_out;
    clk_ref_i       : in  std_logic;
    tm_i            : in  t_mt_timing_if;
    gpio_o          : out std_logic_vector(31 downto 0);
    gpio_i          : in  std_logic_vector(31 downto 0) := x"00000000";
    hmq_in_irq_o    : out std_logic;
    hmq_out_irq_o   : out std_logic;
    notify_irq_o    : out std_logic;
    console_irq_o   : out std_logic);

end mock_turtle_core;

architecture arch of mock_turtle_core is

  -- Not elegant at all. Unfortunately, prior to VHDL2008, strings are not allowed
  -- within records, otherwise these could be embedded in g_CONFIG.cpu_config.
  function f_mt_cpu_iram_init_pick(idx : integer)
    return string is
  begin
    case idx is
      when 0      => return g_CPU0_IRAM_INITF;
      when 1      => return g_CPU1_IRAM_INITF;
      when 2      => return g_CPU2_IRAM_INITF;
      when 3      => return g_CPU3_IRAM_INITF;
      when 4      => return g_CPU4_IRAM_INITF;
      when 5      => return g_CPU5_IRAM_INITF;
      when 6      => return g_CPU6_IRAM_INITF;
      when 7      => return g_CPU7_IRAM_INITF;
      when others => return "none";
    end case;
  end function f_mt_cpu_iram_init_pick;

  constant c_SMEM_REMAP_BASE_IN : t_wishbone_address_array(0 to 2) := (
    0 => x"00000000",
    1 => x"0000c000",
    2 => x"00010000");

  constant c_SMEM_REMAP_MASK_IN : t_wishbone_address_array(0 to 2) := (
    0 => x"00010000",
    1 => x"0001c000",  -- The remapper selects the last entry that matches
    2 => x"00010000");

  -- remapping to squeeze the host address space to 128 kB
  constant c_SMEM_REMAP_BASE_OUT : t_wishbone_address_array(0 to 2) := (
    0 => x"00000000",   -- 0x00000-0x0bfff -> HMQ (use priority in remapper)
    1 => x"00010000",                   -- 0x0c000-0x0ffff -> CSR, CROM
    2 => x"00200000");                  -- 0x10000-0x1ffff -> SMEM

  constant c_SMEM_REMAP_MASK_OUT : t_wishbone_address_array(0 to 2) := (
    0 => x"0000ffff",
    1 => x"00003fff",
    2 => x"0000ffff");

  --  HAC: Host ACcess
  constant c_HAC_WISHBONE_MASTERS : integer := 4;
  constant c_HAC_MASTER_HMQ       : integer := 0;
  constant c_HAC_MASTER_CPU_CSR   : integer := 1;
  constant c_HAC_MASTER_CROM      : integer := 2;
  constant c_HAC_MASTER_SI        : integer := 3;

  constant c_HAC_ADDRESS : t_wishbone_address_array (c_HAC_WISHBONE_MASTERS-1 downto 0) := (
    c_HAC_MASTER_HMQ     => x"00000000",   -- Host MQ    (Host addr 0x00000)
    c_HAC_MASTER_CPU_CSR => x"00010000",   -- CPU CSR    (Host addr 0x0c000)
    c_HAC_MASTER_CROM    => x"00012000",   -- Config ROM (Host addr 0x0e000)
    c_HAC_MASTER_SI      => x"00200000");  -- SMEM       (Host addr 0x10000)

  constant c_HAC_MASK : t_wishbone_address_array (c_HAC_WISHBONE_MASTERS-1 downto 0) := (
    c_HAC_MASTER_HMQ     => x"003f0000",   -- Host MQ
    c_HAC_MASTER_CPU_CSR => x"003ff000",   -- CPU CSR
    c_HAC_MASTER_CROM    => x"003ff000",   -- Config ROM
    c_HAC_MASTER_SI      => x"00300000");  -- SMEM

  signal hac_master_out : t_wishbone_master_out_array(c_HAC_WISHBONE_MASTERS-1 downto 0);
  signal hac_master_in  : t_wishbone_master_in_array(c_HAC_WISHBONE_MASTERS-1 downto 0);

  constant c_SI_WISHBONE_MASTERS : integer := 3;
  constant c_SI_WISHBONE_SLAVES  : integer := g_CONFIG.cpu_count + 1;
  constant c_SI_MASTER_SMEM      : integer := 0;
  constant c_SI_MASTER_CROM      : integer := 1;
  constant c_SI_MASTER_SP        : integer := 2;

  constant c_SI_SLAVE_HAC  : integer := 0;
  constant c_SI_SLAVE_CPU0 : integer := 1;

  constant c_SI_ADDRESS : t_wishbone_address_array (c_SI_WISHBONE_MASTERS-1 downto 0) := (
    c_SI_MASTER_SMEM => x"00200000",    -- Shared Memory
    c_SI_MASTER_CROM => x"00400000",    -- Config ROM
    c_SI_MASTER_SP   => x"08000000");

  constant c_SI_MASK : t_wishbone_address_array (c_SI_WISHBONE_MASTERS-1 downto 0) := (
    c_SI_MASTER_SMEM => x"0ff00000",    -- Shared Memory
    c_SI_MASTER_CROM => x"0ffff000",    -- Config ROM
    c_SI_MASTER_SP   => x"08000000");

  signal si_slave_in  : t_wishbone_slave_in_array(c_SI_WISHBONE_SLAVES-1 downto 0);
  signal si_slave_out : t_wishbone_slave_out_array(c_SI_WISHBONE_SLAVES-1 downto 0);

  signal si_master_in  : t_wishbone_master_in_array(c_SI_WISHBONE_MASTERS-1 downto 0);
  signal si_master_out : t_wishbone_master_out_array(c_SI_WISHBONE_MASTERS-1 downto 0);

  subtype t_cpu_range is natural range g_CONFIG.cpu_count-1 downto 0;

  signal hmq_slave_in  : t_wishbone_slave_in_array(t_cpu_range);
  signal hmq_slave_out : t_wishbone_slave_out_array(t_cpu_range);
  signal hmq_cpu_index : natural;

  signal hmq_in_irq_msk  : std_logic_vector(t_cpu_range);
  signal hmq_out_irq_msk : std_logic_vector(t_cpu_range);
  signal hmq_in_irq      : std_logic;
  signal hmq_out_irq     : std_logic;
  signal console_irq     : std_logic;
  signal notify_irq      : std_logic;

  type t_word_array is array(integer range <>) of std_logic_vector(31 downto 0);
  type t_byte_array is array(integer range <>) of std_logic_vector(7 downto 0);

  --  CSR inputs/outputs
  signal cpu_csr_fromwb : t_mt_cpu_csr_out_registers;
  signal cpu_csr_towb   : t_mt_cpu_csr_in_registers;

  type t_mt_per_cb_csr_in_array is array(t_cpu_range) of t_mt_per_cb_csr_in;
  type t_mt_per_cb_csr_out_array is array(t_cpu_range) of t_mt_per_cb_csr_out;

  signal per_cb_csr_towb   : t_mt_per_cb_csr_in_array;
  signal per_cb_csr_fromwb : t_mt_per_cb_csr_out_array;

  signal notif_value  : std_logic_vector(63 downto 0);
  signal notif_irq    : std_logic_vector(7 downto 0);
  signal ntf_irq_read : std_logic;

  signal hmq_in_status   : std_logic_vector(63 downto 0);
  signal hmq_out_status  : std_logic_vector(63 downto 0);
  signal hmq_in_int_en   : std_logic_vector(63 downto 0);
  signal hmq_out_int_en  : std_logic_vector(63 downto 0);
  signal mbx_data_fromwb : t_word_array(t_maxcpu_range);
  signal mbx_wr          : std_logic_vector(t_maxcpu_range);
  signal mbx_data_towb   : t_word_array(t_maxcpu_range);
  signal dbg_insn        : t_word_array(t_maxcpu_range);
  signal dbg_insn_wr     : std_logic_vector(t_maxcpu_range);

  signal cpu_index : integer := 0;

  signal core_uart_drdy, core_uart_dack : std_logic_vector(t_cpu_range);
  signal core_uart_msg_data             : t_byte_array(t_cpu_range);
  signal uart_msg_data_read_ack         : std_logic;

  signal cpu_gpio_out : t_word_array (t_cpu_range);

  signal rst_n_ref : std_logic;

  signal host_remapped_in  : t_wishbone_slave_in;
  signal host_remapped_out : t_wishbone_slave_out;

  signal rmq_src_o_int        : t_mt_stream_source_out_array2d;
  signal rmq_src_i_int        : t_mt_stream_source_in_array2d;
  signal rmq_snk_o_int        : t_mt_stream_sink_out_array2d;
  signal rmq_snk_i_int        : t_mt_stream_sink_in_array2d;
  signal rmq_src_config_o_int : t_mt_stream_config_out_array2d;
  signal rmq_src_config_i_int : t_mt_stream_config_in_array2d;
  signal rmq_snk_config_o_int : t_mt_stream_config_out_array2d;
  signal rmq_snk_config_i_int : t_mt_stream_config_in_array2d;

  function f_reduce_or (x : t_word_array) return std_logic_vector
  is
    variable rv : std_logic_vector(31 downto 0);
  begin
    rv := (others => '0');
    for n in x'range loop
      rv := rv or x(n);
    end loop;
    return rv;
  end f_reduce_or;

begin  -- arch

  gen_with_wr : if g_WITH_WHITE_RABBIT generate
    U_Sync_Refclk : gc_sync_ffs
      port map (
        clk_i    => clk_ref_i,
        rst_n_i  => '1',
        data_i   => rst_n_i,
        synced_o => rst_n_ref);
  end generate gen_with_wr;

  gen_without_wr : if not g_WITH_WHITE_RABBIT generate
    rst_n_ref <= rst_n_i;
  end generate gen_without_wr;

  U_Remap_SMEM : xwb_remapper
    generic map (
      g_NUM_RANGES => 3,
      g_BASE_IN    => c_SMEM_REMAP_BASE_IN,
      g_BASE_OUT   => c_SMEM_REMAP_BASE_OUT,
      g_MASK_IN    => c_SMEM_REMAP_MASK_IN,
      g_MASK_OUT   => c_SMEM_REMAP_MASK_OUT)
    port map (
      slave_i  => host_slave_i,
      slave_o  => host_slave_o,
      master_i => host_remapped_out,
      master_o => host_remapped_in);

  U_Host_Access_CB : xwb_crossbar
    generic map (
      g_VERBOSE     => FALSE,
      g_NUM_MASTERS => 1,
      g_NUM_SLAVES  => c_HAC_WISHBONE_MASTERS,
      g_REGISTERED  => TRUE,
      g_ADDRESS     => c_HAC_ADDRESS,
      g_MASK        => c_HAC_MASK)
    port map (
      clk_sys_i  => clk_i,
      rst_n_i    => rst_n_i,
      slave_i(0) => host_remapped_in,
      slave_o(0) => host_remapped_out,
      master_i   => hac_master_in,
      master_o   => hac_master_out);


  U_Shared_Interconnect : xwb_crossbar
    generic map (
      g_VERBOSE     => FALSE,
      g_NUM_MASTERS => c_SI_WISHBONE_SLAVES,
      g_NUM_SLAVES  => c_SI_WISHBONE_MASTERS,
      g_REGISTERED  => TRUE,
      g_ADDRESS     => c_SI_ADDRESS,
      g_MASK        => c_SI_MASK)
    port map (
      clk_sys_i => clk_i,
      rst_n_i   => rst_n_i,
      slave_i   => si_slave_in,
      slave_o   => si_slave_out,
      master_i  => si_master_in,
      master_o  => si_master_out);

  sp_master_o                   <= si_master_out(c_SI_MASTER_SP);
  si_master_in (c_SI_MASTER_SP) <= sp_master_i;

-- hack: replace SMEM high address bits with the SMEM_OP register value. This way,
-- the entire 64 kB SMEM window can be visible to the host with direct addressing,
-- and only the type of atomic operation has to be chosen indirectly (by
-- writing to SMEM_OP).

  si_slave_in(c_SI_SLAVE_HAC) <=
    (cyc => hac_master_out(c_HAC_MASTER_SI).cyc,
     stb => hac_master_out(c_HAC_MASTER_SI).stb,
     we  => hac_master_out(c_HAC_MASTER_SI).we,
     sel => hac_master_out(c_HAC_MASTER_SI).sel,
     dat => hac_master_out(c_HAC_MASTER_SI).dat,
     adr => (x"002" & '0'
             & cpu_csr_fromwb.smem_op_o
             & hac_master_out(c_HAC_MASTER_SI).adr(15 downto 0)));

  hac_master_in(c_HAC_MASTER_SI) <= si_slave_out(c_SI_SLAVE_HAC);

  U_CPU_CSR : entity work.mt_cpu_csr_wb_slave
    port map (
      rst_n_i                => rst_n_i,
      clk_sys_i              => clk_i,
      wb_adr_i               => hac_master_out(c_HAC_MASTER_CPU_CSR).adr(8 downto 2),
      wb_dat_i               => hac_master_out(c_HAC_MASTER_CPU_CSR).dat,
      wb_dat_o               => hac_master_in(c_HAC_MASTER_CPU_CSR).dat,
      wb_cyc_i               => hac_master_out(c_HAC_MASTER_CPU_CSR).cyc,
      wb_sel_i               => hac_master_out(c_HAC_MASTER_CPU_CSR).sel,
      wb_stb_i               => hac_master_out(c_HAC_MASTER_CPU_CSR).stb,
      wb_we_i                => hac_master_out(c_HAC_MASTER_CPU_CSR).we,
      wb_ack_o               => hac_master_in(c_HAC_MASTER_CPU_CSR).ack,
      wb_stall_o             => hac_master_in(c_HAC_MASTER_CPU_CSR).stall,
      ntf_irq_read           => ntf_irq_read,
      uart_msg_data_rd_ack_o => uart_msg_data_read_ack,
      regs_i                 => cpu_csr_towb,
      regs_o                 => cpu_csr_fromwb);

  hac_master_in(c_HAC_MASTER_CPU_CSR).err <= '0';
  hac_master_in(c_HAC_MASTER_CPU_CSR).rty <= '0';

  cpu_index <= to_integer(unsigned(cpu_csr_fromwb.core_sel_o));

  cpu_csr_towb.dbg_core0_mbx_i <= mbx_data_towb(0);
  cpu_csr_towb.dbg_core1_mbx_i <= mbx_data_towb(1);

  mbx_wr <= (
    0      => cpu_csr_fromwb.dbg_core0_mbx_load_o,
    1      => cpu_csr_fromwb.dbg_core1_mbx_load_o,
    others => '0');

  dbg_insn <= (
    0      => cpu_csr_fromwb.dbg_core0_insn_o,
    1      => cpu_csr_fromwb.dbg_core1_insn_o,
    others => x"0000_0000");

  dbg_insn_wr <= (
    0      => cpu_csr_fromwb.dbg_core0_insn_wr_o,
    1      => cpu_csr_fromwb.dbg_core1_insn_wr_o,
    others => '0');

  mbx_data_fromwb <= (
    0      => cpu_csr_fromwb.dbg_core0_mbx_o,
    1      => cpu_csr_fromwb.dbg_core1_mbx_o,
    others => x"0000_0000");

  hmq_cpu_index <= to_integer(unsigned(cpu_csr_fromwb.hmq_sel_core_o));

  hmq_in_int_en(31 downto 0)   <= cpu_csr_fromwb.hmqi_inten_lo_o;
  hmq_in_int_en(63 downto 32)  <= cpu_csr_fromwb.hmqi_inten_hi_o;
  hmq_out_int_en(31 downto 0)  <= cpu_csr_fromwb.hmqo_inten_lo_o;
  hmq_out_int_en(63 downto 32) <= cpu_csr_fromwb.hmqo_inten_hi_o;

  gen_cpus : for i in 0 to g_CONFIG.cpu_count-1 generate
    alias c_CFG : t_mt_cpu_config is g_CONFIG.cpu_config(i);
  begin
    hmq_slave_in(i) <=
      (cyc => (hac_master_out(c_HAC_MASTER_HMQ).cyc
               and f_to_std_logic(hmq_cpu_index = i)),
       stb => hac_master_out(c_HAC_MASTER_HMQ).stb,
       we  => hac_master_out(c_HAC_MASTER_HMQ).we,
       sel => hac_master_out(c_HAC_MASTER_HMQ).sel,
       dat => hac_master_out(c_HAC_MASTER_HMQ).dat,
       adr => (x"000" & '0' & cpu_csr_fromwb.hmq_sel_queue_o
               & hac_master_out(c_HAC_MASTER_HMQ).adr(15 downto 0)));

    U_CPU_Block : entity work.mt_cpu_cb
      generic map (
        g_CPU_ID            => i,
        g_CPU_CONFIG        => c_CFG,
        g_CPU_IRAM_INIT     => f_mt_cpu_iram_init_pick(i),
        g_WITH_WHITE_RABBIT => g_WITH_WHITE_RABBIT,
        g_SYSTEM_CLOCK_FREQ => g_SYSTEM_CLOCK_FREQ)
      port map (
        clk_sys_i        => clk_i,
        rst_n_i          => rst_n_i,
        cpu_ext_rst_n_i  => cpu_ext_rst_n_i(i),
        clk_ref_i        => clk_ref_i,
        rst_n_ref_i      => rst_n_ref,
        tm_i             => tm_i,
        sh_master_i      => si_slave_out(c_SI_SLAVE_CPU0 + i),
        sh_master_o      => si_slave_in(C_SI_SLAVE_CPU0 + i),
        dp_master_i      => dp_master_i(i),
        dp_master_o      => dp_master_o(i),
        hmq_slave_i      => hmq_slave_in(i),
        hmq_slave_o      => hmq_slave_out(i),
        rmq_src_o        => rmq_src_o_int(i)(0 to c_CFG.rmq_config.slot_count-1),
        rmq_src_i        => rmq_src_i_int(i)(0 to c_CFG.rmq_config.slot_count-1),
        rmq_snk_o        => rmq_snk_o_int(i)(0 to c_CFG.rmq_config.slot_count-1),
        rmq_snk_i        => rmq_snk_i_int(i)(0 to c_CFG.rmq_config.slot_count-1),
        rmq_src_config_o => rmq_src_config_o_int(i)(0 to c_CFG.rmq_config.slot_count-1),
        rmq_src_config_i => rmq_src_config_i_int(i)(0 to c_CFG.rmq_config.slot_count-1),
        rmq_snk_config_o => rmq_snk_config_o_int(i)(0 to c_CFG.rmq_config.slot_count-1),
        rmq_snk_config_i => rmq_snk_config_i_int(i)(0 to c_CFG.rmq_config.slot_count-1),

        cb_csr_i    => per_cb_csr_fromwb(i),
        cb_csr_o    => per_cb_csr_towb(i),
        gpio_o      => cpu_gpio_out(i),
        gpio_i      => gpio_i,
        uart_drdy_o => core_uart_drdy(i),
        uart_dack_i => core_uart_dack(i),
        uart_data_o => core_uart_msg_data(i));

    cpu_csr_towb.dbg_status_i(i)      <= per_cb_csr_towb(i).cpu_i.dbg_cpu_status_i;
    cpu_csr_towb.dbg_insn_ready_i(i)  <= per_cb_csr_towb(i).cpu_i.dbg_insn_ready_i;
    cpu_csr_towb.uart_poll_ready_i(i) <= core_uart_drdy(i);

    notif_irq(i)                    <= per_cb_csr_towb(i).host_int_i;
    notif_value(8*i + 7 downto 8*i) <= per_cb_csr_towb(i).host_int_val_i;

    mbx_data_towb(i) <= per_cb_csr_towb(i).cpu_i.dbg_mbx_i;

    hmq_in_status(8*i + 7 downto 8*i)  <= per_cb_csr_towb(i).hmqi_status_i;
    hmq_out_status(8*i + 7 downto 8*i) <= per_cb_csr_towb(i).hmqo_status_i;

    hmq_in_irq_msk(i) <= f_to_std_logic (
      (hmq_in_status(8*i + 7 downto 8*i) and
       hmq_in_int_en(8*i + 7 downto 8*i)) /= X"00");

    hmq_out_irq_msk(i) <= f_to_std_logic (
      (hmq_out_status(8*i + 7 downto 8*i) and
       hmq_out_int_en(8*i + 7 downto 8*i)) /= X"00");

    gen_unused_rmq_slots : for j in c_CFG.rmq_config.slot_count to t_maxslot_range'high generate
      rmq_src_o_int(i)(j) <= c_MT_DUMMY_SINK_IN;
      rmq_snk_o_int(i)(j) <= c_MT_DUMMY_SOURCE_IN;
    end generate gen_unused_rmq_slots;

    per_cb_csr_fromwb(i) <= (
      cpu_o             => (
        reset_o         => cpu_csr_fromwb.reset_o(i),
        uaddr_addr_o    => cpu_csr_fromwb.uaddr_addr_o,
        core_sel_o      => f_to_std_logic(cpu_index = i),
        udata_o         => cpu_csr_fromwb.udata_o,
        udata_load_o    => cpu_csr_fromwb.udata_load_o,
        dbg_cpu_force_o => cpu_csr_fromwb.dbg_force_o(i),
        dbg_insn_o      => dbg_insn(i),
        dbg_insn_wr_o   => dbg_insn_wr(i),
        dbg_mbx_o       => mbx_data_fromwb(i),
        dbg_mbx_wr_o    => mbx_wr(i)));
  end generate gen_cpus;

  gen_no_cpus : for i in g_CONFIG.cpu_count to t_maxcpu_range'high generate
    cpu_csr_towb.dbg_status_i(i)       <= '0';
    cpu_csr_towb.dbg_insn_ready_i(i)   <= '0';
    cpu_csr_towb.uart_poll_ready_i(i)  <= '0';
    notif_irq(i)                       <= '0';
    notif_value(8*i + 7 downto 8*i)    <= (others => '0');
    hmq_in_status(8*i + 7 downto 8*i)  <= (others => '0');
    hmq_out_status(8*i + 7 downto 8*i) <= (others => '0');
    rmq_snk_o_int(i)                   <= (others => c_MT_DUMMY_SOURCE_IN);
    rmq_src_o_int(i)                   <= (others => c_MT_DUMMY_SINK_IN);
  end generate gen_no_cpus;

  p_notif_irq_reg: process (clk_i) is
  begin  -- process p_notif_irq_reg
    if rising_edge(clk_i) then  -- rising clock edge
      if rst_n_i = '0' or ntf_irq_read = '1'then
        cpu_csr_towb.int_i <= (others => '0');
      else
        cpu_csr_towb.int_i <= notif_irq or cpu_csr_towb.int_i;
      end if;
    end if;
  end process p_notif_irq_reg;

  cpu_csr_towb.int_val_lo_i <= notif_value(31 downto 0);
  cpu_csr_towb.int_val_hi_i <= notif_value(63 downto 32);

  cpu_csr_towb.hmqi_status_lo_i <= hmq_in_status(31 downto 0);
  cpu_csr_towb.hmqi_status_hi_i <= hmq_in_status(63 downto 32);

  cpu_csr_towb.hmqo_status_lo_i <= hmq_out_status(31 downto 0);
  cpu_csr_towb.hmqo_status_hi_i <= hmq_out_status(63 downto 32);

  --  FIXME: maybe not very efficient ?
  hac_master_in(c_HAC_MASTER_HMQ) <= hmq_slave_out(hmq_cpu_index);

  cpu_csr_towb.udata_i <= per_cb_csr_towb(cpu_index).cpu_i.udata_i;

  U_Shared_Mem : entity work.mt_shared_mem
    generic map (
      g_SIZE => g_CONFIG.shared_mem_size)
    port map (
      clk_i   => clk_i,
      rst_n_i => rst_n_i,
      slave_i => si_master_out(c_SI_MASTER_SMEM),
      slave_o => si_master_in(c_SI_MASTER_SMEM));

  -- Configuration ROM with two simultaneous read-only ports
  -- for parallel access from HAC and SI crossbars
  U_Config_Rom : entity work.mt_config_rom
    generic map (
      g_CONFIG            => g_CONFIG,
      g_SYSTEM_CLOCK_FREQ => g_SYSTEM_CLOCK_FREQ,
      g_WITH_WHITE_RABBIT => g_WITH_WHITE_RABBIT)
    port map (
      clk_i    => clk_i,
      slave1_i => hac_master_out(c_HAC_MASTER_CROM),
      slave1_o => hac_master_in(c_HAC_MASTER_CROM),
      slave2_i => si_master_out(c_SI_MASTER_CROM),
      slave2_o => si_master_in(c_SI_MASTER_CROM));

  p_mux_debug_data : process(core_uart_msg_data, cpu_csr_fromwb, uart_msg_data_read_ack)
  begin

    cpu_csr_towb.uart_msg_data_i <= (others => 'X');

    for i in 0 to g_CONFIG.cpu_count-1 loop
      if unsigned(cpu_csr_fromwb.core_sel_o) = to_unsigned(i, 4) then
        cpu_csr_towb.uart_msg_data_i <= core_uart_msg_data(i);
        core_uart_dack(i)            <= uart_msg_data_read_ack;
      else
        core_uart_dack(i) <= '0';
      end if;
    end loop;
  end process p_mux_debug_data;

  p_debug_irq : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        console_irq <= '0';
      else
        console_irq <= '0';

        for i in 0 to g_CONFIG.cpu_count-1 loop
          if (core_uart_drdy(i) and cpu_csr_fromwb.uart_imsk_enable_o(i)) = '1'
          then
            console_irq <= '1';
          end if;
        end loop;
      end if;
    end if;
  end process p_debug_irq;

  rmq_endpoint_o.snk_out <= rmq_snk_o_int;
  rmq_endpoint_o.snk_config_out <= rmq_snk_config_o_int;
  rmq_endpoint_o.src_out <= rmq_src_o_int;
  rmq_endpoint_o.src_config_out <= rmq_src_config_o_int;

  rmq_snk_i_int <= rmq_endpoint_i.snk_in;
  rmq_src_i_int <= rmq_endpoint_i.src_in;
  rmq_snk_config_i_int <= rmq_endpoint_i.snk_config_in;
  rmq_src_config_i_int <= rmq_endpoint_i.src_config_in;
  
  gpio_o <= f_reduce_or(cpu_gpio_out);

  notify_irq  <= f_to_std_logic(cpu_csr_towb.int_i /= x"00");
  hmq_in_irq  <= f_to_std_logic(hmq_in_irq_msk /= (t_cpu_range  => '0'));
  hmq_out_irq <= f_to_std_logic(hmq_out_irq_msk /= (t_cpu_range => '0'));

  console_irq_o <= console_irq;
  notify_irq_o  <= notify_irq;
  hmq_in_irq_o  <= hmq_in_irq;
  hmq_out_irq_o <= hmq_out_irq;

end arch;
