-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_mqueue_wishbone_slave
--
-- description: Wishbone slave for MQ's Slots.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_mqueue_wishbone_slave is
  generic (
    g_CONFIG : t_mt_mqueue_config);
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    incoming_status_i : in t_slot_status_out_array(g_CONFIG.slot_count-1 downto 0);
    outgoing_status_i : in t_slot_status_out_array(g_CONFIG.slot_count-1 downto 0);

    incoming_o : out t_slot_bus_in_array(g_CONFIG.slot_count-1 downto 0);
    incoming_i : in  t_slot_bus_out_array(g_CONFIG.slot_count-1 downto 0);

    outgoing_o : out t_slot_bus_in_array(g_CONFIG.slot_count-1 downto 0);
    outgoing_i : in  t_slot_bus_out_array(g_CONFIG.slot_count-1 downto 0);

    slave_i : in  t_wishbone_slave_in;
    slave_o : out t_wishbone_slave_out;

    irq_config_o : out t_mt_irq_config);
end mt_mqueue_wishbone_slave;

architecture arch of mt_mqueue_wishbone_slave is

  signal slot_num, slot_num_d0       : std_logic_vector(2 downto 0);
  signal in_area_sel, in_area_sel_d0 : std_logic;
  signal out_area_sel                : std_logic;

  signal wb_write : std_logic;
  signal wb_read  : std_logic;

begin  -- arch

  slot_num     <= slave_i.adr(18 downto 16);
  in_area_sel  <= slave_i.cyc when slave_i.adr(15) = '0' else '0';
  out_area_sel <= slave_i.cyc when slave_i.adr(15) = '1' else '0';

  wb_write <= slave_i.cyc and slave_i.stb and slave_i.we;
  wb_read  <= slave_i.cyc and slave_i.stb and not slave_i.we;

  gen_slots : for i in 0 to g_CONFIG.slot_count-1 generate
    incoming_o(i) <=
      (sel   => f_to_std_logic (in_area_sel = '1' and unsigned(slot_num) = i),
       adr   => slave_i.adr(13 downto 0),
       dat   => slave_i.dat,
       we    => wb_write,
       wmask => slave_i.sel);

    outgoing_o(i) <=
      (sel   => f_to_std_logic (out_area_sel = '1' and unsigned(slot_num) = i),
       adr   => slave_i.adr(13 downto 0),
       dat   => slave_i.dat,
       we    => wb_write,
       wmask => slave_i.sel);
  end generate gen_slots;

  p_delay : process(clk_i)
  begin
    if rising_edge(clk_i) then
      slot_num_d0    <= slot_num;
      in_area_sel_d0 <= in_area_sel;
    end if;
  end process p_delay;

  gen_p_read_data : if g_CONFIG.slot_count > 0 generate
    p_read_data : process(in_area_sel_d0, incoming_i, out_area_sel, outgoing_i,
                          slot_num_d0)
    begin
      if in_area_sel_d0 = '1' then
        slave_o.dat <= incoming_i(to_integer(unsigned(slot_num_d0))).dat;
      elsif out_area_sel = '1' then
        slave_o.dat <= outgoing_i(to_integer(unsigned(slot_num_d0))).dat;
      else
        slave_o.dat <= (others => '0');
      end if;
    end process p_read_data;
  end generate gen_p_read_data;

  slave_o.stall <= '0';
  slave_o.err   <= '0';
  slave_o.rty   <= '0';

  p_ack : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        slave_o.ack <= '0';
      else
        slave_o.ack <= wb_read or wb_write;
      end if;
    end if;
  end process p_ack;

end architecture arch;
