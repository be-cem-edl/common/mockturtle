-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_mqueue_host
--
-- description: Host Message Queue implementation. Exchanges messages between
-- the CPU CBs and the host system.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_mqueue_host is
  generic (
    g_CONFIG : t_mt_mqueue_config := c_MT_DEFAULT_MQUEUE_CONFIG);
  port (
    clk_i             : in  std_logic;
    rst_n_i           : in  std_logic;

    --  Soft CPU bus
    cpu_slave_i       : in  t_wishbone_slave_in;
    cpu_slave_o       : out t_wishbone_slave_out;

    --  Host bus
    host_slave_i      : in  t_wishbone_slave_in;
    host_slave_o      : out t_wishbone_slave_out;

    --  Cpu status.
    cpu_in_status_o   : out std_logic_vector(g_CONFIG.slot_count-1 downto 0);
    cpu_out_status_o  : out std_logic_vector(g_CONFIG.slot_count-1 downto 0);

    --  Host status.
    host_in_status_o  : out std_logic_vector(g_CONFIG.slot_count-1 downto 0);
    host_out_status_o : out std_logic_vector(g_CONFIG.slot_count-1 downto 0));
end mt_mqueue_host;

architecture arch of mt_mqueue_host is

  subtype t_slot_range is natural range g_CONFIG.slot_count-1 downto 0;
  signal host_incoming_in  : t_slot_bus_in_array(t_slot_range);
  signal host_outgoing_in  : t_slot_bus_in_array(t_slot_range);
  signal cpu_incoming_in   : t_slot_bus_in_array(t_slot_range);
  signal cpu_outgoing_in   : t_slot_bus_in_array(t_slot_range);
  signal host_incoming_out : t_slot_bus_out_array(t_slot_range);
  signal host_outgoing_out : t_slot_bus_out_array(t_slot_range);
  signal cpu_incoming_out  : t_slot_bus_out_array(t_slot_range);
  signal cpu_outgoing_out  : t_slot_bus_out_array(t_slot_range);

  signal incoming_stat : t_slot_status_out_array(t_slot_range);
  signal outgoing_stat : t_slot_status_out_array(t_slot_range);

begin  -- arch

  U_CPU_Wishbone_Slave : entity work.mt_mqueue_wishbone_slave
    generic map (
      g_CONFIG   => g_CONFIG)
    port map (
      clk_i             => clk_i,
      rst_n_i           => rst_n_i,
      incoming_status_i => incoming_stat,
      outgoing_status_i => outgoing_stat,
      incoming_o        => cpu_incoming_in,
      incoming_i        => cpu_incoming_out,
      outgoing_o        => cpu_outgoing_in,
      outgoing_i        => cpu_outgoing_out,
      slave_i           => cpu_slave_i,
      slave_o           => cpu_slave_o);

  gen_slots : for i in 0 to g_CONFIG.slot_count-1 generate

    -- CB to Host direction (outgoing slots)
    U_Out_SlotX : entity work.mt_mqueue_slot
      generic map (
        g_CONFIG => g_config.slot_config(i))
      port map (
        clk_i   => clk_i,
        rst_n_i => rst_n_i,
        stat_o  => outgoing_stat(i),
        inb_i   => cpu_outgoing_in(i),
        inb_o   => cpu_outgoing_out(i),
        outb_i  => host_incoming_in(i),
        outb_o  => host_incoming_out(i));

    cpu_out_status_o (i) <= not outgoing_stat(i).full;
    host_in_status_o (i) <= not outgoing_stat(i).empty;

    -- Host to CB direction (incoming slots)
    U_In_SlotX : entity work.mt_mqueue_slot
      generic map (
        g_CONFIG => g_config.slot_config(i))
      port map (
        clk_i   => clk_i,
        rst_n_i => rst_n_i,
        stat_o  => incoming_stat(i),
        inb_i   => host_outgoing_in(i),
        inb_o   => host_outgoing_out(i),
        outb_i  => cpu_incoming_in(i),
        outb_o  => cpu_incoming_out(i));

    cpu_in_status_o (i)   <= not incoming_stat(i).empty;
    host_out_status_o (i) <= not incoming_stat(i).full;
  end generate gen_slots;

  U_Host_Wishbone_Slave : entity work.mt_mqueue_wishbone_slave
    generic map (
      g_CONFIG   => g_CONFIG)
    port map (
      clk_i             => clk_i,
      rst_n_i           => rst_n_i,
      incoming_status_i => outgoing_stat,
      outgoing_status_i => incoming_stat,
      incoming_o        => host_incoming_in,
      incoming_i        => host_incoming_out,
      outgoing_o        => host_outgoing_in,
      outgoing_i        => host_outgoing_out,
      slave_i           => host_slave_i,
      slave_o           => host_slave_o);

end architecture arch;
