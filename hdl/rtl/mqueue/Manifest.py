# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

files = [
    "mt_mqueue_host.vhd",
    "mt_mqueue_remote.vhd",
    "mt_rmq_tx.vhd",
    "mt_rmq_rx.vhd",
    "mt_mqueue_pkg.vhd",
    "mt_mqueue_slot.vhd",
    "mt_mqueue_wishbone_slave.vhd",
]
