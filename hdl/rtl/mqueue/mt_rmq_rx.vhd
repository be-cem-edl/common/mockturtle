-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_rx
--
-- description: Remote message rx adapter

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mt_mqueue_pkg.all;

entity mt_rmq_rx is

  generic (
    g_CONFIG : t_mt_mqueue_slot_config);
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    --  Input
    snk_o : out t_mt_stream_sink_out;
    snk_i : in  t_mt_stream_sink_in;

    --  Output
    outb_i : in  t_slot_bus_out;
    outb_o : out t_slot_bus_in;

    --  Status from output.
    outb_stat_i : in t_slot_status_out);
end mt_rmq_rx;

architecture arch of mt_rmq_rx is

  type t_state_enum is (WAIT_SLOT, CLAIM, WRITE_SIZE, READY);

  type t_state is record
    state        : t_state_enum;
    addr         : unsigned(13 downto 0);
    is_even      : std_logic;
    payload_size : unsigned(12 downto 0);
    is_hdr_d     : std_logic;
  end record;

  signal state, n_state : t_state;

begin
  p_fsm_next : process (rst_n_i, state, outb_stat_i, snk_i)
  begin
    if rst_n_i = '0' then
      n_state <= (state        => WAIT_SLOT,
                  addr         => (others => 'X'),
                  is_even      => '1',
                  payload_size => (others => 'X'),
                  is_hdr_d     => 'X');
      snk_o <= (pkt_ready => '0',
                ready     => '0');
      outb_o <= (sel   => '0',
                 adr   => (others => 'X'),
                 dat   => (others => 'X'),
                 we    => 'X',
                 wmask => "XXXX");
    else
      case state.state is
        when WAIT_SLOT =>
          if outb_stat_i.full = '0' then
            --  A packet can be received.  Claim it.
            snk_o <= (pkt_ready => '1',
                      ready     => '1');
            outb_o <= (sel   => '1',
                       adr   => c_mqueue_addr_command,
                       dat   => (c_mqueue_command_claim => '1', others => '0'),
                       we    => '1',
                       wmask => "1111");
            n_state <= (state        => CLAIM,
                        addr         => unsigned(c_mqueue_addr_header) + 8,
                        is_even      => '1',
                        payload_size => (others => '0'),
                        is_hdr_d     => 'X');
          else
            --  Wait until a slot is ready.
            snk_o <= (pkt_ready => '0',
                      ready     => '0');
            outb_o <= (sel   => '0',
                       adr   => (others => 'X'),
                       dat   => (others => 'X'),
                       we    => 'X',
                       wmask => "XXXX");
            n_state <= state;
          end if;

        when CLAIM =>
          n_state <= state;

          snk_o <= (pkt_ready => '1',
                    ready     => '1');

          if snk_i.valid = '1' then
            --  Data available, store it.
            outb_o <= (sel   => '1',
                       adr   => std_logic_vector(state.addr),
                       dat   => (snk_i.data(15 downto 0)
                                 & snk_i.data(15 downto 0)),
                       we    => '1',
                       wmask => "0000");

            if state.is_even = '1' then
              outb_o.wmask <= "1100";
            else
              outb_o.wmask <= "0011";
            end if;

            if snk_i.error = '1' then
              -- Error (in the packet) or packet dropped by the EP filter
              n_state.state <= WAIT_SLOT;
            elsif snk_i.last = '1' then
              --  End of payload.
              n_state.state <= WRITE_SIZE;
            end if; -- snk_i.error

            if (state.is_hdr_d = '1' and snk_i.hdr = '0') then
              -- end of header
              outb_o.adr           <= c_mqueue_addr_payload;
              outb_o.wmask         <= "1100";
              n_state.addr         <= unsigned(c_mqueue_addr_payload);
              n_state.is_even      <= '0';
              n_state.payload_size <= to_unsigned(2, 13);
              n_state.is_hdr_d     <= '0';
            else
              n_state.is_hdr_d <= snk_i.hdr;
              n_state.is_even  <= not state.is_even;

              if(snk_i.hdr = '0') then
                n_state.payload_size <= state.payload_size + 2;
              end if;

              if state.is_even = '1' then
                n_state.addr <= state.addr;
              else
                n_state.addr <= state.addr + 4;
              end if;  -- even
            end if;  -- hdr

          else
            outb_o <= (sel   => '0',
                       adr   => (others => 'X'),
                       dat   => (others => 'X'),
                       we    => '0',
                       wmask => "XXXX");
          end if; -- snk_i.valid

        when WRITE_SIZE =>
          --  Make the slot ready.
          snk_o <= (pkt_ready => '0',
                    ready     => '0');
          outb_o <= (sel   => '1',
                     adr   => c_mqueue_addr_header_size,
                     dat   => std_logic_vector(resize(state.payload_size, 32)),
                     we    => '1',
                     wmask => "1111");

          n_state <= (state   => READY,
                      addr    => (others => 'X'),
                      is_even => '1',
                      payload_size => (others => 'X'),
                      is_hdr_d => 'X');
        when READY =>
          --  Make the slot ready.
          snk_o <= (pkt_ready => '0',
                    ready     => '0');
          outb_o <= (sel            => '1',
                     adr            => c_mqueue_addr_command,
                     dat            => (c_mqueue_command_ready => '1',
                                        others => '0'),
                     we             => '1',
                     wmask          => "1111");
          n_state <= (state   => WAIT_SLOT,
                      addr    => (others => 'X'),
                      is_even => '1',
                      payload_size => (others => 'X'),
                      is_hdr_d => 'X');

      end case;
    end if;
  end process;

  p_fsm_reg : process (clk_i, rst_n_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        state <= (state   => WAIT_SLOT,
                  addr    => (others => 'X'),
                  is_even => '1',
                  payload_size => (others => 'X'),
                  is_hdr_d => 'X');

      else
        state <= n_state;
      end if;
    end if;
  end process;
end arch;
