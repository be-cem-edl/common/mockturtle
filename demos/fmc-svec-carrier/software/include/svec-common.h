/*
 * Copyright (c) 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __SVEC_COMMON_H
#define __SVEC_COMMON_H
#include <mockturtle/mockturtle.h>


/* HMQ slots used for input */
#define SVEC_HMQ_IN 0

/* HMQ slots used for output */
#define SVEC_HMQ_OUT 0

/* Variable index - used only by svec-librt */
enum trtl_fw_variable_index {
	SVEC_VAR_LEMO_STA = 0,
	SVEC_VAR_LEMO_DIR,
	SVEC_VAR_LEMO_SET,
	SVEC_VAR_LEMO_CLR,
	SVEC_VAR_LED_STA,
	SVEC_VAR_LED_SET,
	SVEC_VAR_LED_CLR,
	SVEC_VAR_AUTO,
	__SVEC_VAR_MAX,
};

enum trtl_fw_buffer_index {
	SVEC_BUF_TEST = 0,
	__SVEC_BUF_MAX,
};

/* Command and log message IDs */
enum rt_action_recv_svec {
	SVEC_ID_LED_SET = 0,
	SVEC_ID_LEMO_SET,
	SVEC_ID_LEMO_DIR_SET,
	SVEC_ID_STATE_GET,
	SVEC_ID_RUN_AUTO,
	SVEC_ID_STATE_GET_REP,
};

/* For the time being all fields must be 32bit because of HW limits */
#define SVEC_BUF_MAX_ARRAY 5
struct svec_structure {
	uint32_t field1;
	uint32_t field2;
	uint32_t array[SVEC_BUF_MAX_ARRAY];
};

#define PIN_LEMO_COUNT	4
#define PIN_LEMO_MASK	(0x0000000F)
#define PIN_LED_COUNT	8
#define PIN_LED_OFFSET	8
#define PIN_LED_MASK	(0x0000FFFF)

#define PIN_LED_RED(ledno)	(8 + (ledno * 2))
#define PIN_LED_GREEN(ledno)	(9 + (ledno * 2))

#endif
