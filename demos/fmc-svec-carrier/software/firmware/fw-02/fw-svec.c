/**
 * Copyright (c) 2015-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <string.h>
#include <mockturtle-rt.h>
#include <fw-svec-common.h>
#include <mockturtle-framework.h>

#define GPIO_CODR	0x0 /* Clear Data Register */
#define GPIO_SODR	0x4 /* Set Data Register */
#define GPIO_DDR	0x8 /* Direction Data Register */
#define GPIO_PSR	0xC /* Status Register */

static struct svec_structure svec_struct;

struct trtl_fw_buffer svec_buffers[] = {
	[SVEC_BUF_TEST] = {
		.buf = &svec_struct,
		.len = sizeof(struct svec_structure),
	}
};

struct trtl_fw_variable svec_variables[] = {
	[SVEC_VAR_LEMO_STA] = {
		.addr = TRTL_ADDR_DP(GPIO_PSR),
		.mask = PIN_LEMO_MASK,
		.offset = 0,
	},
	[SVEC_VAR_LEMO_DIR] = {
		.addr = TRTL_ADDR_DP(GPIO_DDR),
		.mask = PIN_LEMO_MASK,
		.offset = 0,
	},
	[SVEC_VAR_LEMO_SET] = {
		.addr = TRTL_ADDR_DP(GPIO_SODR),
		.mask = PIN_LEMO_MASK,
		.offset = 0,
	},
	[SVEC_VAR_LEMO_CLR] = {
		.addr = TRTL_ADDR_DP(GPIO_CODR),
		.mask = PIN_LEMO_MASK,
		.offset = 0,
	},
	[SVEC_VAR_LED_STA] = {
		.addr = TRTL_ADDR_DP(GPIO_PSR),
		.mask = PIN_LED_MASK,
		.offset = PIN_LED_OFFSET,
	},
	[SVEC_VAR_LED_SET] = {
		.addr = TRTL_ADDR_DP(GPIO_SODR),
		.mask = PIN_LED_MASK,
		.offset = PIN_LED_OFFSET,
	},
	[SVEC_VAR_LED_CLR] = {
		.addr = TRTL_ADDR_DP(GPIO_CODR),
		.mask = PIN_LED_MASK,
		.offset = PIN_LED_OFFSET,
	},
	[SVEC_VAR_AUTO] = {
		.addr = &autosvec_run,
		.mask = 0xFFFFFFFF,
		.offset = 0,
	},
};


/**
 * Send messages over the debug interface
 */
static int svec_debug_interface(void)
{
	pp_printf("Hello world.\n\r");
	pp_printf("We are messages over the serial interface.\n\r");
	pp_printf("Print here your messages.\n\r");

	return 0;
}


/**
 * Well, the main :)
 */
static int svec_main()
{
	while (1) {
		/* Handle all messages incoming from HMQ 0 as actions */
		trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);
	}

	return 0;
}


struct trtl_fw_application app = {
	.name = "manualsvec",
	.version = {
		.rt_id = RT_APPLICATION_ID,
		.rt_version = RT_VERSION(1, 0),
		.git_version = GIT_VERSION
	},

	.buffers = svec_buffers,
	.n_buffers = ARRAY_SIZE(svec_buffers),

	.variables = svec_variables,
	.n_variables = ARRAY_SIZE(svec_variables),

	.init = svec_debug_interface,
	.main = svec_main,
};
