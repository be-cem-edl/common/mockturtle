/**
 * Copyright (c) 2015-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#ifndef __FW_SVEC_COMMON_H__
#define __FW_SVEC_COMMON_H__
#include <mockturtle-rt.h>
#include <svec-common.h>

extern volatile int autosvec_run;
extern volatile int autosvec_led_period;
extern volatile int autosvec_lemo3_period;
extern volatile int autosvec_lemo4_period;
extern volatile int autosvec_print_period;

#endif
