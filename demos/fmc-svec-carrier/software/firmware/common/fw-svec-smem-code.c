/**
 * Copyright (c) 2015-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "mockturtle-rt.h"
#include "fw-svec-common.h"

SMEM volatile int autosvec_run;
#ifndef SIMULATION
SMEM volatile int autosvec_led_period = 0x1FFFF;
SMEM volatile int autosvec_lemo3_period = 0x1FFFF;
SMEM volatile int autosvec_lemo4_period = 0x3FFFF;
SMEM volatile int autosvec_print_period = 0xFFFFF;
#else
SMEM volatile int autosvec_led_period = 0x1;
SMEM volatile int autosvec_lemo3_period = 0x1;
SMEM volatile int autosvec_lemo4_period = 0x3;
SMEM volatile int autosvec_print_period = 0x7;
#endif
