/*
 * Copyright (c) 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>
#include <libsvec.h>
#include <inttypes.h>


static void help()
{
	fprintf(stderr,
		"svec -D 0x<hex-number> -L 0x<hex-number> -l 0x<hex-number> -c <char> -a <char> -s\n");
	fprintf(stderr, "-D device id\n");
	fprintf(stderr, "-l value to write into the LED register\n");
	fprintf(stderr, "-L value to write into the LEMO register\n");
	fprintf(stderr, "-d value to write into the LEMO direction register\n");
	fprintf(stderr, "-s it reports the content of LED and LEMO registers\n");
	fprintf(stderr, "-c set led color (g: green, r: red, o: orange)\n");
	fprintf(stderr, "-a set autosvec status (r: run, s: stop)\n");
	fprintf(stderr, "-v show version\n");
	fprintf(stderr, "-t send random value to the structure and read them back\n");
	fprintf(stderr, "\n");
	exit(1);
}

static void svec_print_status(struct svec_status *status)
{
	fprintf(stdout, "Status:\n");
	fprintf(stdout, "\tled\t0x%x\n", status->led);
	fprintf(stdout, "\tlemo\t0x%x\n", status->lemo);
	fprintf(stdout, "\t\tdirection\t0x%x\n", status->lemo_dir);
	fprintf(stdout, "\tautosvec\t%s\n", status->autosvec ? "run" : "stop");
}

static void svec_print_version(struct trtl_fw_version *version)
{
	fprintf(stdout, "Version:\n");
	fprintf(stdout, "\tRT: 0x%x\n", version->rt_id);
	fprintf(stdout, "\tRT Version: 0x%x\n", version->rt_version);
	fprintf(stdout, "\tGit Version: 0x%x\n", version->git_version);
}

static void svec_print_structure(struct svec_structure *test)
{
	int i;

	fprintf(stdout, "\tfield1: 0x%x\n", test->field1);
	fprintf(stdout, "\tfield2: 0x%x\n", test->field2);
	for (i = 0; i < SVEC_BUF_MAX_ARRAY; i++)
		fprintf(stdout, "\tarray[%d]: 0x%x\n", i, test->array[i]);
}

int main(int argc, char *argv[])
{
	struct svec_node *svec;
	struct svec_status status;
	uint32_t dev_id = 0;
	int led = -1, lemo = -1, lemo_dir = -1, i;
	char c, c_color = 0, autosvec = 0;
	int err = 0, show_status = 0, show_version = 0, structure = 0;
	enum svec_color color = SVEC_RED;
	struct trtl_fw_version version;
	struct svec_structure test, test_rb;

	while ((c = getopt (argc, argv, "hD:l:L:d:c:sa:vt")) != -1) {
		switch (c) {
		case 'h':
		case '?':
			help();
			break;
		case 'D':
			sscanf(optarg, "0x%x", &dev_id);
			break;
		case 'l':
			sscanf(optarg, "0x%x", &led);
			break;
		case 'L':
			sscanf(optarg, "0x%x", &lemo);
			break;
		case 'd':
			sscanf(optarg, "0x%x", &lemo_dir);
			break;
		case 'c':
			sscanf(optarg, "%c", &c_color);
			switch (c_color) {
			case 'g':
				color = SVEC_GREEN;
				break;
			case 'r':
				color = SVEC_RED;
				break;
			case 'o':
				color = SVEC_ORANGE;
				break;
			}
			break;
		case 's':
			show_status = 1;
			break;
		case 'a':
			sscanf(optarg, "%c", &autosvec);
			break;
		case 'v':
			show_version = 1;
			break;
		case 't':
			structure = 1;
			break;
		}
	}

	if (dev_id == 0) {
		help();
		exit(1);
	}

	atexit(svec_exit);
	err = svec_init();
	if (err) {
		fprintf(stderr, "Cannot init svec library: %s\n",
			svec_strerror(errno));
		exit(1);
	}

	svec = svec_open_by_id(dev_id);
	if (!svec) {
		fprintf(stderr, "Cannot open svec: %s\n", svec_strerror(errno));
		exit(1);
	}


	/* Set autosvec status */
	if (autosvec != 0)
		svec_run_autosvec(svec, autosvec == 'r' ? 1 : 0);

	if (lemo_dir >= 0) {
		/* Set LEMO direction */
		err = svec_lemo_dir_set(svec, lemo_dir);
		if (err)
			fprintf(stderr, "Cannot set LEMO direction: %s\n",
				svec_strerror(errno));
	}

	if (led >= 0) {
		/* Set LED register */
		err = svec_led_set(svec, led, color);
		if (err)
			fprintf(stderr, "Cannot set LED: %s\n", svec_strerror(errno));
	}

	if (lemo >= 0) {
		/* Set LEMO register */
		err = svec_lemo_set(svec, lemo);
		if (err)
			fprintf(stderr, "Cannot set LEMO: %s\n", svec_strerror(errno));
	}

	if (show_status) {
		/* Get the current status */
		err = svec_status_get(svec, &status);
		if (err)
			fprintf(stderr, "Cannot get status: %s\n", svec_strerror(errno));
		else
			svec_print_status(&status);
	}

	if (show_version) {
		err = svec_version(svec, &version);
		if (err)
			fprintf(stderr, "Cannot get version: %s\n",
				svec_strerror(errno));
		else
			svec_print_version(&version);
	}

	if (structure) {
		/* Generate random numbers (cannot use getrandom(2) because
		   of old system)*/
		uint32_t seq = 0;
		test.field1 = seq++;
		test.field2 = seq++;
		for (i = 0; i < SVEC_BUF_MAX_ARRAY; i++)
			test.array[i] = seq++;

		fprintf(stdout, "Generated structure:\n");
		svec_print_structure(&test);

		err = svec_test_struct_set(svec, &test);
		if (err) {
			fprintf(stderr, "Cannot set structure: %s\n",
				svec_strerror(errno));
		} else {
			err = svec_test_struct_get(svec, &test_rb);
			if (err) {
				fprintf(stderr, "Cannot get structure: %s\n",
					svec_strerror(errno));
			} else {
				if (memcmp(&test, &test_rb, sizeof(struct svec_structure))) {
					fprintf(stderr, "Got wrong structure: %s\n",
						svec_strerror(errno));
					svec_print_structure(&test_rb);
				} else {
					fprintf(stderr,
						"Structure correctly read back\n");
				}
			}
		}
	}

	svec_close(svec);

	exit(0);
}
