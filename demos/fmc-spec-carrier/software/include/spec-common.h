/*
 * Copyright (c) 2015-2019 CERN (home.cern)
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#ifndef __SPEC_COMMON_H
#define __SPEC_COMMON_H
#include <mockturtle/mockturtle.h>


/**
 * Exported buffers
 */
enum trtl_fw_buffer_index {
	SPEC_BUF_CFG = 0,
	__SPEC_BUF_MAX,
};


/**
 * Known action ID (a.k.a. message ID)
 */
enum rt_action_recv_spec {
	SPEC_ID_CFG_SET = 0,
	SPEC_ID_CFG_GET,
};


/**
 * The possible LED modes
 */
enum spec_led_mode {
	SPEC_LED_MODE0 = 0,
	SPEC_LED_MODE1,
	SPEC_LED_MODE2,
	__SPEC_LED_MODE_MAX,
};


/**
 * Button mode
 */
enum spec_btn_mode {
	SPEC_BTN_NONE = 0,
	SPEC_BTN_PAUSE,
	SPEC_BTN_RESET,
	__SPEC_BTN_MAX,
};

struct spec_cfg {
	enum spec_led_mode led;
	enum spec_btn_mode btn[2];
};

#endif
