/*
 * Copyright (c) 2018-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#ifndef __LIBSPEC_INTERNAL__H__
#define __LIBSPEC_INTERNAL__H__

#include <stdlib.h>
#include <libspec.h>

#define SPEC_CPU_AUTO 0
#define SPEC_CPU_MANUAL 1
#define SPEC_CPU_MANUAL_HMQ 0

/**
 * Description of a fmc-spec-carrier device
 */
struct spec_desc {
	struct trtl_dev *trtl; /**< TRTL device associated */
	uint32_t dev_id; /**< fmc device id */
	uint32_t app_id; /**< Application id */
	uint32_t n_cpu; /**< Number of CPUs */
};

#endif
