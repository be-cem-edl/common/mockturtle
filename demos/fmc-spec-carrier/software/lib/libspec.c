/*
 * Copyright (c) 2018-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

/*
 * This is just a SPEC, the code is not optimized
 */
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <mockturtle/libmockturtle.h>
#include <libspec-internal.h>

const char *spec_btn_str[] = {
	"none",
	"pause",
	"reset",
};

const char *spec_errors[] = {
	"Invalid configuration\n",
};


const char *spec_btn_to_name(enum spec_btn_mode mode)
{
	return spec_btn_str[mode];
}

/**
 * Return a string messages corresponding to a given error code. If
 * it is not a libwrtd error code, it will run trtl_strerror()
 * @param[in] err error code
 * @return a message error
 */
const char *spec_strerror(unsigned int err)
{
	if (err < __ESPEC_MIN_ERROR_NUMBER || err >= __ESPEC_MAX_ERROR_NUMBER)
		return trtl_strerror(err);

	return spec_errors[err - __ESPEC_MIN_ERROR_NUMBER];
}


/**
 * Initialize the SPEC library. It must be called before doing
 * anything else.
 * This library is based on the libmockturtle, so internally, this function also
 * run spec_init() in order to initialize the TRTL library.
 * @return 0 on success, otherwise -1 and errno is appropriately set
 */
int spec_init()
{
	int err;

	err = trtl_init();
	if (err)
		return err;

	return 0;
}


/**
 * Release the resources allocated by spec_init(). It must be called when
 * you stop to use this library. Then, you cannot use functions from this
 * library.
 */
void spec_exit()
{
	trtl_exit();
}


/**
 * Open a WRTD node device using ID ID
 * @param[in] device_id ID device identificator
 * @return It returns an anonymous spec_node structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
struct spec_node *spec_open_by_id(uint32_t device_id)
{
	struct spec_desc *spec;

	spec = malloc(sizeof(struct spec_desc));
	if (!spec)
		return NULL;

	spec->trtl = trtl_open_by_id(device_id);
	if (!spec->trtl)
		goto out;

	spec->dev_id = device_id;
	return (struct spec_node *)spec;

out:
	free(spec);
	return NULL;
}


/**
 * Close a SPEC device opened with one of the following function:
 * spec_open_by_id()
 * @param[in] dev device token
 */
void spec_close(struct spec_node *dev)
{
	struct spec_desc *spec = (struct spec_desc *)dev;

	trtl_close(spec->trtl);
	free(spec);
	dev = NULL;
}


/**
 * Return the TRTL token in order to allows users to run
 * functions from the TRTL library
 * @param[in] dev device token
 * @return the TRTL token
 */
struct trtl_dev *spec_get_trtl_dev(struct spec_node *dev)
{
	struct spec_desc *spec = (struct spec_desc *)dev;

	return (struct trtl_dev *)spec->trtl;
}


static int spec_cfg_is_valid(struct spec_cfg *cfg)
{
	if (cfg->led >= __SPEC_LED_MODE_MAX)
		return 0;
	if (cfg->btn[0] >= __SPEC_BTN_MAX)
		return 0;
	if (cfg->btn[1] >= __SPEC_BTN_MAX)
		return 0;
	return 1;
}

/**
 * Get firmware configuration
 * @param[in] dev device token
 * @param[out] cfg configuration read from Mock Turtle
 */
int spec_configuration_get(struct spec_node *dev, struct spec_cfg *cfg)
{
	struct spec_desc *spec = (struct spec_desc *)dev;
	struct trtl_tlv tlv = {
		.type = SPEC_BUF_CFG,
		.size = sizeof(struct spec_cfg),
		.buf = cfg,
	};
	int err;

	err = trtl_fw_buffer_get(spec->trtl,
				 SPEC_CPU_MANUAL, SPEC_CPU_MANUAL_HMQ,
				 &tlv, 1);
	if (err)
		return err;
	if (!spec_cfg_is_valid(cfg)) {
		errno = ESPEC_INVALID_CONFIG;
		return -1;
	}

	return 0;
}


/**
 * Set firmwae configuration
 * @param[in] dev device token
 * @param[out] cfg configuration read from Mock Turtle
 */
int spec_configuration_set(struct spec_node *dev, struct spec_cfg *cfg)
{
	struct spec_desc *spec = (struct spec_desc *)dev;
	struct trtl_tlv tlv = {
		.type = SPEC_BUF_CFG,
		.size = sizeof(struct spec_cfg),
		.buf = cfg,
	};

	if (!spec_cfg_is_valid(cfg)) {
		errno = ESPEC_INVALID_CONFIG;
		return -1;
	}

	return trtl_fw_buffer_set(spec->trtl,
				  SPEC_CPU_MANUAL, SPEC_CPU_MANUAL_HMQ,
				  &tlv, 1);
}


int spec_version(struct spec_node *dev, struct trtl_fw_version *version)
{
	struct spec_desc *spec = (struct spec_desc *)dev;

	return trtl_fw_version(spec->trtl,
			       SPEC_CPU_MANUAL, SPEC_CPU_MANUAL_HMQ,
			       version);
}
