/*
 * Copyright (c) 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>
#include <libspec.h>
#include <inttypes.h>


static void help()
{
	fprintf(stderr,
		"spec -D 0x<hex-number> -c -b <idx>,<mode> -l <mode>\n");
	fprintf(stderr, "-D device id\n");
	fprintf(stderr, "-c show current configuration\n");
	fprintf(stderr, "-l led mode [0, 1, 2]\n");
	fprintf(stderr, "-b assign button mode [r: RESET, p: PAUSE]\n");
	fprintf(stderr, "-v show version\n");
	fprintf(stderr, "\n");
	exit(1);
}

static void spec_print_status(struct spec_cfg *cfg)
{
	fprintf(stdout, "Status:\n");
	fprintf(stdout, "\tled\t%d\n", cfg->led);
	fprintf(stdout, "\tbutton 1\t%s\n", spec_btn_to_name(cfg->btn[0]));
	fprintf(stdout, "\tbutton 2\t%s\n", spec_btn_to_name(cfg->btn[1]));
}

static void spec_print_version(struct trtl_fw_version *version)
{
	fprintf(stdout, "Version:\n");
	fprintf(stdout, "\tRT: 0x%x\n", version->rt_id);
	fprintf(stdout, "\tRT Version: 0x%x\n", version->rt_version);
	fprintf(stdout, "\tGit Version: 0x%x\n", version->git_version);
}

int main(int argc, char *argv[])
{
	struct spec_node *spec;
	struct spec_cfg cfg, ccfg;
	uint32_t dev_id = 0;
	char c;
	int err = 0, show_config = 0, show_version = 0;
	struct trtl_fw_version version;

	while ((c = getopt (argc, argv, "hD:cl:b:v")) != -1) {
		switch (c) {
		case 'h':
		case '?':
			help();
			break;
		case 'D':
			sscanf(optarg, "0x%x", &dev_id);
			break;
		case 'c':
			show_config = 1;
			break;
		case 'v':
			show_version = 1;
			break;
		}
	}

	if (dev_id == 0) {
		help();
		exit(1);
	}

	atexit(spec_exit);
	err = spec_init();
	if (err) {
		fprintf(stderr, "Cannot init spec library: %s\n",
			spec_strerror(errno));
		exit(1);
	}

	spec = spec_open_by_id(dev_id);
	if (!spec) {
		fprintf(stderr, "Cannot open spec: %s\n", spec_strerror(errno));
		exit(1);
	}

	/* Get current configuration and modify it */
	err = spec_configuration_get(spec, &ccfg);
	if (err)
		fprintf(stderr, "Cannot get configuration: %s\n", spec_strerror(errno));

	memcpy(&cfg, &ccfg, sizeof(struct spec_cfg));
	optind = 0;
	while ((c = getopt (argc, argv, "hD:cl:b:v")) != -1) {
		int ret;

		switch (c) {
		case 'l':
			ret = sscanf(optarg, "%d", &cfg.led);
			if (ret != 1)  {
				fprintf(stderr, "Invalid argument for LED mode\n");
				help();
			}
			break;
		case 'b': {
			int idx;
			char mode;

			ret = sscanf(optarg, "%d,%c", &idx, &mode);
			if (ret != 2) {
				fprintf(stderr, "Invalid argument for button\n");
				help();
			}
			switch (mode) {
			case 'r':
				cfg.btn[idx] = SPEC_BTN_RESET;
				break;
			case 'p':
				cfg.btn[idx] = SPEC_BTN_PAUSE;
				break;
			default:
				fprintf(stderr, "Invalid button mode\n");
				help();
				spec_close(spec);
				exit(1);
				break;
			}
			break;
		}
		}
	}

	/* If something changes, reprogram */
	if (memcmp(&cfg, &ccfg, sizeof(struct spec_cfg)) != 0) {
		err = spec_configuration_set(spec, &cfg);
		if (err)
			fprintf(stderr, "Cannot set configuration: %s\n", spec_strerror(errno));
	}

	if (show_config) {
		/* Get the current status */
		err = spec_configuration_get(spec, &cfg);
		if (err)
			fprintf(stderr, "Cannot get configuration: %s\n", spec_strerror(errno));
		else
			spec_print_status(&cfg);
	}

	if (show_version) {
		err = spec_version(spec, &version);
		if (err)
			fprintf(stderr, "Cannot get version: %s\n",
				spec_strerror(errno));
		else
			spec_print_version(&version);
	}

	spec_close(spec);

	exit(0);
}
