/*
 * Copyright (c) 2018-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <inttypes.h>
#include <string.h>
#include "mockturtle-rt.h"
#include <fw-spec-common.h>

#define GPIO_CODR 0x0
#define GPIO_SODR 0x4
#define GPIO_DDR 0x8
#define GPIO_PSR 0xc

#define PIN_BTN_OFFSET	0
#define PIN_BTN_MASK	(0x00000003)
#define PIN_LED0_OFFSET 2
#define PIN_LED0_MASK   (0x0000003C)
#define PIN_LED1_OFFSET 6
#define PIN_LED1_MASK	(0x000000C0)
#define PIN_LED_OFFSET 2
#define PIN_LED_MASK (PIN_LED0_MASK | PIN_LED1_MASK)

static int gpio_get_state(int pin)
{
	/* Remember, the button logic is inverted */
	return !(dp_readl(GPIO_PSR) & (1 << pin));
}

static int btn_action(void)
{
	int i;

	for (i = 0; i < 2; ++i)
		if (gpio_get_state(i))
			return cfg.btn[i];
	return SPEC_BTN_NONE;
}

static void led_mode0(int iteration)
{
	dp_writel(PIN_LED0_MASK, iteration & 0x1 ? GPIO_SODR : GPIO_CODR);
}

static void led_mode1(int iteration)
{
	dp_writel(PIN_LED1_MASK, iteration & 0x1 ? GPIO_SODR : GPIO_CODR);
}

static void led_mode2(int iteration)
{
	led_mode0(iteration);
	led_mode1(iteration);
}

static void (*ledmodes[])(int) = {
	led_mode0,
	led_mode1,
	led_mode2,
};


static void autospec()
{
	unsigned int i, j = 0, stop;

	while (1) {
		/* Clear all GPIOs (LEDs) */
		dp_writel(PIN_LED_MASK, GPIO_CODR);

		for (i = 0, stop = 0; stop == 0; i++) {
			/* Run the button action when asked */
			switch (btn_action()) {
			case SPEC_BTN_RESET:
				stop = 1;
			case SPEC_BTN_PAUSE:
				continue;
			default:
				/* Update LEDs */
				if ((i & autospec_led_period) == 0 &&
				    cfg.led < __SPEC_LED_MODE_MAX)
					ledmodes[cfg.led](j++);
				break;
			}

			if ((i & autospec_print_period) == 0) {
				/* This output is not reliable for debugging purpose,
				   it's just to show that you can do it */
				pp_printf("GPIO direction 0x%"PRIx32"\n\rGPIO 0x%"PRIx32"\n\r",
					  dp_readl(GPIO_DDR),
					  dp_readl(GPIO_PSR));
			}
		}
	}
}


int main()
{
	cfg.led = SPEC_LED_MODE0;
	cfg.btn[0] = SPEC_BTN_RESET;
	cfg.btn[1] = SPEC_BTN_PAUSE;

#ifndef SIMULATION
	smem_atomic_or(&autospec_led_period, 0x1FFFF);
	smem_atomic_or(&autospec_print_period, 0xFFFFF);
#else
	smem_atomic_or(&autospec_led_period, 0x1);
	smem_atomic_or(&autospec_print_period, 0x7);
#endif

	/* Print something on the debug interface */
	pp_printf("Running LED 'blinker'\n\r");
	pp_printf("Update period:\n\r");
	pp_printf("\t- LED: 0x%x'\n\r", autospec_led_period);
	pp_printf("\t- print: 0x%x'\n\r", autospec_print_period);

	autospec();
}
